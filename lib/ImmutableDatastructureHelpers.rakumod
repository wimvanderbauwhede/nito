use v6;
unit module ImmutableDatastructureHelpers:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>:api<1>;

# A little API for manipulation of Map and List
sub insert(Map \m_, Str \k_, \v_ --> Map ) is export {
        Map.new(m_.list,Pair.new(k_,v_));
}
sub update(Map \m_, Str \k_, \v_ --> Map ) is export {
    Map.new(map ->\h { h.key eq k_ ?? Pair.new(k_,v_) !! h}, m_.list)
}
sub remove(Map \m_, Str \k_ --> Map ) is export {
    Map.new(grep ->\h { h.key ne k_ }, m_.list)
}

# push
sub append(List \l_, \e_ --> List) is export  {
    List.new(|l_,e_);
}
# unshift
sub prepend(List \l_, \e_ --> List) is export  {
    List.new(e_,|l_);
}


# drop the last element
sub init(List \l_ --> List) is export  {
    List.new(|l_[0 .. *-2]);
}
# return the last element, like pop
sub top(List \l_ --> Any) is export  {
    l_[*-1]
}

sub initLast(List \l_ --> List) is export  {
    [List.new(|l_[0 .. *-2]),l_[*-1]];
}

# return the first element, like shift
sub head(List \l_ --> Any) is export  {
    l_.head
}
sub tail(List \l_ --> List) is export  {
    List.new(|l_[1 .. *-1])
}

sub headTail(List \l_ --> List) is export  {
    [l_.head,List.new(|l_[1 .. *-1])]
}

