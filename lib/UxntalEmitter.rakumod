use v6;
use UxntalTypes;
use UxntalDefs;
use ImmutableDatastructureHelpers;

my $DBG=False;

unit module UxntalEmitter:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;

# ==== Emitter for registerised Uxntal code ====

# This emitter produces working Uxntal code where all stack operations have registerised
# This is the key transformation to emit LLVN IR, C, Fortran etc.

sub emit_tal_program( ParsedProgram \reg_based_program, Str \tal_file --> List) is export {
say '=' x 80,"\n",&?ROUTINE.name if $DBG;
    reduce(
        -> \ptt_,\code_block_label {
            my List \block_tal_tokens = _emit_tal_block(reg_based_program,code_block_label);
            [|ptt_, |block_tal_tokens];

        },
        ["( {tal_file} )\n"],
        |reg_based_program.blocks-sequence
    );

} # END of emit_tal_program

sub _emit_tal_block(ParsedProgram \parsed_program,Str \code_block_label --> List) {
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    my ParsedCodeBlock \parsed_block_rep = parsed_program.blocks{code_block_label};
    my List \parsed_block_code_ = parsed_block_rep.code;
    my Array \parsed_block_code = Array.new(|parsed_block_code_); # LAZY!
    my Array \block_tal_tokens=[];
    # say "FUNCTION: ",parsed_block_rep.function.raku if $DBG;
    # if parsed_block_rep.function ~~ Just and parsed_block_rep.function.just {
    #     my \label = shift parsed_block_code;
    #     my \reg_decls = pop parsed_block_code;
    #     my \closing_jump = pop parsed_block_code;
    #     # A bit of a hack, I use an empty opcode so that I only keep the input arg.
    #     # Also, this is not correct: the numbering of the retval regs is wrong, FIXME!
    #     # for |parsed_block_rep.function-retvals.just -> \retval {
    #     #     say "FUNCTION RETVAL: {retval.raku}" if $DBG;
    #     #     push parsed_block_code, RegOperation[Operation[1,0,Opcode['',1,False,False].new].new,[retval],NoRetVal].new;
    #     # }
    #     push parsed_block_code, closing_jump;
    #     push parsed_block_code, reg_decls;
    #     my \function_arguments = parsed_block_rep.function-args ~~ Just ?? map &__emit_retval, reverse |parsed_block_rep.function-args.just !! [];
    #     block_tal_tokens=[__emit_value(label),"\n"];#,function_arguments,"\n"];
    # }

    for |parsed_block_code -> \token_rep {
        my \tal_tokens_seq = __emit_tal_token(token_rep);
        block_tal_tokens=[|block_tal_tokens,|tal_tokens_seq,"\n"];
    }
    return block_tal_tokens;
} # END of _emit_tal_block

sub __emit_tal_token(Token \token_ --> List) {
    say '-' x 40,"\n",&?ROUTINE.name if $DBG;
    say "TOKEN:",token_ if $DBG;
    if %const_operand_types{token_.type}:exists
    or %labels_padding{token_.type}:exists
    or %raw_operand_types{token_.type}:exists
    or token_ ~~ Literal
    {
        return [__emit_value(token_)];
    } elsif token_ ~~ Operation {
        return  [__emit_opcode(token_)];
    } elsif token_ ~~ EvalOpcode {
        return  [ token_.Str ];
    } else {
        die "__emit_tal_token UNHANDLED token : {token_.raku}";
    }

} # END of __emit_tal_token

# # retvals are always registers and should always be emitted as stores
# sub __emit_retval(Token \retval --> List) {

#     if retval ~~ ( Reg | FunctionReg ) {
#         [ ',&r' ~ retval.cnt,'STR' ~ (retval.wordsz==1 ?? '' !! '2')];
#     } elsif retval ~~ NoRetVal {
#         []
#     } else {
#         die 'Unsupported return value: ' ~ retval.raku;
#     }
# }
# # args are either values or regs, in which case they are LDR
# sub __emit_arg(Token \token_ --> List) {

#     if %const_operand_types{token_.type}:exists
#         or %labels_padding{token_.type}:exists
#         or token_ ~~ FunctionAddr
#     {
#         return [ __emit_value(token_) ];
#     }
#     elsif  token_ ~~ BytePair {
#               return [ __emit_value(token_.fst), __emit_value(token_.snd),];
#     # } elsif token_ ~~ ( Reg | FunctionReg ) {
#     #     if token_.idx ~~ Just {
#     #         # so it must be a byte
#     #         my \extra_op = token_.idx.just == 0 ?? 'NIP' !! 'POP';
#     #         return [ token_.type ~ token_.cnt, 'LDR2 ' ~ extra_op ];
#     #     } else {
#     #         return [ token_.type ~ token_.cnt , 'LDR' ~ (token_.wordsz==1 ?? '' !! '2') ];
#     #     }
#     # } elsif token_ ~~ GlobReg {
#     #     if token_.idx ~~ Just {
#     #         # so it must be a byte
#     #         my \extra_op = token_.idx.just == 0 ?? 'NIP' !! 'POP';
#     #         return [ token_.type ~ token_.cnt, 'LDA2 ' ~ extra_op ];
#     #     } else {
#     #         return [ token_.type ~ token_.cnt , 'LDA' ~ (token_.wordsz==1 ?? '' !! '2') ];
#     #     }
#     } else {
#         die "__emit_arg: unhandled token {token_.raku}";
#     }

# }

sub __emit_value(Token \token_ --> Str) {
        if token_ ~~ RawHex {
            token_.val
        }
        elsif token_ ~~ RawAsciiChar {
            "'" ~ token_.val
        }
        elsif token_ ~~ RawAsciiWord {
            '"' ~ token_.val
        }
        elsif token_ ~~ Literal {
            'LIT' ~ (token_.wordsz ==2 ?? 2 !!'') ~ (token_.rs ?? 'r' !! '') ~ ' ' ~
            join( ' ' ,map(&__emit_value,|token_.tokens));
        }
        elsif %const_operand_types{token_.type}:exists
        or %labels_padding{token_.type}:exists
        {
            token_.type ~ token_.val;
        }
        elsif token_ ~~ FunctionAddr {
            ' [ ;' ~ token_.val~ ' ] ';
        }
        # elsif token_ ~~ ( Reg | FunctionReg ) {
        #    ' [ '~ __emit_arg(token_).join( ' ' ) ~ ' ] ';
        # }
        elsif token_ ~~ Operation  {
           token_.opcode.Str ;
        }
        else {
            die 'Unsupported token: ', token_.raku;
        }
}
sub __emit_opcode(Token \token_ --> Str) {
    token_.opcode.name
    ~ ( token_.opcode.wordsz == 1 ?? '' !! '2')
    ~ (token_.opcode.keep ?? 'k' !!'' )
    ~ (token_.opcode.rs ?? 'r' !! '')
    ;
}
