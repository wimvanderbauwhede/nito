use v6;
unit module UxntalParser:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;
use UxntalDefs;
use UxntalTypes;

our $DBG = False;
our $DBG_TAL = False;
sub parse_program_tokens( Seq \tokens_ --> Array) is export {
    say "-" x 80,"\n",&?ROUTINE.name if $DBG;
    my Hash \relative_label_address = {};
    my Array \parsed_program_tokens=[];
    my $i=0;
    my  $skip = 0;
    my @tokens = tokens_;
    for @tokens -> Str \token_str {
        next if token_str eq '';
        say "parse_program_tokens TOKEN: {token_str}" if $DBG ;

        my Str \opcode = substr(token_str,0,3);

        if $skip { $skip--; ++$i; next;}
        my (Token \token_,Int \skip_) = _parse_program_token(token_str,@tokens,$i);
        $skip=skip_;
        push parsed_program_tokens,token_ unless token_ ~~ NoToken;
        $i++;
    }
    # First change const jumps to rel label jumps
    my List \parsed_program_tokens_ = _change_const_jumps_to_rel_label_jumps(parsed_program_tokens);
    # Change all relative labels to absolute ones
    my List \parsed_program_tokens_no_rel = _change_rel_labels_to_abs(parsed_program_tokens_);

    return parsed_program_tokens_no_rel;
} # END of parse_program_tokens


sub _parse_program_token( Str \token_str, @tokens, $i --> List) {
    say "-" x 80,"\n",&?ROUTINE.name if $DBG;
    # my Hash \relative_label_address = {};

    my  $skip = 0;
    # my $eval= False;
    my Str \opcode = substr(token_str,0,3);

    my Token \token_ = do {
        # if $eval {
        #     # This does assume #00 STR <token_>
        #     $eval= False;
        #     LoadOpcode[$i-2,''].new;
        # }
        if token_str ~~ /^ '#' $<val> = [ <xdigit>+ ] /  {
            my $val = $<val>.Str;
            my $ival=:16($val).Int;
            if  $ival == 0 and @tokens[$i+1] eq 'STR' {
                # # Maybe this should simply be EvalOpcode() with $skip=2
                # die '#00 STR is interpreted as staging an eval()';
                # $eval=True;
                # $skip=1;
                # StoreOpcode[$i,''].new;
                $skip=2;
                EvalOpcode.new;
            } else {
                HexLiteral[$val, chars($val)/2,False].new;
            }
        } elsif token_str ~~ /^ '.' $<val> = [ [ \w | '-' | '/' ]+ ] / { # Zero-page address reference
            my $val=$<val>.Str ;
            LitZeroPageAddr[$val].new;
        } elsif token_str ~~ /^ ';' $<val> = [ [ \w | '-' | '/' ]+ ] / { # Main memory address reference
            my $val=$<val>.Str;
            # if the address is a function, i.e. if the next opcode is JSR or JMP, then we need to generate and LLVM function
            # we relegate the check to the JSR/JMP call
            LitAbsAddr[$val,2].new;
        } elsif token_str ~~ /^ ';&'  $<val> = [ [ \w | '-' ]+ ] / { # Relative address reference to a relative label
            my $val=$<val>.Str ;
            LitAbsChildAddr[$val,2].new;
        } elsif token_str ~~ /^ ',&'  $<val> = [ [ \w | '-' ]+ ] / { # Relative address reference to a relative label
            my $val=$<val>.Str ;
            LitRelChildAddr[$val].new;
        } elsif token_str ~~ /^ ',' $<val> = [ [ \w | '-' ]+ ] / { # Relative address reference to an absolute
            my $val=$<val>.Str ;
            LitRelAddr[$val].new;
        } elsif token_str ~~ /^ '$' $<val> = [ [ \w | '-' ]+ ] / { # Relative padding
            my $val=$<val>.Str ;
            RelPadding[$val].new;
        } elsif token_str ~~ /^ '|' $<val> = [ [ \w | '-' ]+ ] / { # Absolute padding
            my $val=$<val>.Str ;
            AbsPadding[$val].new;
        } elsif token_str ~~ /^ '&' $<val> = [ [ \w | '-' ]+ ] / { # Relative address label
            my $val=$<val>.Str ;
            # relative_label_address{$val}=$i;
            ChildLabel[$val].new;
        } elsif token_str ~~ /^ '@' $<val> = [ [ \w | '-' | '/']+ ] / { # Absolute address label
            my $var=$<val>.Str ;
            AbsLabel[$var].new;
        } elsif token_str ~~ /^ ':' $<val> = [ [ \w | '-' ]+ ] / { # Raw absolute address label
            my $var=$<val>.Str ;
            RawAbsAddr[$var].new;
        } elsif token_str ~~ / '"' $<val> = [ .+ ]/ {
            my $val=$<val>.Str ;
            RawAsciiWord[$val,$val.chars].new;
        } elsif token_str ~~ / "'" $<val> = [ .+ ]/ {
            my $val=$<val>.Str ;
            RawAsciiChar[$val,1].new;
        } elsif token_str ~~ /^ 'LIT' / {
            my Bool \rs = token_str ~~ /r/  ?? True !! False;
            $skip = 1;

            if token_str ~~ /^ 'LIT2' / { # LIT2 and LIT2r
                my $val = @tokens[$i+1];
                my (Token \token_1,\_) = _parse_program_token($val,[],0);

                if token_1 ~~ HexLiteral and token_1.wordsz==2 { # LIT2 v
                    die 'WHY? Should be RawHex!';
                    if rs {
                        $skip = 1;
                        Literal[[token_1], token_1.wordsz,rs].new;
                    } else {
                        token_1
                    }
                } elsif token_1 ~~ ChildLabel or token_1 ~~ AbsLabel { # LIT2 l1
                    my $val2 =  @tokens[$i+2];
                    my (Token \token_2,\_) = _parse_program_token($val2,[],0);
                    if token_2.wordsz == 2 { # LIT2 l1 v1
                        $skip = 2;
                        Literal[[token_1,token_2], 2,rs].new;
                    } else {
                        my $val3 =  @tokens[$i+3];
                        my (Token \token_3,\_) = _parse_program_token($val3,[],0);
                        if token_3 ~~ ChildLabel or token_3 ~~ AbsLabel { # LIT2 l1 v1 l2
                            # Look for token_4
                            my $val4 =  @tokens[$i+4];
                            my (Token \token_4,\_) = _parse_program_token($val4,[],0);
                            if token_4.wordsz == 1 { # LIT2 l1 v1 l2 v2
                                $skip = 4;
                                Literal[[token_1,token_2,token_3,token_4], 2,rs].new;
                            } else {
                                die "Can't handle  LIT2 {token_1.Str} {token_2.Str} {token_3.Str} {token_4.Str}";
                            }
                        } elsif token_3.wordsz == 1 {
                            $skip = 3;
                            Literal[[token_1,token_2,token_3], 2,rs].new;
                        } else {
                            die "Can't handle  LIT2 {token_1.Str} {token_2.Str} {token_3.Str}";
                        }
                    }
                } else {
                    if token_1.wordsz == 2 { # LIT2 v
                        $skip = 1;
                        Literal[[token_1], 2,rs].new;
                    } else { # LIT2 v1
                        my $val2 =  @tokens[$i+2];
                        my (Token  \token_2,\_) = _parse_program_token($val2,[],0);

                        if token_2 ~~ ChildLabel or token_2 ~~ AbsLabel {
                            $skip = 3;
                            # LIT2 v1 l1 v2
                            # Look for token_4
                            my $val3 =  @tokens[$i+3];
                            my (Token \token_3,\_) = _parse_program_token($val3,[],0);
                            if token_3.wordsz == 1 {
                                $skip = 3;
                                Literal[[token_1,token_2,token_3], 2,rs].new;
                            } else {
                                die "Can't handle  LIT2 {token_1.Str} {token_2.Str} {token_3.Str}";
                            }
                        } elsif token_2.wordsz == 1 { # LIT2 v1 v2
                            $skip = 2;
                            Literal[[token_1,token_2], 2,rs].new;
                        } else {
                            die "Can't handle  LIT2 {token_1.Str} {token_2.Str}";
                        }
                    }
                }
            } else { # LIT and LITr
            my $val = @tokens[$i+1];
            my (Token \token_1,\_) = _parse_program_token($val,[],0);
                $skip = 1;
                if token_1 ~~ HexLiteral {
                    die 'WHY? Should be RawHex!';
                    if rs {
                        $skip=1;
                        Literal[[token_1], token_1.wordsz,rs].new;
                    } else {
                        token_1
                    }
                } elsif token_1 ~~ ChildLabel or token_1 ~~ AbsLabel {
                    my $val2 =  @tokens[$i+2];
                    $skip = 2;
                     my (Token \token_2,\_) = _parse_program_token($val2,[],0);
                     Literal[[token_1,token_2], 1,rs].new;
                } else {
                    # I make this 2 bytes by default but it really depends on the alloc after it
                    my Int \wordsize = token_str ~~ /2/ ?? 2 !! 1;
                    # I guess 'keep' makes no sense
                    # my Bool \keep = token_str ~~ /k/ ?? True !! False;
                    $skip=1;
                    Literal[[token_1], wordsize,rs].new;
                }
            }

            # my $val = @tokens[$i+1];
            # my (Token \token__,\_) = _parse_program_token($val,[],0);

            #     if token__ ~~ HexLiteral {
            #         if rs {
            #             Literal[token__, token__.wordsz,rs].new;
            #         } else {
            #             token__
            #         }
            #     } else {
            #         # I make this 2 bytes by default but it really depends on the alloc after it
            #         my Int \wordsize = token_str ~~ /2/ ?? 2 !! 1;
            #         # I guess 'keep' makes no sense
            #         # my Bool \keep = token_str ~~ /k/ ?? True !! False;
            #         Literal[token__, wordsize,rs].new;
            #     }


        } elsif %operations{opcode}:exists {
            my Int \wordsize = token_str ~~ /2/ ?? 2 !! 1;
            my Bool \keep = token_str ~~ /k/ ?? True !! False;
            my Bool \rs = token_str ~~ /r/  ?? True !! False;
            my (Int \nargs, Int \res ) = %operations{opcode};
            my Opcode \opcode_tup=Opcode[opcode,wordsize,keep,rs].new;
            Operation[nargs,res,opcode_tup].new;
        } elsif token_str ~~ /$<val> =[<xdigit>+]/ {
            RawHex[token_str,chars($<val>.Str)/2].new;
        } elsif  token_str eq '[' or token_str eq ']' {
            NoToken.new;
        } else {
            die "Unknown token: <{token_str}>";
            Problem.new;
        }
    };

    [token_,$skip]
} # END of _parse_program_token



sub _change_rel_labels_to_abs(List \parsed_tokens --> List ) {
    my (\state, \rewritten_tokens) = reduce(
        -> \acc, \token_ {

            my \current_label = acc[0]<current-label>;
            my \prev_token = acc[0]<prev-token>;
            if token_ ~~ AbsLabel { #'@'
                acc[0]<current-label> = token_.val;
                push acc[1], token_;
            }
            elsif token_ ~~ ChildLabel { #'&'
                push acc[1], AbsLabel[current_label ~ '/' ~ token_.val].new;
            }
            elsif token_ ~~ LitRelAddr { #','
                push acc[1], LitAbsAddr[ token_.val, 2].new;
            }
            elsif token_ ~~ LitAbsChildAddr { #';&'
                push acc[1], LitAbsAddr[current_label ~ '/' ~ token_.val,2].new;
            }
            elsif token_ ~~ LitRelChildAddr { #',&'
                push acc[1], LitAbsAddr[current_label ~ '/' ~ token_.val,2].new;
            }
            elsif token_ ~~ LitAbsAddr { #';'
                push acc[1], token_;
            }
            elsif token_ ~~ Literal {
                if token_.tokens.elems == 1 {
                    push acc[1], token_;
                } else {
                    my @tokens_ =  map ->\t_ { #'&' to '@'
                        t_ ~~ ChildLabel ?? AbsLabel[current_label ~ '/' ~ t_.val].new !! t_;
                    }, |token_.tokens;
                    push acc[1], Literal[@tokens_,token_.wordsz,token_.rs].new;
                }
                # if token_.token ~~ ChildLabel {
                #     push acc[1], Literal[AbsLabel[current_label ~ '/' ~ token_.token.val].new,token_.wordsz,token_.rs].new;
                # } else {
                #     push acc[1], token_;
                # }
            }
            # elsif token_ ~~ StoreOpcode {
            #     push acc[1], StoreOpcode[token_.id,current_label].new;
            # }
            # elsif token_ ~~ LoadOpcode {
            #     push acc[1], LoadOpcode[token_.id,current_label].new;
            # }
            elsif token_ ~~ Operation { # JMP JSR JCN STR LDR
                if %jmp_operations{token_.opcode.name}:exists  {
                    # die prev_token.raku, token_.raku if prev_token ~~ HexLiteral;
                    my \wordsize =
                    (token_.opcode.name eq 'JMP' and token_.opcode.rs) ?? 2
                    !! prev_token ~~ Operation
                    ?? (prev_token.opcode.name eq 'JSR' and prev_token.opcode.wordsz == 2 )
                        ?? token_.opcode.wordsz
                        !! ((%cmp_operations{prev_token.opcode.name}:exists)
                            ?? 1
                            !! prev_token.opcode.wordsz )
                    !! prev_token ~~ HexLiteral
                    ?? prev_token.wordsz
                    !! 2; # WV: bit of a hack
                    # die wordsize if prev_token ~  ~ Operation and prev_token.opcode.name eq 'NEQ';
                    my \opcode = Opcode[token_.opcode.name, wordsize, token_.opcode.keep, token_.opcode.rs ].new;
                    push acc[1], Operation[ token_.nargs, token_.nres,opcode].new
                }
                elsif token_.opcode.name eq 'STR' {
                    # my \opcode_ = Opcode['STA', 2, token_.opcode.keep, token_.opcode.rs ].new;
                    # push acc[1], Operation[ token_.nargs, token_.nres,opcode_].new
                    my \opcode_ = token_.opcode.clone(name=>'STA');
                    push acc[1], token_.clone(opcode=>opcode_);
                }
                elsif token_.opcode.name eq 'LDR' {
                    my \opcode_ = token_.opcode.clone(name=>'LDA');
                    # Opcode['LDA', 2, token_.opcode.keep, token_.opcode.rs ].new;
                    push acc[1], token_.clone(opcode=>opcode_);
                    # Operation[ token_.nargs, token_.nres,opcode].new
                } else {
                    push acc[1], token_;
                }
            }
            else {
                push acc[1], token_;
            }
            acc[0]<prev-token> = token_;
            acc
        },
        [{
            current-label=>'on-reset',
            prev-token => NoToken
        },[]],
        |parsed_tokens
    );
    return rewritten_tokens;
} # END of _change_rel_labels_to_abs

sub split_program_into_blocks( List \tokens --> TokenisedProgram ) is export {
    # die 'need an extra check has-ops to decide to add a jump or not';
    say "=" x 80,"\n",&?ROUTINE.name if $DBG;

    my TokenisedProgram \tokenised_program = reduce(
        -> \tp_,\token_ {
            # This is a bit kludgy
            # But if there is no zero page used, there is no point in creating a block for it.
            my $skip = False;
            # say token_.raku;
            # say "{tp_.block-label} JMP: {tp_.has-jump} BRK: {tp_.has-brk}";  #, (not tp_.blocks{tp_.block-label}:exists),token_.raku;
            if  token_ ~~ AbsPadding and token_.val ~~ /^ <xdigit> <xdigit> $/ and
            (tp_.block-label eq '0000' or tp_.block-label ~~ /^ <xdigit> <xdigit> $/)
            {

                # So these are paddings before |0000, I assume they are device space
                tp_.device-mem = True;
                tp_.zero-page = False;
                tp_.main = False;
                tp_.block-label = token_.val;
                # tp_.blocks{tp_.block-label}=TokenisedCodeBlock.new;
                tp_.blocks{tp_.block-label}=TokenisedCodeBlock.new(block-type=>DeviceMem.new);
                push tp_.blocks-sequence, tp_.block-label;
            }
            # say (tp_.block-label  eq '0000' ), (not tp_.blocks{tp_.block-label}:exists),token_.raku;
            elsif  (
                    tp_.block-label ~~ /^ <xdigit> <xdigit> $/
                    and  ( token_ ~~ AbsPadding and token_.val eq '0000'  )
                ) or
                (
                    tp_.block-label eq '0000' and not tp_.blocks{tp_.block-label}:exists
                    and
                    ( ( token_ ~~ AbsPadding and token_.val eq '0000'  ) or ( token_ ~~ AbsLabel))
                )
            {
                tp_.device-mem = False;
                tp_.zero-page = True;
                tp_.main = False;
                tp_.block-label = token_.val;
                tp_.blocks{tp_.block-label}=TokenisedCodeBlock.new(block-type=>ZeroPage.new );
                push tp_.blocks-sequence, tp_.block-label;
            }
            # The main program
            if token_ ~~ AbsPadding and token_.val eq '0100'  {
                tp_.zero-page = False;
                tp_.device-mem = False;
                tp_.main = True;
                tp_.block-label = '0100';
                tp_.blocks{tp_.block-label}=TokenisedCodeBlock.new;
                push tp_.blocks-sequence, tp_.block-label;
            # I want to treat all labels als functions
            # If a block does not end in a jump, I should just add one.
            # so I need a has-jump and a prev-label
            } elsif token_ ~~ AbsLabel {
                if not tp_.zero-page and not tp_.device-mem
                {

                    # Add a jump to the previous block
                    # if the previous block was empty, just mark it as such
                    if tp_.blocks{tp_.block-label}.tokens.elems == 1 {
                        tp_.blocks{tp_.block-label}.block-type = EmptyBlock.new;

                    }
                    elsif
                    # A function must have operations or be empty
                    # We only add a jump if the last token is not a jump or break
                    # It might be safer to add this in any case.
                    # All that can happen is that it is unreachable.
                    (tp_.main or tp_.has-ops )
                    and (
                        # If the final token is not a BRK or JMP, we need to add a JMP to the next function
                        # I don't think it can't be a LIT as that would have become a Literal
                            (not
                            (tp_.blocks{tp_.block-label}.tokens[*-1] ~~ Operation
                            and tp_.blocks{tp_.block-label}.tokens[*-1].opcode.name eq ( 'JMP' | 'BRK' ) )
                            )
                        or
                        # If the final token is a JMP2r or BRK, but the preceding token is a JMP, we also need to add an extra JMP to the next function
                            (
                            tp_.blocks{tp_.block-label}.tokens[*-1] ~~ Operation
                            and tp_.blocks{tp_.block-label}.tokens[*-2] ~~ Operation
                            and tp_.blocks{tp_.block-label}.tokens[*-2].opcode.Str eq 'JMP'
                            and tp_.blocks{tp_.block-label}.tokens[*-1].opcode.Str eq ( 'JMP2r' | 'BRK' )
                            )
                        )
                    {
                        if not $DBG_TAL {
                        my \next_block_label = LitAbsAddr[token_.val,token_.wordsz].new;
                        push tp_.blocks{tp_.block-label}.tokens, next_block_label;
                        tp_.blocks{tp_.block-label}.block-type = TailCall.new;
                        tp_.blocks{tp_.block-label}.is-function = True;
                        tp_.token-addr++;

                        my \jmp_token = Operation[
                            1, 0, Opcode[
                                'JMP', 2,False,False
                            ].new
                        ].new;
                        push tp_.blocks{tp_.block-label}.tokens, jmp_token;
                        tp_.token-addr++;
                        }

                    }

                    tp_.has-ops = False;
                    tp_.has-jump = False;
                    tp_.has-brk = False;
                    tp_.in-block = True;
                    tp_.block-label = token_.val;
                    tp_.blocks{tp_.block-label}=TokenisedCodeBlock.new(block-type=>Data.new);
                    push tp_.blocks-sequence, tp_.block-label;

                }
            } elsif token_ ~~ LitAbsAddr {
                tp_.has-ops = True;
            } elsif token_ ~~ HexLiteral {
                tp_.has-ops = True;
            } elsif token_ ~~ Literal {
                tp_.has-ops = True;
            } elsif token_ ~~ Operation {
                tp_.has-ops = True;
                if token_.opcode.name eq 'JMP' {
                    # and not tp_.main {
                    tp_.has-jump = True; # means it must be a tail call or return call
                    tp_.blocks{tp_.block-label}.is-function = True;
                    tp_.blocks{tp_.block-label}.block-type =
                    token_.opcode.rs
                    ?? PlainFunction.new
                    !! TailCall.new;
                }
                # BRK means end of main
                elsif token_.opcode.name eq 'BRK'  {
                        tp_.blocks{tp_.block-label}.block-type = Exit.new;
                        tp_.blocks{tp_.block-label}.is-function = True;
                        $skip = True; # Means it must be an exit call
                        if tp_.main {
                            tp_.main = False;
                        }
                        tp_.has-brk = True;
                        # if not tp_.main {
                        #     $skip = True;
                        # } else {
                        #     tp_.main = False;
                        # }
                }
            } elsif token_ ~~ AbsPadding and not tp_.zero-page and not tp_.device-mem {
                tp_.has-ops = False;
                tp_.block-label= token_.val;
                tp_.blocks{tp_.block-label}=TokenisedCodeBlock.new;
                push tp_.blocks-sequence, tp_.block-label;
            }
            if not $skip or token_.opcode.name eq 'BRK' {
                # say tp_.block-label;
                push tp_.blocks{tp_.block-label}.tokens, token_;
                tp_.token-addr++;
            }
            tp_;
        },
        TokenisedProgram.new,
        |tokens
    );
    # tokenised_program.print;
    return _identify_empty_blocks(tokenised_program);
} # END of split_program_into_blocks

sub _identify_empty_blocks(\tp_) {

    for 0 .. tp_.blocks-sequence.elems-1 -> \idx {
        my \block_label = tp_.blocks-sequence[idx];
        if tp_.blocks{block_label}.block-type ~~ EmptyBlock {
            my $idx_ = idx+1;
            while tp_.blocks{tp_.blocks-sequence[$idx_]}.block-type ~~ EmptyBlock {
                ++$idx_;
            };
            for idx .. $idx_-1 -> \idx__ {
                my \block_label_ = tp_.blocks-sequence[idx__];
                if tp_.blocks{tp_.blocks-sequence[$idx_]}.block-type ~~ Data {
                    tp_.blocks{block_label_}.block-type = Data.new;
                } else {
                    tp_.blocks{block_label_}.block-type = TailCall.new;
                    tp_.blocks{block_label_}.is-function = True;

                    my \token_ = tp_.blocks{ tp_.blocks-sequence[$idx_]}.tokens[0];
                    my \next_block_label = LitAbsAddr[token_.val,token_.wordsz].new;
                    push tp_.blocks{block_label_}.tokens, next_block_label;

                    my \jmp_token = Operation[
                        1, 0, Opcode[
                            'JMP', 2,False,False
                        ].new
                    ].new;
                    # push tp_.blocks{block_label_}.tokens,
                    push tp_.blocks{block_label_}.tokens, jmp_token;
                }
            }
        }
    }
    tp_;
} # END of _identify_empty_blocks

# I'm transforming constant jumps to labeled jumps first


# if HexLiteral JMP then:
# - Create a rel label ',&label' ~ hexlit.val
# - for all tokens after JMP:
# - ignore if rel label
# - if padding, add to count
# - if raw, add count to
# - if LIT x, add x.wordsz to count

# Basically, if anything, add to count; if count == hexlit.val, insert a label '&label' ~ hexlit.val
sub _change_const_jumps_to_rel_label_jumps(\parsed_tokens) {
    my $prev_token=NoToken;
    my $skip=False;
    my $jmp_dist = 0;
    my $calc_jmp=False;
    my $hexlit=0;
    my @updated_tokens;
    my $jmp_str='';
    my $cnt=0;
    for 0 .. parsed_tokens.elems-1 -> \idx {
         $skip=False;
        my \token_ = parsed_tokens[idx];
        my \next_token_ = parsed_tokens[idx+1];
        if token_ ~~ HexLiteral and next_token_ ~~ Operation and next_token_.opcode.Str eq ( 'JMP' | 'JCN' | 'JSR' ) { # JSR is stretching it but who cares
        $jmp_str = next_token_.opcode.Str;
            $hexlit = token_.val;
            push @updated_tokens, LitRelChildAddr[ 'label' ~ $cnt    ].new;
            $jmp_dist = :16($hexlit).Str;
            $skip=True;
        } elsif token_ ~~ Operation and token_.opcode.Str eq $jmp_str and idx>0 and parsed_tokens[idx-1] ~~ HexLiteral {
            $calc_jmp=True;
            # say parsed_tokens[idx-1].Str~ ' ' ~parsed_tokens[idx].Str~ ' '~parsed_tokens[idx+1].Str;
        } elsif $calc_jmp {

            if token_ ~~ Operation {
                $jmp_dist -= 1;
                # say token_.Str, ' => ' , $jmp_dist;
            }
            elsif not (token_ ~~ AbsLabel or token_ ~~ ChildLabel) {
                $jmp_dist -= token_.wordsz;
                if token_ ~~ HexLiteral {
                    $jmp_dist-=1; # WV: FIXME: this assumes that #xxxx is 3 bytes and #xx #yy is 4 bytes
                    # In practice the assembler will make the latter also 3 bytes.
                    # But if they can be lazy, so can I
                }
            }
                # say token_.Str, ' => ' , $jmp_dist;
                if $jmp_dist < 0 {
                    $calc_jmp=False;
                    push @updated_tokens, ChildLabel['label' ~ $cnt].new;
                    $cnt++;
                    # say @updated_tokens[*-2].Str;
                    # say @updated_tokens[*-1].Str;
                    # say token_.Str;
                    # say parsed_tokens[idx-1].Str~ ' '~parsed_tokens[idx].Str~ ' '~parsed_tokens[idx+1].Str;die;
                }

        }
        push @updated_tokens, token_ unless $skip;

    }
    @updated_tokens;

} # END of _change_const_jumps_to_rel_label_jumps

