use v6;
unit module UxntalTokeniser:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;

sub tokenise_program(Str \prog_str --> Seq) is export {
    my $tal_str = prog_str;
    # Loop here
    while ($tal_str ~~ / "'" '(' /) {
        $tal_str ~~ s/ "'" '('  /QUOTED_OPEN_PAREN/
    }
    while ($tal_str ~~ / "'" ')' /) {
        $tal_str ~~ s/ "'" ')'  /QUOTED_CLOSE_PAREN/
    }

    while ($tal_str ~~ /\(.+\)/) {
        $tal_str ~~ s/ '(' <-[ \( \) ] >+ ')' //
    }
    while ($tal_str ~~ /QUOTED_OPEN_PAREN/) {
        $tal_str ~~ s/QUOTED_OPEN_PAREN/'(/
    }
    while ($tal_str ~~ /QUOTED_CLOSE_PAREN/) {
        $tal_str ~~ s/QUOTED_CLOSE_PAREN/')/
    }

    my Seq \tokens = $tal_str.split(/[ \s | \n ]+/);

    my Seq \renamed_tokens = map {  S/ $<reg> = [< ,&r &r @rr ;rr >] $<cnt> = [\d+]$ /$<reg>_$<cnt>/ }, tokens;
    my Seq \renamed_tokens_ = map {  S/main/_main/ }, renamed_tokens;

    # die renamed_tokens.raku;
    my Seq \r_tokens = grep ->\t {t ne ''}, renamed_tokens_;
    # say @r_tokens.raku;
    return r_tokens;
}
