use v6;
unit module UxntalInliner:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;
use UxntalTypes;
use UxntalDefs;
use ImmutableDatastructureHelpers;

our $DBG = False;

# This relies on the UxnTalAnalyser Leaf/NonLeaf analysis and the FunctionAddr rewrite (_identify_function_addrs), currently also in UxnTalAnalyser, and on _loop_detection which marks routines as inlineable.

=begin pod
I have two categories: Leaf and Non-leaf. The former is always inlineable, the latter can be inlineable or not.
If a function is marked as not inlineable, what it means is that this function should be called instead of inlined.
But that function itself could still contain calls that could be inlined.
What I need to avoid is that I go into a loop.
A possible way is to look at all Leaf functions, and for each of them, look at their callers, and inline the Leaf into the caller.
Then we remove that caller from the callers of that Leaf.
If the caller is not marked as not-inlineable, it becomes Leaf itself. So we keep those new Leaves in a separate list and do this until that list is empty.

=end pod

sub inline_subroutine_calls (ParsedProgram \ppr --> ParsedProgram ) is export {
    # temp $DBG = True;
    say "=" x 80,"\n",&?ROUTINE.name if $DBG;
# - Get all the Leaf functions
    my List \leaves = [|grep ->\f {
        # say f;
        # (f ne '0100') and
        (ppr.blocks{f}.node-type ~~ LeafFunction )
        }, |ppr.blocks-sequence];
        say "\n";
    # die leaves;
    _recursive_inline(ppr,leaves);
    # die if $DBG;
} # END of inline_subroutine_calls

sub _recursive_inline(ParsedProgram \ppr,List \leaves --> ParsedProgram ) {
    if leaves.elems == 0  {
        return ppr;
    } else {
        my (\leaf,\leaves_) = headTail(leaves);
        say "\nInlining {leaf} in all callers" if $DBG;
        # - Loop over them and get their callers
        my Map \leaf_callers = ppr.callers{leaf};
        # map ->\f {  say "{f}: ",ppr.blocks{f}.code.head,';',ppr.blocks{f}.code.elems  },|ppr.blocks-sequence;
        # - Loop over the callers and inline the Leaf function into them
        my (\ppr_inlined_leaf,\leaves__) = reduce(
            ->\ppr_new_leaves,\leaf_caller {
                my (\ppr_,\new_leaves) = ppr_new_leaves;
                my \pb_ = _inline_subroutine_call(ppr_,leaf,leaf_caller);
                # add new Leaves to the new_leaves list
                my \new_leaves_ = pb_.node-type ~~ LeafFunction ?? append(new_leaves,leaf_caller) !! new_leaves;
                # put pb_ into blocks and blocks into ppr
                say "Inserting the inlined block for {leaf_caller}" if $DBG;
                my Map \blocks_ = insert(ppr_.blocks,leaf_caller,pb_);
                # - Then we remove that caller from the callers of that Leaf;
                my Map \leaf_callers_ =  remove(leaf_callers,leaf_caller);
                my \remove_leaf = False; #leaf_callers_.keys.elems == 0 ;
                my Map \ppr_callers_ = insert(ppr.callers,leaf,leaf_callers_);
                my \blocks_sequence_ = remove_leaf ?? [|grep ->\f { f ne leaf },|ppr_.blocks-sequence] !! ppr_.blocks-sequence;
                [ppr_.clone(blocks=>blocks_,callers=>ppr_callers_,blocks-sequence=>blocks_sequence_), new_leaves_];
            },
            [ppr, leaves_],
            |leaf_callers.keys.sort
        );
        _recursive_inline(ppr_inlined_leaf,leaves__)
    }
} # END of _recursive_inline

sub _inline_subroutine_call(ParsedProgram \ppr,Str \leaf,Str \leaf_caller --> ParsedCodeBlock) {
    say "Inlining {leaf} in caller {leaf_caller}" if $DBG;
    # This is another reduction, over all tokens in leaf_caller
    my \pb = ppr.blocks{leaf_caller};
    # die pb.code.raku if leaf_caller eq 'print/short';
    my (\_,\inlined_code) = reduce(
        ->\acc,\token_ {
            my (\prev_token, \inlined_code_) = acc;
            # So if we find a jump with a FunctionAddr that is leaf, we inline it.
            # say "{leaf_caller}: {token_}";
            if token_ ~~ Operation and token_.opcode.name eq ( 'JCN_OFF' | 'JSR' | 'JMP' ) and token_.Str ne 'JMP2r'
                and prev_token ~~ FunctionAddr and prev_token.val eq leaf {
                    my \jmp = token_.opcode.name;
                    say "Found call to {leaf} in {leaf_caller}, inlining" if $DBG;
                # Inline
                my List \leaf_code = ppr.blocks{leaf}.code;
                # The init() is to remove the prev_token;
                # The current token is a JMP, JSR or JCN. A JCN is different. If we want to replace the call in the JCN by an inline, that would work in C but not in Tal. So let's first see if it works when we ignore JCN.
                # If the call to be inlined is the tail call, we should not remove the final return.

                my \leaf_code_ = ((jmp ne 'JMP') and (top(leaf_code).Str eq ( 'BRK' | 'JMP2r'))) ?? init(leaf_code) !! leaf_code;
                my List \inlined_code__ = List.new(|init(inlined_code_),|tail(leaf_code_));
                # say "Inlined code for {leaf_caller}";
                # map ->\t {say t.Str},|inlined_code__;
                [token_,inlined_code__]
            } else {
                # Just append the token
                my List \inlined_code__ =  List.new(|inlined_code_,token_);
                [token_,inlined_code__]
            }
        },
        [NoToken.new,[]],
        |pb.code
    );
    # map ->\t {say t.Str},|pb.code;say '';
    # map ->\t {say t.Str},|inlined_code;
    # If the caller is not marked as not-inlineable, it becomes Leaf itself.
    # I think this is wrong: it is only correct if there are no jumps left.
    my \jumps_left = grep ->\t { t ~~ Operation and t.opcode.name eq ( 'JCN' | 'JSR' | 'JMP' ) and t.Str ne 'JMP2r' }, |inlined_code;
    my Bool \is_now_leaf = (jumps_left.elems == 0) && (leaf_caller ne '0100');
    if pb.inlineable ~~ Just {
        say "{leaf_caller} is now " ~ ((is_now_leaf and pb.inlineable.just) ?? LeafFunction.new !! pb.node-type) if $DBG;
        my \pb__ = pb.clone(code=>inlined_code, node-type => ((is_now_leaf and pb.inlineable.just) ?? LeafFunction.new !! pb.node-type) );
        pb__;
    } else {
        say "{leaf_caller} is still unknown" if $DBG;
        pb;
    }

} # END of _inline_subroutine_call

=begin pod
For every block
	If the function is NonLeaf
set a flag non-leaf to False
	Go through the tokens, tracking the previous token
		If the token is a jump, and the prev token is a label for a called function
			if the called function is Leaf, inline it. Else descend into the called function, and check if it is Leaf on return. If not, break with a non-leaf flag set to True
			if we reach the end of the block and the non-leaf flag is still False, set the function to Leaf
Because of the recursion, we need to record if a function has already been visited. But we need to go through the sequence as well because the interrupt handlers are not called from anywhere.

The above needs an additions to detect loops, see the blocktype detection code or better, the code use in preparation of argument inference.

If we find a recursive chain, we can inline any intermediate subroutines into the initial recursive subroutines, but the final call to this routine can't be inlined.

TODO!

=end pod
