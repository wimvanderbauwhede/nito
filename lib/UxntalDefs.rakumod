#!/usr/bin/env raku
use v6;

# Just a hack to provide version information with the same syntax as the other modules
role UxntalDefs:ver<1.1.0>:auth<codeberg:wimvanderbauwhede> {};

our @opcodes = <
 LIT
 INC
 POP
 NIP
 SWP
 ROT
 DUP
 OVR
 EQU
 NEQ
 GTH
 LTH
 JMP
 JCN
 JSR
 STH
 LDZ
 STZ
 LDR
 STR
 LDA
 STA
 DEI
 DEO
 ADD
 SUB
 MUL
 DIV
 AND
 ORA
 EOR
 SFT
>;

our %opcodes = map ->\m { state $i=0; "{m}" => $i++ }, @opcodes;
# Global read-only data.
# STZ: 1|2 1 0 LDZ: 1 1|2
# STR: 1|2 1
# STA: 1|2 2
our %mixed_arg_size_opcodes = set < STZ STR DEO SFT JCN >; # Typing this as Set does not work: "Cannot put a type constraint on an 'our'-scoped variable"
our %byte_arg_opcodes = set < LDZ LDR DEI >;

# [#in-args,#out-args]
our %stack_operations =
    POP => [1,0],
    NIP => [2,1],
    SWP => [2,2],
    ROT => [3,3],
    DUP => [1,2],
    OVR => [2,3],
    STH => [1,0] # move between ws and rs
;

# These are the only operations that always return a byte
our %cmp_operations =
    EQU => [2,1],
    NEQ => [2,1],
    GTH => [2,1],
    LTH => [2,1]
;

our %jmp_operations =
    JMP => [1,0],
    JCN => [2,0],
    JSR => [1,0],
;

# TODO: make this a set
our %const_operand_types =
    '#' => 1,
    '.' => 1,# Zero-page address reference
    ';' => 1,# Main memory address reference
    ';f_' => 1,# Function address
    ',&' => 1,# Relative address reference
    ',' => 1,# Relative address reference
    ':' => 1,# Raw absolute address label
;

# TODO: make this a set
our %raw_operand_types =
    'raw'=> 1,
    "'" => 1,
    '"' => 1
;

our %labels_padding =
            '$' # Relative padding
        =>1, '|' # Absolute padding
        =>1, '&' # Relative address label
        =>1, '@'
        =>1,
;

our %mem_operations =
    LDZ => [1,1],
    STZ => [2,0],
    LDR => [1,1],
    STR => [2,0],
    LDA => [1,1],
    STA => [2,0],
;

our %non_stack_operations =

    BRK => [0,0],

    INC => [1,1],

    %cmp_operations,

    %jmp_operations,

    %mem_operations,

    DEO => [2,0],
    DEI => [1,1],

    ADD => [2,1],
    SUB => [2,1],
    MUL => [2,1],
    DIV => [2,1],
    AND => [2,1],
    ORA => [2,1],
    EOR => [2,1],
    SFT => [2,1],
;

our %operations =  %stack_operations, %non_stack_operations;
