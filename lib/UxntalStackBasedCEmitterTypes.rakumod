use v6;

# Just a hack to provide version information with the same syntax as the other modules
role UxntalStackBasedCEmitterTypes:ver<1.1.0>:auth<codeberg:wimvanderbauwhede> {};

our $DBG_ = False;

role CGen {};
role Control {};
role Push[\arg_,\wordsz_,\rs_,\keep_] does CGen {
    has Str $.arg = arg_;
    has Int $.wordsz = Int.new(wordsz_);
    has Bool $.rs = rs_;
    has Bool $.keep = keep_;
    method cgens {
        $.wordsz ==1 ?? PUSH1($.arg,$.rs) !! PUSH2($.arg,$.rs)
    }
    method Str {
        $.wordsz ==1 ?? join("\n",|PUSH1($.arg,$.rs)) !! join("\n",|PUSH2($.arg,$.rs))
    }
}

role Pop[\wordsz_,\rs_,\keep_] does CGen {
    has Bool $.rs = rs_;
    has Bool $.keep = keep_;
    has Int $.wordsz = wordsz_;
    method cgens {
        $.wordsz ==1 ?? POP1($.rs) !! POP2($.rs)
    }
    method Str {
        $.wordsz ==1 ?? join("\n",|POP1($.rs)) !! join("\n",|POP2($.rs))
    }
}

role Get[\arg_,\wordsz_,\rs_,\keep_] does CGen {
    has Str $.arg = arg_;
    has Int $.wordsz = Int.new(wordsz_);
    has Bool $.rs = rs_;
    has Bool $.keep = keep_;
    method cgens {
        $.wordsz ==1 ?? GET1($.arg,$.rs) !! GET2($.arg,$.rs)
    }
    method Str {
        $.wordsz ==1 ?? join("\n",|GET1($.arg,$.rs)) !! join("\n",|GET2($.arg,$.rs))
    }
}

role Assign[\lhs_,\rhs_,\wordsz_] does CGen {
    has Str $.lhs = lhs_;
    has Str $.rhs = rhs_;
    has Int $.wordsz = Int.new(wordsz_);
    method Str {
        'unsigned '~ ($.wordsz==1 ?? 'char' !! 'short') ~ ' ' ~
        "$.lhs = $.rhs;"
    }
}

role Statement[\expr_] does CGen {
    has Str $.expr = expr_;
    method Str {
        $.expr
    }
}

role Protect[\expr_] does CGen {
    has Str $.expr = expr_;
    method Str {
        $.expr
    }
}

role Debug[\expr_] does CGen {
    has Str $.expr = expr_;
    method Str {
        $.expr
    }
}

role Cond[\cond_,\block_] does CGen does Control {
    has Str $.cond = cond_;
    has List $.block = block_;
    method Str {

        'if ( ' ~ $.cond ~ ' ) { '
         ~ (
            $.block.elems>0
            ?? join("\n",map ->\cg {cg.Str},|$.block)
            !! ''
         ) ~
         ' }'
    }
}

role SubCall[\fname_,\args_] does CGen does Control {
    has Str $.fname = fname_;
    has List $.args = args_;
    method Str {
        $.fname~'(' ~ ($.args.elems>0 ?? join( ', ', |$.args)!!'') ~ ');';
    }
}

role CondSubCall[\cond_,\expr_] does CGen does Control {
    has Str $.cond = cond_;
    has CGen $.expr = expr_;
    method Str {
        #  'LOOP?' ~ $.cond.raku ~ $.expr.raku;
        'if ( ' ~ $.cond ~ ' ) { ' ~
        $.expr.Str ~
        'return;' ~
        ' }'
    }
}

role Return[\expr_] does CGen does Control {
    has Str $.expr = expr_;
    method Str {
        $.expr
    }
}

role Comment[\comment_] does CGen {
    has Str $.comment = comment_;
    method Str {
        $.comment
    }
}

sub PUSH1(\v,\rs) {
     my \r = rs ?? 'r' !! '';
    my \st = rs ?? 'r' !! 'w';
    no_protect_debug(
    [
    Statement[ '{'].new,
    Comment[ '/* PUSH1' ~r~ ' */' ].new,
    Debug[ '#ifdef DBG_' ].new,
    Debug[ '    printf("PUSH1 ' ~st~ 'sp:%d\n",u->' ~st~ 'st.ptr);' ].new,
    Debug[ '#endif' ].new,
    Statement[ '    u->' ~st~ 'st.dat[u->' ~st~ 'st.ptr++] = ' ~v~ ';' ].new,
    Protect[ '#ifdef DEFENSIVE' ].new,
    Protect[ 'if (u->' ~st~ 'st.ptr>STACK_SZ) {' ].new,
    Protect[ '    printf("Working stack overflow\n");' ].new,
    Protect[ '    exit(0);' ].new,
    Protect[ '}' ].new,
    Protect[ '#endif' ].new,
    Statement[ '}' ].new
    ]
    );
}


sub PUSH2(\v,\rs) {
    my \r = rs ?? 'r' !! '';
    my \st = rs ?? 'rst' !! 'wst';
    no_protect_debug(
    [
    Statement[ '{' ].new,
    Comment[ '/* PUSH2' ~r~ ' */' ].new,
    Assign[ 'v_1', '(unsigned char)((' ~v~ ') & 0xFF)',1].new,
    Assign[ 'v_2', '(unsigned char)(((' ~v~ ') >> 8) & 0xFF)',1].new,
    Debug[ '#ifdef DBG_' ].new,
    Debug[ '    printf("PUSH2 ' ~st~ ':%d\n",u->' ~st~ '.ptr);' ].new,
    Debug[ '#endif' ].new,
    Statement[ '    u->' ~st~ '.dat[u->' ~st~ '.ptr++] = v_2;' ].new,
    Debug[ '#ifdef DBG__' ].new,
    Debug[ '    printf("' ~st~ 'sp:%d:%d\n",u->' ~st~ '.ptr,u->' ~st~ '.dat[u->' ~st~ '.ptr-1]);' ].new,
    Debug[ '#endif' ].new,
    Statement[ '    u->' ~st~ '.dat[u->' ~st~ '.ptr++] = v_1;' ].new,
    Debug[ '#ifdef DBG__' ].new,
    Debug[ '    printf("PUSH2 ' ~st~ ':%d:%d\n",u->' ~st~ '.ptr,u->' ~st~ '.dat[u->' ~st~ '.ptr-1]);' ].new,
    Debug[ '#endif' ].new,
    Protect[ '#ifdef DEFENSIVE' ].new,
    Protect[ 'if (u->' ~st~ '.ptr>STACK_SZ) {' ].new,
    Protect[ 'printf("Working stack overflow\n");' ].new,
    Protect[ 'exit(0);' ].new,
    Protect[ '}' ].new,
    Protect[ '#endif' ].new,
    Statement[ '}' ].new
    ]
    )
}

sub POP1(\rs) {
    my \r = rs ?? 'r' !! '';
    my \st = rs ?? 'rst' !! 'wst';
no_protect_debug(
    [
    Statement[ '{' ].new,
    Comment[ '/* POP1' ~r~ ' */' ].new,
    Statement[ '    u->' ~st~ '.ptr-=1;' ].new,
    Debug[ '#ifdef DBG_' ].new,
    Debug[ '    printf("POP1 ' ~st~ ':%d\n",u->' ~st~ '.ptr);' ].new,
    Debug[ '#endif' ].new,
    Protect[ '#ifdef DEFENSIVE' ].new,
    Protect[ 'if ((short)(u->' ~st~ '.ptr)<0) {' ].new,
    Protect[ '  printf("Stack underflow\n");' ].new,
    Protect[ '  exit(0);' ].new,
    Protect[ '}' ].new,
    Protect[ '#endif' ].new,
    Statement[ '}' ].new,
    ]
)
}

sub POP2(\rs) {
    my \r = rs ?? 'r' !! '';
    my \st = rs ?? 'rst' !! 'wst';
no_protect_debug(
    [
    Statement[ '{' ].new,
    Comment[ '/* POP2' ~r~ ' */' ].new,
    Statement[ 'u->' ~st~ '.ptr-=2;' ].new,
    Debug[ '#ifdef DBG_' ].new,
    Debug[ '    printf("POP2 ' ~st~ ':%d\n",u->' ~st~ '.ptr);' ].new,
    Debug[ '#endif' ].new,

    Protect[ '#ifdef DEFENSIVE' ].new,
    Protect[ 'if ((short)(u->' ~st~ '.ptr)<0) {' ].new,
    Protect[ 'printf("Working stack underflow\n");' ].new,
    Protect[ 'exit(0);' ].new,
    Protect[ '}' ].new,
    Protect[ '#endif' ].new,
    Statement[ '}' ].new
    ]
)
}

sub POP1k() { '' }
sub POP2k() { '' }

# like pop but returns the value popped
sub GET1(\v,\rs) {
    my \r = rs ?? 'r' !! '';
    my \st = rs ?? 'rst' !! 'wst';
    no_protect_debug(
    [
    Comment[ '/* GET1' ~r~ ' */' ].new,
    Protect[ '#ifdef DEFENSIVE' ].new,
    Protect[ 'if (u->' ~st~ '.ptr==0) {' ].new,
    Protect[ '  printf("Stack underflow\n");' ].new,
    Protect[ '  exit(0);' ].new,
    Protect[ '}' ].new,
    Protect[ '#endif' ].new,
    Assign[ v, 'u->' ~st~ '.dat[--u->' ~st~ '.ptr];',1 ].new,
    Debug[ '#ifdef DBG_' ].new,
    Debug[ '    printf("GET1 ' ~st~ ':%d:%d\n",u->' ~st~ '.ptr,' ~v~ ');' ].new,
    Debug[ '#endif' ].new,
    ]
    )
}

sub GET2(\v,\rs) {
    my \r = rs ?? 'r' !! '';
    my \st = rs ?? 'rst' !! 'wst';
    no_protect_debug(
    [
    Comment[ '/* GET2' ~r~ ' */' ].new,
    Protect[ '#ifdef DEFENSIVE' ].new,
    Protect[ 'if (u->' ~st~ '.ptr==1) {' ].new,
    Protect[ '  printf("Stack underflow\n");' ].new,
    Protect[ '  exit(0);' ].new,
    Protect[ '}' ].new,
    Protect[ '#endif' ].new,
    Assign[ 'v' ~v~ '_1', 'u->' ~st~ '.dat[--u->' ~st~ '.ptr];',1 ].new,
    Assign[ 'v' ~v~ '_2', 'u->' ~st~ '.dat[--u->' ~st~ '.ptr];',1 ].new,
    Assign[ v, '(((unsigned short)v' ~v~ '_2) << 8) + (unsigned short)v' ~v~ '_1;',2 ].new,
    Debug[ '#ifdef DBG_' ].new,
    Debug[ '    printf("GET2 ' ~st~ ':%d:%d\n",u->' ~st~ '.ptr,' ~v~ ');' ].new,
    Debug[ '#endif' ].new,
    ]
    )
}

sub no_protect_debug(@cgens) {
    $DBG_ ?? @cgens !! grep ->\cg {not cg ~~ ( Protect | Debug )}, @cgens;
}