use v6;
unit module UxntalStackBasedCOpsEmitter:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;
use UxntalTypes;
use UxntalStackBasedCEmitterTypes;
use UxntalDefs;

my $V = False;
my $DBG=False;
my $NO_INLINE = False;

my $prefix='';


my %binops =

    EQU => '==',
    NEQ => '!=',
    GTH => '>',
    LTH => '<',

    ADD => '+',
    SUB => '-',
    MUL => '*',
    DIV => '/',
    AND => '&',
    ORA => '|',
    EOR => '^'
;

my @opcodes = %non_stack_operations.keys.sort;

my $mem = 'u->ram';
my $zeropage =  'u->ram';
my $wsp = 'u->wst.ptr';
my $rsp = 'u->rst.ptr';


# This routine decomposes every token into a list of CGen
sub generate-C-op(Token \token_, Int \cnt, Str \prefix_ --> List) is export { # List of CGen
# say "Inline Ops optimisation";
# say "generate-C-op {token_.Str}";
    $prefix = prefix_;
    my @c_gens;
    if token_ ~~ Call {
        my \fname = c_identifier( token_.fname.val );
        push @c_gens, SubCall[ fname ,[] ].new;
    } elsif token_ ~~ CondCall {
        if $NO_INLINE {
            push @c_gens, Statement[ 'if ( GET1() ) { ' ~c_identifier( token_.fname.val )~ '(); return; };'  ].new;
        } else {
            push @c_gens, Get[ 'cond' ~cnt, 1, False,False].new;
            push @c_gens, CondSubCall[ 'cond' ~cnt,
                SubCall[ c_identifier( token_.fname.val ), []].new,
            ].new;
        }
    } elsif token_ ~~ CondReturn {
        if $NO_INLINE {
            push @c_gens, Statement[ 'GET1( cond' ~cnt~ ' );' ].new;
        } else {
            push @c_gens, Get[ 'cond' ~cnt, 1, False,False].new;
        }
        push @c_gens, Protect[ '#ifdef DEFENSIVE' ].new;
        push @c_gens, Protect[ 'if (cond' ~cnt~ '>1) {' ].new;
        push @c_gens, Protect[ '    printf("Sorry, computed jumps only supported with booleans or constant values: ' ~ token_.cond.Str ~ ' %d\n",cond' ~cnt~ ');' ].new;
        push @c_gens, Protect[ '    exit(0);' ].new;
        push @c_gens, Protect[ '}' ].new;
        push @c_gens, Protect[ '#endif' ].new;

        push @c_gens, Cond[ '!cond' ~cnt,
            [
                Return[ 'return;' ].new
            ]
        ].new;

    } elsif token_ ~~ CondJump {

        # my Str \op_str = __emit_C_token_str(token_.op);
        # say "CondJump recursion";
        my @c_gens_op = generate-C-op(token_.op,cnt,prefix_);
        # say token_.raku; die @c_gens_op.raku;
        # say "AFTER CondJump recursion";
        if $NO_INLINE {
            push @c_gens, Statement[ 'GET1( cond' ~cnt~ ' );' ].new;
        } else {
            push @c_gens, Get[ 'cond' ~cnt,1, False,False].new;
        }
        push @c_gens, Protect[ '#ifdef DEFENSIVE' ].new;
        push @c_gens, Protect[ 'if (cond' ~cnt~ '>1) {' ].new;
        push @c_gens, Protect[ '    printf("Sorry, computed jumps only supported with booleans or constant values: ' ~ token_.cond.Str ~ ' %d\n",cond' ~cnt~ ');' ].new;
        push @c_gens, Protect[ '    exit(0);' ].new;
        push @c_gens, Protect[ '}' ].new;
        push @c_gens, Protect[ '#endif' ].new;
        push @c_gens, Cond[ '!cond' ~cnt, @c_gens_op ].new;

    } elsif token_ ~~ Operation {
        my (\opcode_,\wordsz,\keep,\rs) = token_.opcode.tuple;

        my Str \r = rs ?? 'r' !! '';
        my Str \k = keep ?? 'k' !! '';
        # my Str \ctype = wordsz==1??'char' !!'short';
        my Str \sp = rs ?? $rsp !! $wsp;
        my Str \stack = rs ?? 'u->rst.dat' !! 'u->wst.dat';

        if $NO_INLINE { # Purely for debugging
            if opcode_ eq 'BRK'  {
                push @c_gens, Statement[ 'return; /* BRK */;' ].new;
            }
            elsif opcode_~ wordsz ~ k ~ r eq 'JMP2r'  {
                push @c_gens, Statement[ 'return; /* JMP2r */;' ].new;
            } else {
                push @c_gens, Statement[ opcode_~ wordsz ~ k ~ r ~ '();' ].new;
            }
        } else {
            push @c_gens, Comment[ '/* ---- ' ~ opcode_~ wordsz ~ k ~ r ~ ' ---- */' ].new;
            push @c_gens, Debug[ '#ifdef DBG_' ].new;
            push @c_gens, Debug[ 'printf("' ~ opcode_~ wordsz ~ k ~ r ~ ' IN: ' ~sp~ ':%d\n",' ~sp~ ');' ].new;
            push @c_gens, Debug[ '#endif' ].new;

            given opcode_ {
                when 'POP' {
                    push @c_gens, Pop[wordsz,rs,keep].new;
                }

                when 'NIP' {
                    # a b => b
                    # a b => a b b
                    push @c_gens, Comment[ '/* a b => ' ~(keep ?? 'a b ' !! '')~ ' b */' ].new;
                    push @c_gens, Protect[ 'if ('~sp~' < ' ~wordsz~ ' ) {' ].new;
                    push @c_gens, Protect[ 'printf( "NOT ENOUGH ITEMS ON THE STACK for NIP1' ~ r ~ '");' ].new;
                    push @c_gens, Protect[ 'exit(0);' ].new;
                    push @c_gens, Protect[ '}' ].new;
                    push @c_gens, Get[ 'b' ~cnt, wordsz,rs,keep].new;
                    if keep {
                        push @c_gens, Push[ 'b' ~cnt,wordsz,rs,keep].new;
                    } else {
                        push @c_gens, Pop[wordsz,rs,keep].new;
                    }
                    push @c_gens, Push[ 'b' ~cnt, wordsz,rs,keep].new;

                } # NIP
                when 'ROT' {

                    push @c_gens, Comment[ '        /* a b c =>' ~(keep?? 'a b c' !! '')~ ' b c a */' ].new;
                    push @c_gens, Protect[ '    if ( ' ~sp~ ' < ' ~(3*wordsz-1)~ ' ) {' ].new;
                    push @c_gens, Protect[ '        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT\n");' ].new;
                    push @c_gens, Protect[ '        exit(0);' ].new;
                    push @c_gens, Protect[ '    }' ].new;
                    push @c_gens, Get['c'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Get['b'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Get['a'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;
                        push @c_gens, Push['b'~cnt,wordsz,rs,keep].new;
                        push @c_gens, Push['c'~cnt,wordsz,rs,keep].new;
                    }
                    push @c_gens, Push['b'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Push['c'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;

                } # ROT
                when 'OVR' {

                    push @c_gens, Comment[ '    /* a b => a b a' ~ (keep ?? ' b a' !! '')~ ' */' ].new;
                    push @c_gens, Protect[ '    if (' ~sp~ ' < '  ~(wordsz*2-1)~  ' ) {' ].new;
                    push @c_gens, Protect[ '    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR1k");' ].new;
                    push @c_gens, Protect[ '    exit(0);' ].new;
                    push @c_gens, Protect[ '    }' ].new;
                    push @c_gens, Get['b'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Get['a'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;
                        push @c_gens, Push['b'~cnt,wordsz,rs,keep].new;
                    }
                    push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Push['b'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;

                } # OVR
                when 'DUP' {

                    push @c_gens, Comment[ '    /* a => ' ~(keep ?? 'a ' !! '' )~ 'a a */ ' ].new;
                    push @c_gens, Protect[ '    if (' ~sp~ ' < '  ~wordsz~ ' ) {' ].new;
                    push @c_gens, Protect[ '        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP1k");' ].new;
                    push @c_gens, Protect[ '        exit(0);' ].new;
                    push @c_gens, Protect[ '    }' ].new;
                    push @c_gens, Get['a'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;
                    }
                    push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;

                } # DUP

                when 'SWP' {

                    push @c_gens, Comment[ '    /* a b => ' ~(keep ?? 'a b ' !! '')~ ' b a */' ].new;
                    push @c_gens, Protect[ '    if (' ~sp~ ' < 1) {' ].new;
                    push @c_gens, Protect[ '        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");' ].new;
                    push @c_gens, Protect[ '        exit(0);' ].new;
                    push @c_gens, Protect[ '    }' ].new;
                    push @c_gens, Get['b'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Get['a'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens, Get['a'~cnt,wordsz,rs,keep].new;
                        push @c_gens, Push['b'~cnt,wordsz,rs,keep].new;
                    }
                    push @c_gens, Push['b'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Push['a'~cnt,wordsz,rs,keep].new;

                } # SWP
                when 'STH' {
                    # GET from stack
                    push @c_gens, Get['a'~cnt,wordsz,rs,False].new;
                    if keep {
                    # PUSH back onto the stack
                        push @c_gens, Push['a'~cnt,wordsz,rs,False].new;
                    }
                    # PUSH onto other stack
                    push @c_gens, Push['a'~cnt,wordsz,(not rs),False].new;
                }

                when 'BRK' { push @c_gens, Return[ 'return; /* BRK */' ].new; }
                when 'STR' {
                    die "/* {opcode_}{wordsz}{k}{r} is not supported, should have become STA */\n";
                    push @c_gens, Comment["/* {opcode_}{wordsz}{k}{r} is not supported, should have become STA */"].new
                }
                when 'LDR' {
                    die "/* {opcode_}{wordsz}{k}{r} is not supported, should have become LDA */\n";
                    push @c_gens, Comment["/* {opcode_}{wordsz}{k}{r} is not supported, should have become LDA */"].new;
                }
                # JMP2, JSR2, JCN2 only occur for calls to function pointers
                # Otherwise we have Call and CondCall
                when 'JMP' {
                    if wordsz == 2 and not keep and not rs { # JMP2
                        @c_gens = |@c_gens,|_gen_GETFP(cnt);
                        push @c_gens, SubCall[ 'fp' ~cnt ,[] ].new;
                    } elsif wordsz == 2 and rs {
                        push @c_gens, Return[ 'return; /* JMP2r */'].new;
                    } else {
                        die "/* {opcode_}{wordsz}{k}{r} is not supported: only JMP2(r) */";
                    }
                }
                when 'JSR' {
                    if  wordsz == 2 and not keep and not rs {
                        @c_gens = |@c_gens,|_gen_GETFP(cnt);
                        push @c_gens, SubCall[ 'fp' ~cnt ,[] ].new;
                    } else {
                        push @c_gens, Comment["/* {opcode_}{wordsz}{k}{r} is not supported: only JSR2 */"].new;
                    }
                }
                when 'JCN' {
                    if  wordsz == 2 and not keep and not rs {
                        @c_gens = |@c_gens,|_gen_GETFP(cnt);
                        push @c_gens, Get[ 'cmp' ~cnt,1,rs,keep].new;
                        push @c_gens, CondSubCall[ 'cmp' ~cnt,  SubCall['fp' ~cnt,[] ].new ].new;
                    } else {
                        push @c_gens, Comment[ "/* {opcode_}{wordsz}{k}{r} is not supported: only JCN2 */"].new;
                    }
                }
                when 'LDA' {
                    # a LDA
                    push @c_gens, Get[ 'a'~cnt,2,rs,keep].new;
                    if keep {
                        push @c_gens, Push[ 'a'~cnt,2,rs,keep].new;
                    }
                    if  wordsz == 1 {
                        push @c_gens, Assign[ 'v' ~cnt, 'u->ram[a' ~cnt~ ']',1].new;
                    } else {
                        push @c_gens, Assign[ 'v' ~cnt, '(u->ram[a' ~cnt~ '] << 8) + u->ram[a' ~cnt~ ' + 1]',2].new;
                    }
                    push @c_gens, Push[ 'v'~cnt,wordsz,rs,keep].new;
                }
                when 'STA' {
                    # v a STA
                    push @c_gens,Get['a'~cnt,2,rs,keep].new;
                    push @c_gens,Get['v'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens,Push['v'~cnt,wordsz,rs,keep].new;
                        push @c_gens,Push['a'~cnt,2,rs,keep].new;
                    }
                    if  wordsz == 1 {
                        push @c_gens, Statement[ 'u->ram[a' ~cnt~ '] = v' ~cnt~ ';'].new;
                    } else {
                        push @c_gens, Statement[ 'u->ram[a' ~cnt~ '] = v' ~cnt~ ' >> 8; u->ram[a' ~cnt~ ' + 1] = v' ~cnt~ ';'].new;
                    }
                }
                when 'LDZ' {
                    # a LDZ
                    push @c_gens, Get[ 'a'~cnt,1,rs,keep].new;
                    if keep {
                        push @c_gens, Push[ 'a'~cnt,1,rs,keep].new;;
                    }
                    if  wordsz == 1 {
                        push @c_gens, Assign[ 'v' ~cnt, 'u->ram[(unsigned short)a' ~cnt~ ']',1].new;
                    } else {
                        push @c_gens, Assign[ 'v' ~cnt, '(u->ram[a' ~cnt~ '] << 8) + u->ram[a' ~cnt~ ' + 1]',2].new;
                    }
                    push @c_gens, Push[ 'v'~cnt,wordsz,rs,keep].new;;
                }
                when 'STZ' {
                    # v a STZ
                    push @c_gens, Get[ 'a'~cnt,1,rs,keep].new;
                    push @c_gens, Get[ 'v'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens, Push[ 'v'~cnt,wordsz,rs,keep].new;;
                        push @c_gens, Push[ 'a'~cnt,wordsz,rs,keep].new;;
                    }
                    if  wordsz == 1 {
                        push @c_gens, Statement[ 'u->ram[(unsigned short)a' ~cnt~ '] = v' ~cnt~ ';'].new;
                    } else {
                        push @c_gens, Statement[ 'u->ram[a' ~cnt~ '] = v' ~cnt~ ' >> 8; u->ram[a' ~cnt~ ' + 1] = v' ~cnt~ ';'].new;
                    }
                }
                when 'DEO' {
                    # c p DEO
                    push @c_gens, Comment[ '/* DEO' ~wordsz~r~k~' */' ].new;
                    push @c_gens, Get[ 'p'~cnt ,1,rs,keep].new;
                    push @c_gens, Statement[ 'Device* dev' ~cnt~ ' = &(u->dev[p' ~cnt~ ' >> 4]);'].new;
                    push @c_gens, Get[ 'c'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens,Push[ 'c'~cnt,wordsz,rs,keep].new;;
                        push @c_gens,Push[ 'p'~cnt,1,rs,keep].new;;
                    }
                    if wordsz==2 {
                        push @c_gens, Statement[ 'dev' ~cnt~ '->dat[p' ~cnt~ ' & 0xf] = (unsigned char)(c' ~cnt~ '>>8);' ].new;
                        push @c_gens, Statement[ 'dev' ~cnt~ '->deo(dev' ~cnt~ ', p' ~cnt~ ' & 0x0f);' ].new;
                        push @c_gens, Statement[ 'dev' ~cnt~ '->dat[(p' ~cnt~ '+1) & 0xf] = (unsigned char)((c' ~cnt~ ') & 0xFF);' ].new;
                        push @c_gens, Statement[ 'dev' ~cnt~ '->deo(dev' ~cnt~ ', (p' ~cnt~ '+1) & 0x0f);' ].new;
                    } else {
                        push @c_gens, Statement[ 'dev' ~cnt~ '->dat[p' ~cnt~ ' & 0xf] = c' ~cnt~ ';' ].new;
                        push @c_gens, Statement[ 'dev' ~cnt~ '->deo(dev' ~cnt~ ', p' ~cnt~ ' & 0x0f);' ].new;
                    }
                    push @c_gens, Debug[ '#ifdef DBG' ].new;
                    push @c_gens, Debug[ 'printf("'~opcode_~ ': %d (%c) to port %d\n",c' ~cnt~ ',c' ~cnt~ ',p' ~cnt~ ');' ].new;
                    push @c_gens, Debug[ '#endif' ].new;

                }
                when 'DEI' {
                    push @c_gens, Comment[ '/* DEI' ~wordsz~r~k~' */' ].new;
                    # p DEI

                    push @c_gens, Get[ 'p'~cnt,1,rs,keep].new;

                    if keep {
                        push @c_gens,Push[ 'p'~cnt,1,rs,keep].new;
                    }

                    push @c_gens, Statement[ 'Device* dev' ~cnt~ ' = &(u->dev[p' ~cnt~ ' >> 4]);' ].new;
                    push @c_gens, Assign[ 'c' ~cnt,  'dev' ~cnt~ '->dei(dev' ~cnt~ ', p' ~cnt~ ' & 0x0f)',wordsz ].new;
                    if wordsz==2 {
                        push @c_gens, Statement[ 'c' ~cnt~ ' = (c' ~cnt~ ' << 8) + dev' ~cnt~ '->dei(dev' ~cnt~ ', (p' ~cnt~ ' + 1) & 0x0f);' ].new;
                    }
                    push @c_gens, Push[ 'c' ~cnt,wordsz,rs,keep].new;
                    push @c_gens, Debug[ '#ifdef DBG' ].new;
                    push @c_gens, Debug[ 'printf("'~opcode_~ ': %d (%c) from port %d\n",c' ~cnt~ ',c' ~cnt~ ',p' ~cnt~ ');' ].new;
                    push @c_gens, Debug[ '#endif' ].new;
                }
                when 'INC' {
                    # v INC
                    push @c_gens,Get[ 'v' ~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens,Push[ 'v' ~cnt,wordsz,rs,keep].new;;
                    }
                    push @c_gens, Push[ 'v' ~cnt~ '+1',wordsz,rs,keep].new;
                }
                when 'SFT' {
                    # b c SFT

                    push @c_gens, Get[ 'c'~cnt,1,rs,keep].new;
                    push @c_gens, Get[ 'b'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens,Push[ 'b'~cnt,wordsz,rs,keep].new;
                        push @c_gens,Push[ 'c'~cnt,1,rs,keep].new;
                    }
                    push @c_gens, Assign[ 'c8l' ~cnt,'(c' ~cnt~ ' & 0xF)',1 ].new;
                    push @c_gens, Assign[ 'c8h' ~cnt,' ((c' ~cnt~ ' >> 4) & 0xF)',1 ].new;
                    push @c_gens, Push[ '((b' ~cnt~ '>>c8l' ~cnt~ ')<<c8h' ~cnt~ ')' ,wordsz,rs,keep].new;
                }
                when %binops{opcode_}:exists {
                    # a b c BINOP => b `BINOP` c
                    push @c_gens, Get['c'~cnt,wordsz,rs,keep].new;
                    push @c_gens, Get['b'~cnt,wordsz,rs,keep].new;
                    if keep {
                        push @c_gens,Push['b'~cnt,wordsz,rs,keep].new;
                        push @c_gens,Push['c'~cnt,wordsz,rs,keep].new;
                    }
                    if %cmp_operations{opcode_}:exists {
                        # push @c_gens, Assign[ 'cmp' ~cnt,'(' ~ctype~ ')b' ~cnt~ %binops{opcode_} ~ '('~ctype~ ')c' ~cnt,1 ].new;
                        push @c_gens, Assign[ 'cmp' ~cnt,'b' ~cnt~ %binops{opcode_} ~ 'c' ~cnt,1 ].new;
                        push @c_gens, Push[ 'cmp' ~cnt,1,rs,keep].new;
                    } else {
                        push @c_gens, Push[ '( b' ~cnt~ %binops{opcode_} ~ 'c' ~cnt~ ' )', wordsz,rs,keep].new;
                    }
                }

                default {
                    die 'Unsupported operation: ',opcode_.raku;
                }
            } # Given
            push @c_gens, Debug[ '#ifdef DBG_' ].new;
            push @c_gens, Debug[ 'printf("' ~ opcode_~ wordsz ~ k ~ r ~ ' OUT: ' ~sp~ ':%d\n",' ~sp~ ');' ].new;
            push @c_gens, Debug[ '#endif' ].new;
            push @c_gens, Comment[ '/* END ---- ' ~ opcode_~ wordsz ~ k ~ r ~ ' ---- END */' ].new;
        } # $NO_OPT
        @c_gens

    } else { # Not an operation, so must be a value

        if token_ ~~ Literal {
            _gen_Literal(token_,cnt)
        } elsif token_ ~~ FunctionAddr {
            my \val_str = _emit_C_value_str(token_);
            if $NO_INLINE {
                [
                    Statement[ 'PUSHFP'  ~'(' ~val_str~ ');'  ].new;
                ]
            } else {
                _gen_PUSHFP(val_str,cnt );
            }
        } else {
            my \val_str = _emit_C_value_str(token_);
            if $NO_INLINE {
                [
                    Statement[ 'PUSH' ~ token_.wordsz ~'(' ~val_str~ ');'  ].new;
                ]
            } else {
                [ Push[val_str,token_.wordsz,False,False].new ];
            }
        }
    }
} # END of generate-C-op


sub _emit_C_value_str(Token \token_ --> Str) {
    say "_emit_C_value_str TOKEN: {token_.raku}" if $DBG;
        if token_ ~~ RawHex {
          token_.val
        }
        elsif token_ ~~ RawAsciiChar {
            "'" ~ token_.val ~ "'"
        }
        elsif token_ ~~ RawAsciiWord {
            '"' ~ token_.val ~ '"'
        }
        elsif token_ ~~ HexLiteral {
             hex2dec(token_.val);
        }

        elsif token_ ~~ LitAbsAddr {
            c_identifier( token_.val);
        }
        elsif token_ ~~ FunctionAddr {
            c_identifier( token_.val);
        }
        elsif token_ ~~ LitZeroPageAddr {
            c_identifier( token_.val);
        }
        elsif token_ ~~ AbsLabel {
            c_identifier( token_.val) ;
        }
        elsif token_ ~~ RelPadding { # no more regs: token_ ~~ Reg or
            ''
        }
        else {
            die '_emit_C_value_str: Unsupported token: ', token_.raku;
        }
} # END of _emit_C_value_str

sub _gen_PUSHFP(Str \fp_str, Int \cnt ) {
    # void (*fp)(void)
    my @c_gens;
    push @c_gens, Debug[ '#ifdef DBG_' ].new;
    push @c_gens, Debug[ '    printf("PUSHFP\n");' ].new;
    push @c_gens, Debug[ '#endif' ].new;

    push @c_gens, Statement[ '    void* vfp' ~cnt~ ' = (void*)(' ~ fp_str ~');' ].new;

    push @c_gens, Statement[ '    int idx' ~cnt~ '=0;' ].new;
    push @c_gens, Statement[ '    for (idx' ~cnt~ '=0;idx' ~cnt~ '<f_idx;++idx' ~cnt~ ') {' ].new;
    push @c_gens, Statement[ '        if (functions[idx' ~cnt~ ']==vfp' ~cnt~ ') {' ].new;
    push @c_gens, Push[ 'idx' ~cnt,2,False,False ].new;
    push @c_gens, Statement[ '            break;' ].new;
    push @c_gens, Statement[ '        }' ].new;
    push @c_gens, Statement[ '    }' ].new;

    push @c_gens, Statement[ '    functions[f_idx]=vfp' ~cnt~ ' ;' ].new;
    push @c_gens, Statement[ '    unsigned short f_idx' ~cnt~ ' = f_idx;' ].new;
    push @c_gens, Statement[ '    f_idx++;' ].new;
    push @c_gens, Push[ 'f_idx' ~cnt, 2,False,False].new;
    @c_gens;
} # END of _gen_PUSHFP

sub _gen_Literal(Token \token_,Int \cnt) {
            my @c_gens;
            push @c_gens, Comment[ '/* LIT' ~ token_.wordsz ~ ' */' ].new;
            my $prev_token=NoToken.new;
            my Str \r = token_.rs ?? 'r' !! '';
            for |token_.tokens -> \t_ {
                if t_ ~~ AbsLabel {
                    my $val = c_identifier( t_.val);
                    # This must be PUSH2 because it's an absolute address
                    if $NO_INLINE {
                        push @c_gens, Statement[ '/* LIT' ~ token_.wordsz ~ ' */' ].new;
                        push @c_gens, Statement[ 'PUSH2(' ~$val~ ')' ].new;
                    } else {
                    push @c_gens, Push[ $val, 2, token_.rs, False].new;
                    }
                    my @c_gens_LDA = _gen_LDA(token_.tokens.elems >2 ?? 1 !! token_.wordsz, token_.rs,False,cnt);
                    @c_gens = |@c_gens,|@c_gens_LDA;

                } elsif (t_ ~~ RawHex  or t_ ~~ HexLiteral) {
                    if not $prev_token ~~ AbsLabel {
                        my $val = hex2dec(t_.val);
                        push @c_gens, Push[$val,t_.wordsz,token_.rs,False].new;
                    }
                } elsif (t_ ~~ RawAsciiChar) {
                    if not $prev_token ~~ AbsLabel {
                        my $val = "'" ~ t_.val ~"'";
                        push @c_gens, Push[$val,t_.wordsz,token_.rs,False].new;
                    } else {
                    die "TODO: Raw Ascii Char without label in Literal: " ~ t_.raku;
                    }
                } elsif (t_ ~~ RawAsciiWord) {
                    if not $prev_token ~~ AbsLabel {
                        if t_.val.chars == 1 {
                            my $val = "'" ~ t_.val ~"'";
                            push @c_gens, Push[$val,1,token_.rs,False].new;
                        } elsif t_.val.chars == token_.wordsz {
                            my $val1 = "'" ~ t_.val.substr(0,1) ~"'";
                            push @c_gens, Push[$val1,1,token_.rs,False].new;
                            my $val2 = "'" ~ t_.val.substr(1,1) ~"'";
                            push @c_gens, Push[$val2,1,token_.rs,False].new;
                        } else {
                            die "TODO: Raw Ascii Word after LIT is limited to {token_.wordsz} bytes: " ~ t_.raku;
                        }
                    } else {
                    die "TODO: Raw Ascii Word without label in Literal: " ~ t_.raku;
                    }
                } elsif not t_ ~~ RelPadding {
                    die "TODO: " ~ t_.raku;
                }
                $prev_token=t_;
            }
            @c_gens;
} # END of _gen_Literal

sub _gen_LDA(\wordsz,\rs,\keep,\cnt) {
    my @c_gens;
    if $NO_INLINE {
        push @c_gens, Statement[ 'LDA' ~ wordsz ~ (keep ?? 'k' !! '') ~ (rs ?? 'r' !! '') ~ '();' ].new;
    } else {
        push @c_gens, Get[ 'a'~cnt,2,rs,keep].new;
        if keep {
            push @c_gens, Push[ 'a'~cnt,2,rs,keep].new;
        }
        if  wordsz == 1 {
            push @c_gens, Statement[ 'unsigned char v' ~cnt~ ' = ' ~ $mem ~ '[a' ~cnt~ '];'].new;
        } else {
            push @c_gens, Statement[ 'unsigned short v' ~cnt~ ' = (u->ram[a' ~cnt~ '] << 8) + u->ram[a' ~cnt~ ' + 1];'].new;
        }
        push @c_gens, Push[ 'v'~cnt,wordsz,rs,keep].new;
    }
    @c_gens;
}

sub _gen_GETFP(\cnt) {
    my @c_gens;
    if $NO_INLINE {
        push @c_gens, Statement[ 'GETFP();' ].new;
    } else {
        push @c_gens, Get[ 'f_idx_'~cnt,2,False,False].new;
        push @c_gens, Statement[ 'void* vfp' ~cnt~ ' = functions[f_idx_' ~cnt~ '];' ].new;
        push @c_gens, Statement[ 'f_ptr fp' ~cnt~ ' = (f_ptr)vfp' ~cnt~ ';' ].new;
    }
    @c_gens;
}
# Note that for raw bytes we can say \x...
sub hex2dec (Str \hexval --> Str ) {
    :16(hexval).Str
}

sub c_identifier (Str \str_ --> Str) {
    my \str__ = TR/ - / _ / with str_;
    my \id = TR/ \/ / _ / with str__;
    $prefix ~ '_' ~ id;
}


# This should be preceded by a recursive inlining of all non-recursive functions.
# See UxntalInliner.rakumod

=begin pod
This is a stack-to-register optimisation
We keep SubCall and Cond separated because if we want to optimise these as well, we'll use separate strategies


** For if cond f(), we basically will have:

PUSH a
PUSH b
...
if cond f()
GET b'
GET a'

So if we can get the type of f, say f(a,b,&a'',&b''), we'd have

ctype a'',b'';
f(a,b,&a'',&b'');
b' =  cond ? b'' ! b;
a' = cond ? a'' ! a;

** A plain subcall would simply become

ctype a'',b'';
f(a,b,&a'',&b'');

What this requires is that we
(1) infer the type of f
(2) adapt the declaration and signature of f
(3) replace Get and Push by assignments


** A call with a single instruction (the Boolean jump thing) would be for example

PUSH a
PUSH b
if cond {
    GET b''
    GET a''
    ctype res1 = calc1()
    PUSH res1
    ctype res2 = calc2()
    PUSH res2
}
GET b'
GET a'

What we want in the end is

ctype res1,res2;
if cond {
    b'' = b
    a'' = a
    res1 = calc
    res2 = calc
}
b' =  cond ? res2 ! b;
a' = cond ? res1 ! a;

Although in practice it would be

PUSH a
PUSH b
if cond {
    GET b''
    GET a''
    PUSH calc1(a'',b'')
    PUSH calc2(a'',b'')
}
GET b'
GET a'

and simply become

ctype b'' = b
ctype a'' = a
b' =  cond ? calc2(a'',b'') ! b;
a' = cond ? calc1(a'',b'') ! a;

It could of course even become

b' = cond ? calc2(a,b) ! b;
a' = cond ? calc1(a,b) ! a;

So how do we do this?
0/ The GETs and PUSHs are those in the part of the code beween other Controls that have not been eliminated.
1/ We find a Cond
2/ We look for GETs below it
3/ We link them up *somehow* with PUSHs above the Cond.
Probably the usual way with Assign but we also need to store the indices of both the GETs and PUSHs
4/ Then we go into the Cond block and we link up the GETs in that block with the PUSHs above it.
5/ We link th PUSHs in th block with the GETs below it
6/ Finally we emit the assignments with ?! expressions

Can we do it in three steps?

(0) match up Get below Cond with Push above Cond. We need a special CondAssign for this and in that, we populate the False branch with this ass

PUSH a
PUSH b
if cond {
    GET b''
    GET a''
    PUSH calc1(a'',b'')
    PUSH calc2(a'',b'')
}
b' = cond ? _ ! b but really we do CondAssign[cond,NoToken,Assignment(b',b)]
a' = cond ? _ ! a

(1) match up Get in Cond with Push above Cond, eliminate both the Push and the Get
if cond {
    PUSH calc1(a'',b'')
    PUSH calc2(a'',b'')
}
b' = cond ? _ ! b but really we do CondAssign[cond,NoToken,Assignment(b',b)]
a' = cond ? _ ! a

(2) match up CondAssign below Cond with Push in Cond and eliminate Cond

 b' = cond ? calc2(a'',b'') ! b
 a' =  cond ? calc1(a'',b'') ! a



=end pod

# It works as follows:
sub stack_to_reg_optimisation(\c_gens,$DBG_LVL) is export {
    # say "Stack to Reg optimisation" if $DBG;
    my %eliminated_push;
    my %get_to_assignment;
    my $rs = False;
    for 0 .. c_gens.elems -> \idx {
        next if c_gens[idx] ~~ ( Str | Nil );

        # Whenever we find a Get or a Pop
        if c_gens[idx] ~~ ( Pop | Get ) {
            # Set the stack to be manipulated
            $rs = c_gens[idx].rs;
            say  idx,':',c_gens[idx].raku if $V;
            # Move up in the code until we find a Push with the same $rs that has not yet been registered in %eliminated_push
            my $ridx=idx-1;
            # We go back until we encounter a Control, Pop/Get or the start of the block
            # A Pop/Get means that it was not eliminated earlier so it's a barrier
            while (
                $ridx>=0 and
                not c_gens[$ridx] ~~ Control and
                not ( c_gens[$ridx] ~~ ( Pop | Get ) and c_gens[$ridx].rs == $rs)
                )
                {
                # say  "\t$ridx:",c_gens[$ridx].raku;
                if
                c_gens[$ridx] ~~ Push
                and c_gens[$ridx].rs == $rs
                and not %eliminated_push{$ridx}:exists
                {
                    # say "\tunseen PUSH:$ridx:",c_gens[$ridx].raku;

                    # If the wordsz matches, we can remove the Push
                    # We replace a Get with an assign to the pushed value
                    # A Pop is simply removed.
                    if c_gens[$ridx].wordsz == c_gens[idx].wordsz {
                        say "\tregister PUSH2:$ridx" if $V;
                        %eliminated_push{$ridx}=c_gens[$ridx];
                        if c_gens[idx] ~~ Get {
                            # Create the assignment
                            my \assign = Assign[ c_gens[idx].arg, c_gens[$ridx].arg, c_gens[idx].wordsz].new;
                            %get_to_assignment{idx}=assign;
                        } elsif c_gens[idx] ~~ Pop {
                            my \drop_pop = Comment[ '/* dropped POP~ ' ~c_gens[idx].wordsz~ (c_gens[idx].rs ?? 'r' !! '') ~ ' */'].new;
                            %get_to_assignment{idx}=drop_pop;
                        }
                    }
                    # If the wordz does not match, there are two cases.
                    # Case 1 is PUSH1 / GET2|POP2
                    elsif c_gens[$ridx].wordsz == 1 and c_gens[idx].wordsz == 2 {

                        # In that case we go further up to find another PUSH1
                        # say "\t\t Look for another PUSH1";
                        my \rhs1 = c_gens[$ridx].arg;
                        my $ridx2=$ridx-1;
                        # Carry on until we hit a Control, Pop/Get or the top
                        while $ridx2>=0
                        and not c_gens[$ridx2] ~~ Control
                        and not ( c_gens[$ridx2] ~~ ( Pop | Get ) and c_gens[$ridx2].rs == $rs ) {

                            # say "\t\t$ridx2 ",c_gens[$ridx2].raku;
                            if
                            c_gens[$ridx2] ~~ Push
                            and c_gens[$ridx2].wordsz == 1
                            and not %eliminated_push{$ridx2}:exists
                            and c_gens[$ridx2].rs == $rs
                            {
                                # say "\t\t2nd unseen PUSH1:$ridx2:",c_gens[$ridx2].raku;
                                say "\t\t$ridx2 ",c_gens[$ridx2].raku if $V;
                                say "\tregister PUSH:$ridx, $ridx2" if $V;
                                %eliminated_push{$ridx}=c_gens[$ridx];
                                %eliminated_push{$ridx2}=c_gens[$ridx2];
                                my \rhs2 = c_gens[$ridx2].arg;
                                if c_gens[idx] ~~ Get {
                    # We need to look for another PUSH1 and then we need to do compose the RHS as
                                    my \assign = Assign[ c_gens[idx].arg, '(unsigned short)' ~rhs1~' + (unsigned short)(' ~rhs2~ '<<8)', 2].new;
                                    %get_to_assignment{idx}=assign;
                                } elsif c_gens[idx] ~~ Pop  { # must be POP, so drop it
                                    my \drop_pop = Comment[ '/* dropped POP~ ' ~c_gens[idx].wordsz~ (c_gens[idx].rs ?? 'r' !! '') ~ ' */'].new;
                                    %get_to_assignment{idx}=drop_pop;
                                }
                                last;
                            } # if
                            --$ridx2;

                        }  # while
                    }
                    # Case 2 is PUSH2 / GET1|POP1
                    elsif c_gens[$ridx].wordsz == 2 and c_gens[idx].wordsz == 1 {

                         # This means we should look for another GET1 below the current one, and emit an Assign for each byte of the pushed 2-byte word
                         # or drop it if it is a Pop


                         my $ridx2=idx+1;
                         # Now see if there is a matching Get/Pop below that
                         # We stop on a control; we should also stop at the end of the list
                         # And we should also stop at a Push because that modifies the stack
                        while
                        $ridx2 < c_gens.elems-2 and $ridx2
                        and not (c_gens[$ridx2] ~~ ( Push | Get | Pop ) and c_gens[$ridx2].rs == $rs)
                        and not c_gens[$ridx2] ~~ Control
                        {
                            if
                            c_gens[$ridx2] ~~ ( Get | Pop ) and
                            c_gens[$ridx2].wordsz == 1
                            and c_gens[$ridx2].rs == $rs
                            and not %get_to_assignment{$ridx2}:exists
                            { # OK, we found a candidate
                                # We can eliminate the Push
                                say "\tregister PUSH:$ridx" if $V;
                                %eliminated_push{$ridx}=c_gens[$ridx];

                                # First register the Get/Pop we started from.
                                if c_gens[idx] ~~ Get  {
                                    my \assign1 =  Assign[ c_gens[idx].arg, '(' ~ c_gens[idx].arg ~ ') & 0xFF', 1].new;
                                    %get_to_assignment{idx}=assign1;
                                } elsif c_gens[idx] ~~ Pop {
                                    my \drop_pop = Comment[ '/* dropped POP1' ~(c_gens[idx].rs ?? 'r' !! '')~ ' */'].new;
                                    %get_to_assignment{idx}=drop_pop;
                                }

                                if c_gens[$ridx2] ~~ Get
                                {
                                    my \assign2 =  Assign[ c_gens[$ridx2].arg, '(' ~ c_gens[$ridx].arg ~ ')>>8', 1].new;
                                    %get_to_assignment{$ridx2}=assign2;
                                } elsif c_gens[$ridx2] ~~ Pop
                                {
                                    my \drop_pop = Comment[ '/* dropped POP1' ~(c_gens[$ridx2].rs ?? 'r' !! '')~ ' */'].new;
                                    %get_to_assignment{$ridx2}=drop_pop;
                                }
                                last;
                            }
                            ++$ridx2;
                        }
                    } # Three cases: 2/2, 2/1/1, 1/1/2
                    last;
                } # if we found a Push
                --$ridx;
            } # while looking for a Push
        } # if we found a Get or Pop
    } # for

# Then we do a reduction over the indices where we say

# - if it's in get_to_assignment, emit the assignment
# - elsif it's not in eliminated_push, emit the original
# - else emit nothing.


my \CGens_to_skip =
    $DBG_LVL == 3 ?? ( Debug | Protect | Comment )
    !! $DBG_LVL == 2 ?? ( Debug | Protect  )
    !! $DBG_LVL == 1 ??  Debug
    !! Protect
;

    my \c_gens_opt = reduce(
        -> \c_gens_,\idx {
            # say c_gens[idx].raku;
            if  %get_to_assignment{idx}:exists {
                [|c_gens_,%get_to_assignment{idx}]
            } elsif not %eliminated_push{idx}:exists {
                if $DBG_LVL >0 {
                    if not (c_gens[idx] ~~  CGens_to_skip) {
                        if c_gens[idx] ~~ ( Push | Pop | Get ) {
                            [|c_gens_,
                            |grep( ->\cg { not cg ~~ CGens_to_skip},|c_gens[idx].cgens)]
                        } else {
                            [|c_gens_,c_gens[idx]]
                        }
                    } else {
                        c_gens_
                    }
                } else {
                        if c_gens[idx] ~~ ( Push | Pop | Get ) {
                            [|c_gens_, |c_gens[idx].cgens]
                        } else {
                            [|c_gens_,c_gens[idx]]
                        }

                }
            } else {
                [|c_gens_,'/* eliminated: PUSH' ~ c_gens[idx].wordsz ~ ' ' ~ c_gens[idx].arg ~ ' */']
            }
        },
        [],
        |(0 .. c_gens.elems-1)
    );
    c_gens_opt;


} # END of stack_to_reg_optimisation



=begin pod
So is something wrong with the algorithm?


Basic idea:


PUSH$wordsz$rs(vp);
< some non-stack ops >
GET$wordsz$rs(vg);

Rather than pushing and getting, we simply say

vg = vp;

The effect on the stack should be nothing.

If the word size and the stack match, this should be fine.

PUSH1(vp1);

< some non-stack ops >

PUSH1(vp2);
< some non-stack ops >
GET2(vg);

Again, the stack pointer should not be affected.

As we gave CGens for all ops, the only way the stack should be modified is via PUSH, POP and GET.

PUSH2(vp2);
< some non-stack ops >
GET1(vg1);
< some non-stack ops >
GET1(vg2);

If the pattern is anything else than the above, we should not eliminate anything.
=end pod