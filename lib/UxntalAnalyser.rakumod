use v6;
unit module UxntalAnalyser:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;
use UxntalTypes;
use UxntalDefs;
use ImmutableDatastructureHelpers;

our $DBG = False;


sub analyse_program (ParsedProgram \parsed_program_0 , Int \OPT --> ParsedProgram) is export {
    say "=" x 80,"\n",&?ROUTINE.name if $DBG;

    # This is needed for dynamic jumps; maybe should go after _identify_function_addrs
    my ParsedProgram \parsed_program_1 = OPT>3 ??  _find_callers(parsed_program_0)  !! parsed_program_0;
    my ParsedProgram \parsed_program_2 = _identify_function_addrs(parsed_program_1);

    if OPT>3 {
        # Experimental and probably broken, full subroutine inlining
        # Labels for JCN and JMP but not JSR because this is to find end points for tail calls
        my ParsedProgram \parsed_program_3 = _collect_labels(parsed_program_2 );

        my ParsedProgram \parsed_program_4 = _mark_functions_leaf_nonleaf(parsed_program_3 );
        my ParsedProgram \parsed_program_5 = _loop_detection(parsed_program_4 );
        return parsed_program_5;
    } else {
        return parsed_program_2;
    }
} # END of analyse_program

sub tokenised_to_parsed_program(TokenisedProgram \tokenised_program  --> ParsedProgram) is export {
    say "=" x 80,"\n",&?ROUTINE.name if $DBG;
    my Map \code_block_tokens = tokenised_program.blocks;
    my List \code_block_sequence = tokenised_program.blocks-sequence;

    # This simply turns a TokenisedProgram into a ParsedProgram
    reduce(
        -> \ppr_, Str \code_block_label {
            my List \block_tokens = code_block_tokens{code_block_label}.tokens;
            my Map \block_labels = code_block_tokens{code_block_label}.labels;
            my ParsedCodeBlock \parsed_code_block = ParsedCodeBlock.new(
                code=>block_tokens,
                labels=>block_labels,
                function=>Just[code_block_tokens{code_block_label}.is-function].new,
                block-type=>code_block_tokens{code_block_label}.block-type
            );
            my Map \ppr_blocks = ppr_.blocks;
            my Map \ppr_blocks_ = Map.new(ppr_blocks.list, Pair.new(code_block_label , parsed_code_block) );
            my List \blocks_seq_ = ppr_.blocks-sequence;
            my List \blocks_seq = append(blocks_seq_,code_block_label);

            my ParsedProgram \ppr__ = ppr_.clone(
                blocks-sequence=>blocks_seq,
                blocks => ppr_blocks_
                );
                ppr__;
        },
        ParsedProgram.new(
            blocks => Map.new,
            blocks-sequence => List.new,
            callers => Map.new
        ),
        |code_block_sequence
    );
} # END of tokenised_to_parsed_program
# =================================================
# ======== ANALYSIS ===============================

=begin pod
This analysis is for the stack-based C version
We need to identify functions as leaf so we can recursively inline them
The sketch is:

For every block
	Assume the function is Leaf
	Go through the tokens
		If the token is a jump but not a return, mark the function as NonLeaf; break
=end pod

sub _mark_functions_leaf_nonleaf(ParsedProgram \parsed_program_rep_ --> ParsedProgram) {
    say '-' x 80,"\n",&?ROUTINE.name if $DBG;
    my \ppr = reduce(
        -> \ppr_,\code_block_label {
            # say "_mark_functions_leaf_nonleaf({code_block_label})";
		my ParsedCodeBlock \parsed_block = ppr_.blocks{code_block_label};
		my \parsed_block_ = do {
            if parsed_block.block-type ~~ FunctionBlock {
                # say "_mark_functions_leaf_nonleaf({code_block_label}): FunctionBlock";# ~ parsed_block.block-type.raku;
                reduce(
                -> \pb_,\token_ {

                    if pb_.node-type ~~ NonLeafFunction {
                        pb_
                    } else {
                        if token_ ~~ Operation and token_.opcode.name eq ( 'JCN' | 'JSR' | 'JMP' ) and token_.Str ne 'JMP2r' {
                            # say "{code_block_label} is NonLeafFunction";
                            pb_.clone(node-type=>NonLeafFunction.new)
                        }
                        elsif (token_ ~~ Literal and grep( ->\t{ t ~~ AbsLabel }, |token_.tokens).elems>0) {
                            # say "{code_block_label} is non-inlineable NonLeafFunction";
                            pb_.clone(node-type=>NonLeafFunction.new, inlineable=>Just[False].new)
                        } else { pb_ }
                    }
                },
                parsed_block.clone(node-type=>LeafFunction.new, inlineable=>Just[True].new),
                |parsed_block.code
            );
            } else {
                parsed_block
            }
		};
        if parsed_block_.block-type ~~ FunctionBlock {
        say "{code_block_label} is {parsed_block_.inlineable ~~ Just and parsed_block_.inlineable.just ?? 'inlineable' !! 'non-inlineable'} {parsed_block_.block-type.Str}" if $DBG   ;
        }
		my Map \blocks_ = insert(ppr_.blocks,code_block_label,parsed_block_);
            ppr_.clone(blocks=>blocks_);
        },
        parsed_program_rep_,
        |parsed_program_rep_.blocks-sequence
    );
    ppr;
} # END of _mark_functions_leaf_nonleaf

=begin pod
If a function is non-leaf, it we need a further analysis, which I do in a separate pass.
If the function is the start of a loop, then eventually the inlining will have to stop when the function that has the final call back is inlined

=end pod

sub _loop_detection(ParsedProgram \parsed_program_ --> ParsedProgram) {
    # temp $DBG=True;
    say '-' x 80,"\n",&?ROUTINE.name if $DBG;
    reduce(
        -> \ppr_, \code_block_label {
            say "LABEL {code_block_label}" if $DBG;
            my ParsedCodeBlock \pb_ = ppr_.blocks{code_block_label};
            if pb_.node-type ~~ NonLeafFunction {
                my (Maybe \inlineable_, Str \fname__) = __loop_detection_rec( ppr_, pb_, Map.new("{code_block_label}"=>''), code_block_label );
                my ParsedCodeBlock \tpb = ppr_.blocks{fname__};
                my ParsedCodeBlock \pb__ = tpb.clone(inlineable=>inlineable_);
                my Map \ppr_blocks = ppr_.blocks;

                my Map \ppr_blocks_ = insert(ppr_blocks,fname__,pb__);
                my ParsedProgram \ppr__ = ppr_.clone(blocks=>ppr_blocks_);
                ppr__;
            } else {
                ppr_;
            }
        },
        parsed_program_,
        |parsed_program_.blocks-sequence
    );

} # END of _loop_detection

sub __loop_detection_rec( ParsedProgram \ppr, ParsedCodeBlock \pb, Map \seen_labels, Str \block_label --> List) {
    say '-' x 80,"\n",&?ROUTINE.name, '(' ~block_label~ ')' if $DBG;
    my (\_, \inlineable,\fname) = reduce(
        -> \acc_, \token_ {
            my  (\prev_token, \inlineable_, \block_label_) = acc_;
            # The idea here is to follow through any call to see if it is a loop
            if token_ ~~ Operation and token_.opcode.name eq ( 'JCN' | 'JSR' | 'JMP' ) and token_.Str ne 'JMP2r' {
                if prev_token ~~ FunctionAddr {
                    my \fname_ = prev_token.val;
                    if ppr.blocks{fname_}.node-type ~~ NonLeafFunction and ppr.blocks{fname_}.inlineable ~~ Just and ppr.blocks{fname_}.inlineable.just {
                        if not seen_labels{fname_}:exists {
                            say "__loop_detection_rec: recurse into <{fname_}>" if $DBG;
                            my \seen_labels_ = insert(seen_labels,fname_, block_label);
                            my \pb__ = ppr.blocks{fname_};
                            my (Maybe \inlineable__, Str \fname__) = __loop_detection_rec( ppr, pb__, seen_labels_, fname_ );
                            if inlineable__ ~~ Just and inlineable_ ~~ Just {
                                say "__loop_detection_rec: loop for <{fname__}> ? "~ (not inlineable__.just) if $DBG;
                                [token_, inlineable__.just && inlineable_.just,fname__ ]
                            } else {
                                [token_, Nothing.new,fname__ ]
                            }
                        } else { # A loop, so this is not a tail call.
                            say "__loop_detection_rec: loop for <{fname_}>" if $DBG;
                            [token_,Just[False].new,fname_ ];
                        }
                    } else {
                        [token_, inlineable_, block_label_ ];
                    }
                } else {
                    [token_, inlineable_, block_label_ ];
                }
            } else {
                [token_, inlineable_, block_label_ ];
            }
        },
        [NoToken.new, Just[True].new,block_label],
        |pb.code
    );
    [inlineable,fname];
}

=begin pod
Pass1: analysis of arguments
I will have to introduce function arguments, but the problem is that I don't quite know how many. In fact, with every function block we should start counting afresh.
=> It's just a matter of counting the inputs and outputs of all operations:
- Start with  counter = 0  and reg_counter = 0
- For each elt on the stack (const, address),  counter += wordsize
- For each operation counter -= number_of_args*size
- If the result is negative, there is a stack underflow, so push fresh regs [',&r',++reg_counter,wordsize]  onto the stack and do counter+=wordsize until counter == 0
- In the case of a JMP, we need to follow through to the jump location *unless the address is that of a subroutine* ; if it is a subroutine, we need to query its arg count and if that is not known yet, do that one first.
=end pod

# This seems rather restrictive: only functions called with JSR2 and that have function args are considered
# Should we not consider all functions and calls, i.e. also JMP2 and JCN2 ?
# I think in the end a better way is probably to keep the stack for any function that is dynamically called
sub _find_callers(ParsedProgram \parsed_program_rep_ --> ParsedProgram) {

    say '-' x 80,"\n",&?ROUTINE.name, "\n", '-' x 80 if $DBG;
    reduce(
        -> \ppr_glob,\code_block_label {
            say "_find_callers LABEL {code_block_label}" if $DBG;
            my ParsedCodeBlock \parsed_block = ppr_glob.blocks{code_block_label};

            my (\st,\ppr_glob_) = reduce(
                -> \st_ppr_loc,\token_ {
                    my (List \prev_tokens_stack, ParsedProgram \ppr_loc) = st_ppr_loc;
                    my \st_ppr_loc_ = do {
                        if token_ ~~ Operation and token_.opcode.name eq ( 'JSR' | 'JMP' | 'JCN' ) {
                            my Token \prev_token_ = prev_tokens_stack.elems == 0 ?? NoToken.new !! top(prev_tokens_stack);
                            my Map \ppr_loc_callers__ = do {
                                if prev_token_ ~~ (FunctionAddr|LitAbsAddr) {
                                    my List \prev_tokens_stack_ = init(prev_tokens_stack);
                                    my \function_args = map ->\t {t.val}, grep ->\t { t ~~ (FunctionAddr|LitAbsAddr) }, |prev_tokens_stack_;
                                    # Read this as 'the call to prev_token_.val in code_block_label has function_args'
                                    # If there are multiple, the function args must have compatible types so overwriting is OK
                                    say "_find_callers: FOUND {prev_token_.val} => {code_block_label}: {function_args}" if $DBG;
                                    my Map \ppr_loc_callers = ppr_loc.callers;
                                    my Map \ppr_loc_callers_ =
                                        ppr_loc_callers{prev_token_.val}:exists
                                        ?? ppr_loc_callers
                                        !! insert(ppr_loc_callers,prev_token_.val,Map.new);

                                    my Map \ppr_loc_callers_prev_token = insert(ppr_loc_callers_{prev_token_.val}, code_block_label, function_args);
                                    insert(ppr_loc_callers_,prev_token_.val,ppr_loc_callers_prev_token);
                                } else {
                                    ppr_loc.callers
                                }
                            }
                            [
                                List.new,
                                ppr_loc.clone(
                                    callers=>ppr_loc_callers__
                                )
                            ];
                        } else {
                            my List \prev_tokens_stack_ = append(prev_tokens_stack,token_);
                            [prev_tokens_stack_,ppr_loc];
                        }
                    };
                    st_ppr_loc_;
                },
                [List.new,  ppr_glob],
                |parsed_block.code
            );
            ppr_glob_;
        },
        parsed_program_rep_,
        |parsed_program_rep_.blocks-sequence
    );

} # END of _find_callers

# LitAbsAddr tokens that refer to subroutines

sub _identify_function_addrs(ParsedProgram \parsed_program_rep_ --> ParsedProgram) {

    say '-' x 80,"\n",&?ROUTINE.name, "\n", '-' x 80 if $DBG;
    reduce(
        -> \ppr_glob,\code_block_label {
            say "_identify_function_addrs LABEL {code_block_label}" if $DBG;
            my ParsedCodeBlock \parsed_block = ppr_glob.blocks{code_block_label};

            my \parsed_block_code = reduce(
                -> \parsed_block_code,\token_ {
                    if token_ ~~ LitAbsAddr
                    and ppr_glob.blocks{token_.val}:exists and
                    ppr_glob.blocks{token_.val}.block-type ~~ FunctionBlock
                    # and ppr_glob.blocks{token_.val}.function ~~ Just
                    # and ppr_glob.blocks{token_.val}.function.just
                    {
                        my Token \fun_token_ = FunctionAddr[token_.val, token_.wordsz].new;
                        append(parsed_block_code,fun_token_);
                    } else {
                        append(parsed_block_code,token_);
                    }
                },
                List.new,
                |parsed_block.code
            );
            my ParsedCodeBlock \parsed_block_ = parsed_block.clone(code=>parsed_block_code);
            my Map \blocks_ = insert(ppr_glob.blocks,code_block_label,parsed_block_);
            ppr_glob.clone(blocks=>blocks_);
        },
        parsed_program_rep_,
        |parsed_program_rep_.blocks-sequence
    );

} # END of _identify_function_addrs

sub _collect_labels(ParsedProgram \parsed_program_ --> ParsedProgram) {
    say '-' x 80,"\n",&?ROUTINE.name if $DBG;
    reduce(
        -> \ppr_,\code_block_label {
            say "_collect_labels BLOCK {code_block_label}" if $DBG;
            my ParsedCodeBlock \parsed_block = ppr_.blocks{code_block_label};
            # Check for calls to the label itself, cond and uncond recursion
            my (\_,\labels) = reduce(
                -> \acc, \token_ {
                    my (\prev_token_,\labels_) = acc;

                    if token_ ~~ Operation and token_.opcode.name eq ( 'JCN' | 'JMP' )
                            and prev_token_ ~~ ( LitAbsAddr | FunctionAddr )
                            and prev_token_.val ne code_block_label
                    {
                            [token_,insert(labels_,prev_token_.val,True)]
                    } else {
                            [token_,labels_]
                    }
                },
                [NoToken,Map.new],
                |parsed_block.code
            );
            my ParsedCodeBlock \parsed_block_ = parsed_block.clone(labels=>labels);
            my Map \blocks_ = insert(ppr_.blocks,code_block_label,parsed_block_);
            ppr_.clone(blocks=>blocks_);
        },
        parsed_program_,
        |parsed_program_.blocks-sequence
    );

} # END of _collect_labels

