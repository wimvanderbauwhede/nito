use v6;
unit module UxntalStackBasedCEmitter:ver<1.1.0>:auth<codeberg:wimvanderbauwhede>;
use UxntalTypes;
use UxntalStackBasedCEmitterTypes;
use UxntalDefs;
use ImmutableDatastructureHelpers;
use UxntalStackBasedCOpsEmitter; # generate-C-op(Token \token_, Int \cnt --> List[CGen])

my $UXNEMU = True;
my $OPT = 2; # can be 0, 1 or 2
my $OPT_INLINE_OPS = True;
my $OPT_STACK2REG = True;
my $OPT_CALL2GOTO = False;

# Current opts are inlining of ops, stack to reg, inlining of subroutines and replacing subroutine calls by goto
# Inlining of subroutines is done in an earlier pass so -O=3 results in $OPT=2. This is not very nice, it might be better to have strings for the opts, and have some convenience grouping
# So e.g. -O=inline-ops,stack-to-reg,inline-subs,call-to-goto
# But for the moment, I  say that
# -O=1=inline-ops
# -O=2=inline-ops,stack-to-reg
# -O=3=inline-ops,stack-to-reg,call-to-goto
# -O=4=inline-ops,stack-to-reg,inline-subs
# -O=5=inline-ops,stack-to-reg,inline-subs,call-to-goto
# -O=6=inline-ops,stack-to-reg,subs-stack-to-reg
# -O=7=inline-ops,stack-to-reg,inline-subs,subs-stack-to-reg
# -O=8=inline-ops,stack-to-reg,inline-subs,subs-stack-to-reg,call-to-goto


my $mem =  $UXNEMU
    ?? 'u->ram'
    !! 'mem';

my $zeropage =  $UXNEMU
    ?? 'u->ram'
    !! 'zeropage';

our $DBG = True;
my $DBG_LVL = 3;

my \CGens_to_skip =
    $DBG_LVL == 3 ?? ( Debug | Protect | Comment )
    !! $DBG_LVL == 2 ?? ( Debug | Protect  )
    !! $DBG_LVL == 1 ??  Debug
    !! Protect
;
our %binops =

    EQU => '==',
    NEQ => '!=',
    GTH => '>',
    LTH => '<',

    ADD => '+',
    SUB => '-',
    MUL => '*',
    DIV => '/',
    AND => '&',
    ORA => '|',
    EOR => '^'
;

# SFT a b>>c8l<<c8h
my $prefix;

sub emit_stack_based_C_program( ParsedProgram \parsed_program, Str \tal_file, Int \opt ) is export {
    $DBG = $UxntalStackbasedCEmitter::DBG;
    $OPT = opt == 3 ?? 2 !! opt;
    $OPT_INLINE_OPS = opt > 0 ?? True !! False;
    $OPT_STACK2REG = opt > 1 ?? True !! False;

    $OPT_CALL2GOTO = opt > 3 ?? True !! False;
    # $prefix is a global used in c_identifier(), lazy.
    $prefix =  tal_file.split('.').head.trans( '-' => '_'); ;

    say '=' x 80,"\n",&?ROUTINE.name if $DBG;
    my (
        List \zp_cnts,List \mem_cnts,
        List \preamble,
        List \zeropage,List \mem,
        List \decls,List \fptr_addr_tups, List \ptt
    ) =
    reduce(
        -> \st_ptt_,\code_block_label {

            my (
                List \zp_cnts_,List \mem_cnts_,
                List \preamble_,
                List \zeropage_,List \mem_,
                List \decls_,List \fptr_addr_tups_,List \ptt_
                )=st_ptt_;
                # say "MEM CNTS ({code_block_label}): {mem_cnts_}";
            my (
                Int \zp_cnt_, Int \mem_cnt_,
                List \preamble__,
                List \zeropage__, List \mem__,
                List \decls__, List \fptr_addr_tups_block, List \block_C_tokens0
                ) = _emit_C_block(zp_cnts_.head,mem_cnts_.head,parsed_program,code_block_label);
                # say "MEM CNTS after _emit_C_block({code_block_label}): {mem_cnt_},{mem_cnts_}";
            [
                [zp_cnt_,|zp_cnts_],
                [mem_cnt_,|mem_cnts_],
                [|preamble_,|preamble__],
                [|zeropage_,|zeropage__],
                [|mem_,|mem__],
                [|decls_,decls__],
                [|fptr_addr_tups_,|fptr_addr_tups_block],
                [|ptt_, |block_C_tokens0]
            ];

        },
        [
            [0], # zero-page counters
            [256], # memory counters
            [ # preamble
             "/* {tal_file} */",
             '#include <stdlib.h>',
             '#include <stdio.h>',
             '#include <string.h>',
             '#include "../uxn-runtime-libs/uxn-runtime.h"'
            # $OPT ?? '#define DBG' !! '',
            # $OPT ?? '#define DBG_' !! '',
            # $OPT ?? '#define DEFENSIVE' !! ''
            ], # END preamble
            [], # zero-page
            ['\0' xx 256,], # memory
            [], # subroutine decls
            [], # function pointer address tuples, for RawAbsAddr
            [] # code
        ],
        |parsed_program.blocks-sequence
    );

    # What we need is some way to check if mem is actually used, and if not we don't instantiate it
    my Str \stack_init= 'static char ws[256]; static char rs[256];';

    my Seq \mem_rhs_seq = map  ->\lst { map ->\x { "'" ~ x ~ "'" },|lst  }, |mem;
    my @mem_rhs=mem_rhs_seq.flat;
    my $mem_ct= (elems @mem_rhs);
    my Str \mem_sz_def = '#define MEM_CNT ' ~$mem_ct;
    my List \mem_init =
    $mem_ct > 256 ??
    [
        mem_sz_def,
        '#if MEM_CNT > 0',
        'static unsigned char _mem[MEM_CNT] = {' ~ join(',',@mem_rhs) ~ '};',
        '#endif'
    ] !! [mem_sz_def];
# die fptr_addr_tups.raku;
    my List \fptr_init_code = _emit_fptr_init_code(fptr_addr_tups);

    my \init_ram_strs = [
        '',
        'void zero_interrupt(void) {',
        '    /* printf("zero_interrupt()\n"); */',
        '}',
        '',
        'int init_ram (Uxn* u) {',
        ($UXNEMU ?? 'u=&uxn;' !! ''),
        '#if MEM_CNT >256',
        '   memcpy((void *)'~$mem~', (const void *)_mem,MEM_CNT);',
        '#endif',
        '   int i;',
        '   for (i=0;i<256;i++) {',
        '       functions[i]=(void*)zero_interrupt;',
        '   }',
        |fptr_init_code,
        '   return 0;',
        '}',
    ];

    return grep ->\l { l.elems>0 }, [
        |preamble,
        # stack_init,
        # |zeropage_init,   # This is always zero so no point?
        |mem_init,
        |decls,
        |init_ram_strs,
        |ptt
    ];

} # END of emit_stack_based_C_program

=begin pod
To emit a C function:
- We will assume all functions are void
- All args will be re-assigned
- All return values are pointers, re-assigned to scalars

So we emit
    "void $code_block_label ($args_str,$retvals_str) {"

=end pod

sub _emit_C_block(Int \zeropage_cnt, Int \mem_cnt, ParsedProgram \parsed_program, Str \code_block_label --> List) {
    # say "MEM COUNT _emit_C_block({code_block_label}): {mem_cnt}";
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    my ParsedCodeBlock \parsed_block = parsed_program.blocks{code_block_label};
    if code_block_label eq '0100' {
        _emit_C__emit_C_main(zeropage_cnt, mem_cnt, parsed_program,code_block_label);
    }
    # elsif parsed_block.function ~~ Just and parsed_block.function.just {
    elsif parsed_block.block-type ~~ FunctionBlock {
        _emit_C_function( zeropage_cnt, mem_cnt, parsed_program, code_block_label);
    }
    else {
        # die parsed_block.raku; #if code_block_label eq 'display-rabbit/start';
        _emit_C_block_allocations(zeropage_cnt, mem_cnt, parsed_program,code_block_label);
    }
} # END of _emit_C_block

sub _replace_jumps_by_calls(ParsedCodeBlock \parsed_block) {
    my @parsed_block_code = |parsed_block.code;
    my $max_idx = @parsed_block_code.elems-1;
    my @parsed_block_code_with_calls;
    my $skip=0;

    for 0 ..  @parsed_block_code.elems-1 -> $i {
        my $token = @parsed_block_code[$i];
        my $next_token = $i+1 <= $max_idx ?? @parsed_block_code[$i+1] !! NoToken.new;
        my $next_next_token = $i+2 <= $max_idx ?? @parsed_block_code[$i+2] !! NoToken.new;
        # say "$i:$token,$next_token,$next_next_token";
        # die 'FIXME: if we have @f_1 ;f_2 JCN then this breaks because there is no instr for the cond';
        # SO we must somehow make sure that this call will use the stack, so I guess we insert a NoToken instead?
        if $token ~~ FunctionAddr and $next_token ~~ Operation and $next_token.opcode.name eq 'JCN'
        {

            my Token \call_token_ = CondCall[NoToken.new,$token,$next_token].new;
            # say "COND CALL {$next_token.val}";
                @parsed_block_code_with_calls.push(call_token_);
                $skip=1;
        }
        elsif $token ~~ FunctionAddr and $next_token ~~ Operation and $next_token.opcode.name eq (  'JMP' | 'JSR' )
        {
            my Token \call_token_ = Call[$token,$next_token,$next_token.opcode.name eq 'JSR'  ].new;
            # say "CALL {$token.val}";
            @parsed_block_code_with_calls.push(call_token_);
            $skip=1;
        }
        elsif
        $token ~~ Operation # and %cmp_operations{ $token.opcode.name }:exists # Turns out LDZ JMP is also used
        and $next_token ~~ Operation and $next_token.opcode.name eq ( 'JMP' | 'JSR' ) and not $next_token.opcode.rs
        {
            if $next_token.opcode.name eq 'JSR'  {
                if $next_token.opcode.wordsz==0 {
                    die "Sorry, can't handle a JSR to a calculated position, "
                } else {
                    # I guess we could simply assume that the JSR means there is a function pointer on the stack.
                    @parsed_block_code_with_calls.push($token);
                    @parsed_block_code_with_calls.push($next_token);
                    $skip=1;
                }
            } else {
                if $next_next_token ~~ Operation and $next_next_token.opcode.name eq 'BRK' {
                    my Token \condret_token_ = CondReturn[$token].new;
                    # say "CALL {$token.val}";
                    @parsed_block_code_with_calls.push($token) unless $token.opcode.name eq 'JSR' ;
                    @parsed_block_code_with_calls.push(condret_token_);
                    $skip=2;
                } else {
                    my Token \condjmp_token_ = CondJump[$token,$next_next_token].new;
                    # say "CALL {$token.val}";
                    @parsed_block_code_with_calls.push($token) unless $token.opcode.name eq 'JSR' ;
                    @parsed_block_code_with_calls.push(condjmp_token_);
                    $skip=2; # This should skip the actual operation
                }
            }
        }
        elsif $skip==0
        {
            @parsed_block_code_with_calls.push($token);
        } else {
            # say "SKIP: $token";
            $skip--;
        }
    }
    List.new(|@parsed_block_code_with_calls);
} # END of _replace_jumps_by_calls

sub _emit_C_function(Int \zeropage_cnt, Int \mem_cnt, ParsedProgram \parsed_program,Str \code_block_label --> List) {
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    my ParsedCodeBlock \parsed_block = parsed_program.blocks{code_block_label};
    # say parsed_block.raku;
    my List \parsed_block_code = _replace_jumps_by_calls(parsed_block); # parsed_block.code; #

# say parsed_block_code.raku;
    my (\label,\parsed_block_code_) = headTail(parsed_block_code);

    # my List \block_C_tokens=[];
    my List \preamble=[];
    my List \zeropage=[];
    my List \mem=[];

    my List \decls= List.new("void {c_identifier(label.val)}("  ~ ');');

    my List \block_C_tokens = List.new( '',
    'void ' ~ c_identifier(label.val) ~ '( void ) {',
    '#ifdef DBG' ,
    'printf("'~ label.val ~ '\n");',
    '#endif'
    # "\t" ~ c_identifier(label.val) ~ '_entry: 1;',
    );

    my (List \block_C_tokens_, Int \mem_cnt_,List \preamble_,List \mem_) = __emit_C_block_common(parsed_program,code_block_label, parsed_block_code_,block_C_tokens,mem_cnt,preamble,mem);

    return [
        zeropage_cnt,mem_cnt_,
        preamble_,
        zeropage,mem_,
        decls,[],block_C_tokens_
    ];
} # END of _emit_C_function


sub _emit_C__emit_C_main(Int \zeropage_cnt, Int \mem_cnt, ParsedProgram \parsed_program,Str \code_block_label --> List) {
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    my ParsedCodeBlock \parsed_block = parsed_program.blocks{code_block_label};
    my List \parsed_block_code =  _replace_jumps_by_calls(parsed_block); # parsed_block.code;#

    my (Token \label, List \parsed_block_code_) =  headTail(parsed_block_code);

    my List \block_C_tokens = [
        '',
        'void run_program (void) {'

    # Zero page memory is always empty so this would just copy zeroes
    # '#if ZP_CNT > 0',
    # 'memcpy((void *)zeropage, (const void *)_zeropage,ZP_CNT);',
    # '#endif',
    # ( $UXNEMU ?? 'uxn.wst.ptr=0;uxn.rst.ptr=0;' !! 'wsp=0;rsp=0;'),
    # ( $UXNEMU ?? 'uxn_port(u, 0x1, nil_dei, console_deo);' !! '')
    ];

    my List \preamble=[];
    my List \zeropage=[];
    my List \mem=[];
    my List \decls=[];
    # my List \block_C_tokens_ = __emit_C_block_common(parsed_program,code_block_label,parsed_block_code_,block_C_tokens);
    my (List \block_C_tokens_, Int \mem_cnt_,List \preamble_,List \mem_) = __emit_C_block_common(parsed_program,code_block_label, parsed_block_code_,block_C_tokens,mem_cnt,preamble,mem);


    return [
        zeropage_cnt, mem_cnt_,
        preamble_,
        zeropage,mem_,
        decls,[],block_C_tokens_
    ];
} # END of _emit_C_main

sub __emit_C_block_common(ParsedProgram \parsed_program,Str \code_block_label,List \parsed_block_code, List \block_C_tokens_,
Int \mem_cnt, List \preamble, List \mem
 --> List) {
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    my (List \block_C_tokens,Map \local_decls,Int \mem_cnt_, List \preamble_, List \mem_) =
    reduce(
        -> \acc_, \token_ {
            # To make this block scoped, it means we need to store them temporarily and then put then straight after the function signature.
            my (List \block_C_tokens__,Map \local_decls_,
            Int \mem_cnt__, List \preamble__, List \mem__, Int \cnt) = acc_;
            my Array \blockvars_ = [];

            my (Int \mem_cnt_3, List \preamble_3, List \mem_3) = do {
                if token_ ~~  Literal and token_.tokens.elems>1 {
                    reduce(
                        -> \acc_, \t_ {
                        my (\mem_cnt_i, \preamble_i,\mem_i) = acc_;
                        if t_ ~~ AbsLabel {
                            my Token \label = t_;
                            my List \preamble_2 =
                                append( preamble_i, 'static short ' ~ c_identifier(label.val) ~ ' = ' ~ mem_cnt_i ~ ';' );
                            [
                                mem_cnt_i,
                                preamble_2,
                                mem_i,
                            ];
                        }  elsif t_ ~~ RelPadding {
                            my List \mem_2 =  append( mem_i,  '\0' xx t_.wordsz );
                            my Int \mem_cnt_2 = mem_cnt_i + t_.wordsz;
                            [
                                mem_cnt_2,
                                preamble_i,
                                mem_2,
                            ];
                        }  elsif t_ ~~ RawAsciiChar {
                            my List \mem_2 =  append( mem_i, "'"  ~ t_.val ~ "'" );
                            my Int \mem_cnt_2 = mem_cnt_i + 1;
                            [
                                mem_cnt_2,
                                preamble_i,
                                mem_2,
                            ];
                        }  elsif t_ ~~ RawHex {
                            my List \mem_2 =  append( mem_i,  '\x' ~t_.val );
                            my Int \mem_cnt_2 = mem_cnt_i + t_.wordsz;
                            [
                                mem_cnt_2,
                                preamble_i,
                                mem_2,
                            ];
                        } else {
                            die "TODO: {t_.raku}";
                        }
                    },
                    [mem_cnt__,preamble__,mem__],
                     |token_.tokens
                    );
                } else {
                    [mem_cnt__,preamble__,mem__];
                }
            };
            my (\local_decls__,\c_tokens_seq) = __emit_C_token(token_,code_block_label,parsed_program,local_decls_);

            my @c_gens_ = $OPT_INLINE_OPS ?? generate-C-op(token_,cnt,$prefix) !! [];
            my @c_gens = $DBG_LVL >0  ?? List.new(|(grep ->\cg {not (cg ~~ CGens_to_skip) },@c_gens_)) !! @c_gens_;

            # So we do block_C_tokens__ ++ [(snd . fst c_tokens_seq) ++ ";"]
            [
                List.new(|block_C_tokens__,($OPT_INLINE_OPS ?? |@c_gens !! c_tokens_seq.head ~';')
                ),
                local_decls__,
                mem_cnt_3,
                preamble_3,
                mem_3,cnt+1
            ];
        },
        [
        block_C_tokens_,Map.new,
        mem_cnt,preamble,mem,0
        ],
        |parsed_block_code
    );
    # I can use this for functions I guess.
    # die local_decls.raku;
    my List \block_C_tokens__ = append( block_C_tokens, '}');
    if $OPT_STACK2REG {
        my @c_gens_opt =
        # $OPT_STACK2REG ??
        stack_to_reg_optimisation(block_C_tokens__,$DBG_LVL) ;
        # !! block_C_tokens__;
        [@c_gens_opt,mem_cnt_,preamble_,mem_]
    } else {
    # say "MEM COUNT end of __emit_C_block_common: {mem_cnt_}";
        [block_C_tokens__,mem_cnt_,preamble_,mem_]
    }
} # END of __emit_C_block_common

sub _emit_C_block_allocations(Int \zeropage_cnt, Int \mem_cnt, ParsedProgram \parsed_program,Str \code_block_label --> List) {
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    # say "MEM COUNT start of _emit_C_block_allocations({code_block_label}): {mem_cnt}";
    my ParsedCodeBlock \parsed_block = parsed_program.blocks{code_block_label};

    my List \parsed_block_code = parsed_block.code;
    my List \block_C_tokens=[];
    my List \preamble=[];
    my List \zeropage=[];
    my List \mem=[];
    my List \decls=[];

    # if code_block_label eq '0000' {
    if parsed_block.block-type ~~ ZeroPage or parsed_block.block-type ~~ DeviceMem {
        # These blocks are named by the absolute padding
        my Int \zeropage_cnt = hex2dec( code_block_label).Int;
        # say "DeviceMem: {code_block_label} => {zeropage_cnt}";
        if parsed_block_code.elems > 1 {
            my List\parsed_block_code_1 =
                parsed_block_code.head ~~ AbsPadding
                ?? tail(parsed_block_code)
                !! parsed_block_code;
            my List\parsed_block_code_2 =
                # parsed_block_code_1[*-1] ~~ RegDecls
                # ?? init(parsed_block_code_1)
                # !!
                parsed_block_code_1;
            my List\parsed_block_code_3 =
                parsed_block_code_2[*-1] ~~ AbsLabel
                # Ad hoc, just pad 2 bytes
                ?? append(parsed_block_code_2, RelPadding['2'].new)
                !! parsed_block_code_2;
            # Split the zero-page on the labels and then treat them like the other labels
            my (Map \zplabels,  List \zplabelseq, Str \current_label) =
            reduce(
                -> \acc_,\token_ {
                    my (Map \zplabels_,  List \zplabelseq_, Str \current_label_) = acc_;
                    if token_ ~~ AbsLabel {
                        [
                            insert(zplabels_, token_.val,List.new),
                            append( zplabelseq_, token_),
                            token_.val
                        ];
                    }
                    elsif token_ ~~ AbsPadding {
                        [
                            zplabels_,
                            append( zplabelseq_, token_),
                            current_label_
                        ];
                    } else {
                        my Map \dummy_for_test = Map.new(zplabels_.list);
                        my List \zplabels_current_label_ =
                            dummy_for_test{current_label_}:exists
                            ?? dummy_for_test{current_label_}
                            !! List.new;
                        my \zplabels_current_label__=append(zplabels_current_label_,token_);
                        [
                            insert( zplabels_, current_label_, zplabels_current_label__),
                            zplabelseq_,
                            current_label_
                        ];
                    }
                },
                [Map.new,List.new,''],
                |parsed_block_code_3
            );
            # say 'HERE1',zplabelseq.raku;
            # This is similar to the mem code
            my (List \preamble_, List \zeropage_, Int \n_zp_bytes_)=
            zplabelseq.elems == 0
            ?? [preamble,zeropage,0]
            !! reduce(
                -> \acc_, Token \zptoken {
                    my (List \preamble__, List \zeropage__, Int \n_zp_bytes__) = acc_;
                    if zptoken ~~ AbsLabel {
                        my Str \zplabel = zptoken.val;
                        my Map \bug_workaround = Map.new(zplabels.list);
                        my \zp_alloc = bug_workaround{zplabel};
                        # my \zp_alloc = zplabels{zplabel};
                        if parsed_block.block-type ~~ ZeroPage {
                        [
                            append( preamble__, 'static short ' ~ c_identifier(zplabel) ~ ' = ' ~ (zeropage_cnt+n_zp_bytes__) ~ ';' ),
                            append( zeropage__, to-bytes(zp_alloc)),
                            n_zp_bytes__ + [+] map ->\t {t.wordsz} , |zp_alloc
                        ]
                        } else {
                        [
                            append( preamble__, 'static short ' ~ c_identifier(zplabel) ~ ' = ' ~ (zeropage_cnt+n_zp_bytes__) ~ ';' ),
                            zeropage__,
                            n_zp_bytes__ + [+] map ->\t {t.wordsz} , |zp_alloc
                        ]
                        }

                    } elsif  zptoken ~~ AbsPadding and zptoken.val ne '0000' {
                        my @padding = '\0' xx (Int.new( hex2dec(zptoken.val)) - zeropage_cnt);
                        [
                            preamble__,
                            append( zeropage__, @padding),
                            Int.new( hex2dec(zptoken.val))
                        ];
                    }
                },
                [preamble,zeropage,0],
                |zplabelseq
            ); #reduce
            # say "MEM COUNT end of _emit_C_block_allocations(ZP): {mem_cnt}";
            return [
                zeropage_cnt + n_zp_bytes_,mem_cnt,
                preamble_,
                zeropage_,mem,
                decls,[],block_C_tokens
            ];
        } else {
            # say "MEM COUNT end of _emit_C_block_allocations(empty ZP): {mem_cnt}";
            return [
                zeropage_cnt,mem_cnt,
                preamble,
                zeropage,mem,
                decls,[],block_C_tokens
            ];

        }
    } else { # memory allocation
        __emit_C_mem_allocations(zeropage_cnt,mem_cnt,parsed_program,code_block_label);
    }

} # END of _emit_C_block_allocations

sub __emit_C_mem_allocations(\zeropage_cnt,\mem_cnt,\parsed_program,\code_block_label --> List) {
    say '-' x 80,"\n",&?ROUTINE.name,"({code_block_label})" if $DBG;
    my ParsedCodeBlock \parsed_block = parsed_program.blocks{code_block_label};
    my List \parsed_block_code = parsed_block.code;
    my List \block_C_tokens=[];
    my List \preamble=[];
    my List \zeropage=[];
    my List \mem=[];
    my List \decls=[];

    my (\label,\parsed_block_code_) = headTail( parsed_block_code);
    my \parsed_block_code__ =
    parsed_block_code_.elems == 0
        ?? []
        !!
        # top(parsed_block_code_) ~~ RegDecls
        #     ?? init(parsed_block_code_)
        #     !!
            parsed_block_code_;
    if label ~~ AbsLabel {
        my \preamble_ =
            append( preamble, 'static short ' ~ c_identifier(label.val) ~ ' = ' ~ mem_cnt ~ ';' );
        my \mem_ = append( mem, to-bytes(parsed_block.code));
        my \n_bytes_ =
            parsed_block_code__.elems == 0
            ?? 0
            !! [+] map ->\t {
                # say label,':', t.raku;
                t.wordsz
                } , grep ->\t { not t ~~ AbsLabel }, |parsed_block_code__;
            # say "MEM COUNT end of _emit_C_block_allocations({label}): {mem_cnt} + {n_bytes_}";
        # This creates a list of (fname,addr) tuples
        my (List \fptr_addr_tups,  \n_bytes__) = _get_fptr_addrs(parsed_block.code,mem_cnt);
        die "Assert: {label.val}: {mem_cnt}: {n_bytes_} <> {n_bytes__}" if n_bytes_ != n_bytes__;
        [
            zeropage_cnt, mem_cnt + n_bytes_ ,
            preamble_,
            zeropage,mem_,
            decls,fptr_addr_tups,block_C_tokens
        ];
    } elsif  label ~~ AbsPadding and label.val ne '0000' {
        my @padding = '\0' xx (Int.new( hex2dec(label.val)) - mem_cnt);
        my \mem_ = append( mem, @padding);
        say "MEM COUNT end of _emit_C_block_allocations({label}): {mem_cnt}";
        [
            zeropage_cnt,Int.new( hex2dec(label.val)),
            preamble,
            zeropage,mem_,
            decls,[],block_C_tokens
        ];
    }
    # die if code_block_label eq 'rabbits';
} # END of __emit_C_mem_allocations

# local_decls is pure pass-through
#  block_label_ and parsed_program_ are ignored
# the return values is actuall (_,(str)) and so this is simply __emit_C_token_str(token_)
sub __emit_C_token(Token \token_, Str \block_label_,  ParsedProgram \parsed_program_ , Map \local_decls --> List){
    say '-' x 40,"\n",&?ROUTINE.name if $DBG;
    # say "TOKEN:",token_ if $DBG;
    [local_decls,[__emit_C_token_str(token_,block_label_)]];
} # END of __emit_C_token


sub __emit_C_token_str(Token \token_, Str \block_label_--> Str){
    say '-' x 40,"\n",&?ROUTINE.name if $DBG;
    # say "TOKEN:",token_ if $DBG;

    if token_ ~~ Call {
        my \fname = c_identifier( token_.fname.val );
         fname ~ '()';
    } elsif token_ ~~ CondCall {
        # no point in using goto, it's not any faster
        if $OPT_CALL2GOTO and token_.fname.val eq block_label_ {
            'if ( GET1() ) { goto ' ~ c_identifier( token_.fname.val ) ~ '_entry; }';
        } else {
            'if ( GET1() ) { ' ~ c_identifier( token_.fname.val ) ~ '(); return; }';
        }
    } elsif token_ ~~ CondReturn {

        '{
    unsigned char cond = GET1();
    #ifdef DEFENSIVE
    if (cond>1) {
        printf("Sorry, computed jumps only supported with booleans or constant values: ' ~ token_.cond.Str ~ ' %d\n",cond);
        exit(0);
    }
    #endif
    if ( !cond ) { return; }' ~ "\n" ~ '}';

    } elsif token_ ~~ CondJump {
        my Str \op_str = __emit_C_token_str(token_.op,block_label_);

        '{
    unsigned char cond = GET1();
    #ifdef DEFENSIVE
    if (cond>1) {
        printf("Sorry, computed jumps only supported with booleans or constant values: ' ~ token_.cond.Str ~ ' %d\n",cond);
        exit(0);
    }
    #endif
    if ( !cond ) { ' ~ op_str ~ '; }' ~ "\n" ~ '}';

    } else {
            if %const_operand_types{token_.type}:exists
            or %labels_padding{token_.type}:exists
            or %raw_operand_types{token_.type}:exists
            or token_ ~~ Literal
            {
                    __emit_C_value(token_);
            } elsif token_ ~~ Operation {
                    __emit_C_opcode(token_);
            } elsif token_ ~~ EvalOpcode {
                'EVAL()';
            } else {
                die "__emit_C_token UNHANDLED token : {token_.raku}";
            }
    }
    # }
} # END of __emit_C_token_str

sub __emit_C_decl (Token \argval --> Str) {

    # if argval ~~ Reg {
    #      (argval.wordsz==1 ?? 'char' !! 'short') ~ ' r' ~ argval.cnt ~ '_;';
    # } els
    if argval ~~ NoRetVal {
        ''
    } else {
        die '__emit_C_apreamble_rgval: Unsupported return value: ' ~ argval.raku;
    }
}



sub __emit_C_value(Token \token_ --> Str) {
    say "__emit_C_value TOKEN: {token_.raku}" if $DBG;
        if token_ ~~ RawHex {
          "PUSH{token_.wordsz}( {token_.val} )"
        }
        elsif token_ ~~ RawAsciiChar {
            "'" ~ token_.val ~ "'"
        }
        elsif token_ ~~ RawAsciiWord {
            '"' ~ token_.val ~ '"'
        }
        elsif token_ ~~ HexLiteral {
            my $val = hex2dec(token_.val);
            "PUSH{token_.wordsz}( $val )"
        }
        elsif token_ ~~ Literal {
            my $lit_tokens_str= '/* LIT' ~ token_.wordsz ~ ' */';
            my $prev_token=NoToken.new;
            for |token_.tokens -> \t_ {
                if t_ ~~ AbsLabel {
                    my $val = c_identifier( t_.val);
                    # This must be PUSH2 because it's an absolute address
                    $lit_tokens_str ~= "\nPUSH2{(token_.rs ?? 'r' !! '')}( $val );\nLDA{
                        token_.tokens.elems >2 ?? 1 !! token_.wordsz
                        }{(token_.rs ?? 'r' !! '')}();";
                } elsif (t_ ~~ RawHex  or t_ ~~ HexLiteral) {
                    if not $prev_token ~~ AbsLabel {
                        my $val = hex2dec(t_.val);
                        $lit_tokens_str ~= "\nPUSH{t_.wordsz}{(token_.rs ?? 'r' !! '')}( $val );";
                    }
                } elsif t_ ~~ RawAsciiChar {
                    if not $prev_token ~~ AbsLabel {
                        my $val = "'" ~ t_.val ~"'";
                        $lit_tokens_str ~= "\nPUSH{t_.wordsz}{(token_.rs ?? 'r' !! '')}( $val );";
                    } else {
                        die "TODO: Raw Ascii Char without label in Literal: " ~ t_.raku;
                    }
                } elsif t_ ~~ RawAsciiWord {
                    if not $prev_token ~~ AbsLabel {
                        if t_.val.chars == 1 {
                            my $val = "'" ~ t_.val ~"'";
                            $lit_tokens_str ~= "\nPUSH{t_.wordsz}{(token_.rs ?? 'r' !! '')}( $val );";
                        } else {
                            die "TODO: Raw Ascii Word with more than 1 char: " ~ t_.raku;
                        }
                    } else {
                    die "TODO: Raw Ascii Word without label in Literal: " ~ t_.raku;
                    }
                } elsif t_ ~~ Operation {
                    my $val = _opcode_to_num(t_.opcode);
                    $lit_tokens_str ~= "\nPUSH1{(token_.rs ?? 'r' !! '')}( $val );";
                } elsif not t_ ~~ RelPadding {
                    die "TODO: " ~ t_.raku;
                }
                $prev_token=t_;
            }
            $lit_tokens_str;
        }
        elsif token_ ~~ LitAbsAddr {
            my $val = c_identifier( token_.val);
            "PUSH{token_.wordsz}( $val )"
        }
        elsif token_ ~~ FunctionAddr {
            my $val = c_identifier( token_.val);
            "PUSHFP( $val )"
        }
        elsif token_ ~~ LitZeroPageAddr {
             my $val = c_identifier( token_.val);
             "PUSH{token_.wordsz}( $val )"
        }
        elsif token_ ~~ AbsLabel {
            c_identifier( token_.val) ~ ': 1; /* label needs statement */';
        }
        elsif token_ ~~ RelPadding { # no more Reg: token_ ~~ Reg or
            ''
        #    '['~
        #    __emit_C_arg(token_).join( ' ' );
            # ~ ']';
        }
        else {
            die '__emit_C_value: Unsupported token: ', token_.raku;
        }
}

sub __emit_C_opcode(Token \token_ --> Str) {
    say "__emit_C_opcode({token_.opcode})" if $DBG;

    given token_.opcode.name {
        when 'BRK' {
            # 'exit(0)'
            'return; /* BRK */' # Not entirely sure about this but has to be something
        }
        when 'JMP' { # This basically assumes that the jump is a tail call, and therefore is wrong when we have something like
        # <some calc> JMP, because JMP simply increments the pc with the value.

            'return; /* ' ~  token_.opcode.Str ~ '  */'
        }
        default {
    token_.opcode.name
    ~ ( token_.opcode.wordsz == 1 ?? '1' !! '2')
    ~ (token_.opcode.keep ?? 'k' !!'' )
    ~ (token_.opcode.rs ?? 'r' !! '')
    ~ '()'
        }
    }
}

=begin pod
### C translation

/*
So a working translation rule is:
- All labels are indices into a global mem[] array, which is an array of char.
- So if we store a short in it, we either split the short into two bytes or we see if we can do this through casting:

    short r4_ = ...;
    *((short*)(&mem[r2_])) = r4_;
    short r3_ = *((short*)(&mem[r2_]));

- we need a global memory counter for all labels etc. Although in practice I think we can count non-function labels only. So it might be useful to store the allocated or used size in the block. I am inclined to store the zero-page memory separately.
- we need syntax pattern matching :
given token_ {
    when GlobRegSet {}
    when RegOperation {
        given token_.op.opcode.name {
        when DEO {}
        when INC {}
        when JCN {}
        when JMP {}
        when JSR {}
        when binop{}
        }
    }
    default {}
}
    a #18 DEO => printf("%c",a);
    a INC2 => a+1
    a b NEQ => a != b
    a b JCN => if (a) { goto b ; }
    a JMP => goto a;
    a b f JSR2 r => r = f(a,b)
    a b BINOP => a `binop` b

The zero-page emitter:
- We need to see if the renaming
=end pod

# Note that for raw bytes we can say \x...
sub hex2dec (Str \hexval --> Str ) {
    :16(hexval).Str
}

sub c_identifier (Str \str_ --> Str) {
    my \str__ = TR/ - / _ / with str_;
    my \id = TR/ \/ / _ / with str__;
    $prefix ~ '_' ~ id;
}

sub __emit_C_operation (Token \token_ --> Str) {

    my Str \reg_op_str = do {
        my Opcode \opcode_ = token_.op.opcode;
        given opcode_.name {
            when 'BRK' {
                'exit(0);'
            }
            when 'LDA' {
                if opcode_.wordsz == 1 {
                    ''
                } else {
                    ''
                }
            }
            when 'STA' {
                # die 'TODO';
                if opcode_.wordsz == 1 {
                    ''
                } else {
                    ''
                }
            }
            when 'LDZ' {
                if opcode_.wordsz == 1 {

                } else {

                }
            }
            when 'STZ' {
                if opcode_.wordsz == 1 {
                    ''
                } else {
                    ''
                }
            }
            when 'DEO' {
                'printf("%c",' ~ 'XXX' ~ ');'
            }
            when 'INC' {
                ''
            }
            when 'SFT' {
                ''
            }
            when 'JCN' {
                'if ( '  ~ ' ) {  ' ~  '; }';
            }
            when '' { # Dummy used for return values
                ''
            }
            when %binops{opcode_.name}:exists { # TODO check if this order is right!
                # "{retval_str} {args_strs[0]} {%binops{token_.op.opcode.name}} {args_strs[1]};"
                ''
            }
            default {
                my Str \opcode_str = __emit_C_opcode(token_.op);
                "/* DEFAULT ({token_.op.opcode}) */ <{opcode_str}> ;";
            }
        }
    };

} # END of __emit_C_operation

sub to-bytes ( List \tokens_ ) {

    my @res = (map -> \token_ {
        # say token_;
        my \res = do given token_ {
            when RawAsciiChar { [token_.val] }
            when RawAsciiWord { [token_.val.comb] }
            when RawHex {
                if token_.val.chars == 2 {
                [ '\x' ~ token_.val ]
                }
                elsif token_.val.chars == 4 {
                [ '\x' ~ token_.val.substr(0,2), '\x' ~ token_.val.substr(2,2)]
                }
                else {
                    die "Only 1- and 2-byte hex literals supported: {token_.val}";
                }
                }
            when RelPadding { '\0' xx token_.wordsz }
            when RawAbsAddr {
                # I will assume that a raw absolute address always refers to a subroutine, not to data
                # In the C code, paradoxically these are only known at run time.
                # To make this work, I will need to allocate the indices in the functions[] array at compile time.
                #  I can generate the array statically, just like the memory.
                # What I could do is
                # - register the addresses used for these RawAbsAddr
                # - in the initialisation of functions[], put the corresponding functions first (after 0)
                # - write the indices in those addresses
                [ '\0', '\0']

            }
            default { [ ]}
        }
        # say res;
        res;
    }, |tokens_).flat;
    return @res;
}
sub _get_fptr_addrs(List \tokens_ ,Int \mem_cnt --> List) {
    my $label;
    my $mem_addr = 0;
    my @fptr_addr_tups;
    for |tokens_ -> \token_ {
        # say token_;
        given token_ {
            when RawAsciiChar { $mem_addr+=token_.wordsz }
            when RawAsciiWord { $mem_addr+=token_.wordsz } # val.chars }
            when RawHex {
                $mem_addr+= token_.wordsz;#val.chars/2;
            }
            when RelPadding { $mem_addr += token_.wordsz }
            when RawAbsAddr {
                push @fptr_addr_tups, [ token_.val, $mem_addr+mem_cnt ];
                $mem_addr+=token_.wordsz;
            }
            when AbsLabel {
                $label = token_.val;
            }
            # when Literal {

            # }
            default { die "Unsupported token in $label: " ~ token_.raku }
        }
    }
    [@fptr_addr_tups, $mem_addr]; # a list of pairs
} # END of _get_fptr_addrs

sub _emit_fptr_init_code (List \fptr_addr_tups --> List) {
    my $idx=0;
    my @fptr_init_code;
    for |fptr_addr_tups -> \tup {
        my (\fname,\addr) = tup;
            ++$idx;
            push @fptr_init_code, "    u->ram[{addr}+1] = $idx & 0xF;";
            push @fptr_init_code, "    u->ram[{addr}] = ($idx >> 4) & 0xF;";
            push @fptr_init_code, "    functions[$idx]=(void*)" ~ c_identifier( fname) ~ ';',
    }
    push @fptr_init_code, "    f_idx= $idx + 1;";
    @fptr_init_code
} # END of _emit_fptr_init_code

sub _opcode_to_num(Opcode\opcode) {
    my \wordsz = opcode.wordsz-1;
    my \rs = opcode.rs ?? 1 !! 0;
    my \keep = opcode.keep ?? 1 !! 0;
    my \instr = %opcodes{opcode.name};
    instr + (wordsz +< 5) + (rs +< 6) + (keep +< 7);
}


=begin pod
Demos status

- dvd is OK opt=0,1,2
- polycat is OK opt=0,1,2
- snake_proc is OK for opt=0,1,2
- amiga is OK for opt=0,1,2
- move is OK for opt=0,1,2
- life is OK for opt=0,1,2
- bifurcan is OK for opt=0,1,2
- bitwise is OK for opt=0,1,2
- bunnymark is OK opt=0,1,2
- wireworld is OK opt=0,1,2
- cube3d is OK for opt=0,1,2
- mandelbrot_proc

- piano
- calc
- ray

- drool segmentation fault or overflow (generated tal works but not correctly)

=end pod