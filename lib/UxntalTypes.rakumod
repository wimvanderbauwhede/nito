use v6;

# Just a hack to provide version information with the same syntax as the other modules
role UxntalTypes:ver<1.1.0>:auth<codeberg:wimvanderbauwhede> {};

role Maybe {}
role Nothing does Maybe {
        method combine(Maybe \m_, &f_) {
        if self ~~ Nothing {
            Nothing.new
        } else {
            &f_( self, m_ )
        }
    }
}
role Just[\v] does Maybe {
    has $.just = v;
    method combine(Maybe \m_, &f_) {
        if self ~~ Nothing {
            Nothing.new
        } else {
            &f_( self, m_ )
        }
    }
}


role Token {}

role RawHex[Str \val_,  \wordsz_] does Token {
    has Str $.type = 'raw';
    has Str $.val = val_;
    has Int $.wordsz = Int.new(wordsz_);
    method tuple {
        [$.val,$.wordsz]
    }
    method Str {
        "0x{$.val}:{$.wordsz}"
    }
}

role RawAsciiChar[Str \val_,  \wordsz_] does Token {
    has Str $.type = "'";
    has Str $.val = val_;
    has Int $.wordsz = Int.new(wordsz_);
    method tuple {
        [$.val,$.wordsz]
    }
    method Str {
        "'" ~ "{$.val}:{$.wordsz}"
    }
}


role RawAsciiWord[Str \val_,  \wordsz_] does Token {
    has Str $.type = '"';
    has Str $.val = val_;
    has Int $.wordsz = Int.new(wordsz_);
    method tuple {
        [$.val,$.wordsz]
    }
    method Str {
        '"' ~ "{$.val}:{$.wordsz}"
    }
}

role HexLiteral[Str \val_,  \wordsz_, \rs_] does Token {
    has Str $.type = '#';
    has Str $.val = val_;
    has Bool $.rs = rs_;
    has Int $.wordsz = Int.new(wordsz_);
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        "#{$.val}:{$.wordsz}: " ~($.rs ?? 'r' !! '');
    }
}

# This is anything that comes after a LIT and is not a hex
# Because LIT can be followed by up to 4 tokens, we need a list of tokens as argument.

role Literal[List \tokens_,Int \wordsz_,Bool \rs_] does Token {
    has Str $.type = 'LIT';
    has List $.tokens = tokens_;
    # has Str $.val = val_;
    has Bool $.rs = rs_;
    has Int $.wordsz = Int.new(wordsz_);
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        'LIT ' ~ join( ' ' , map -> \t {t.Str},|$.tokens) ~ ':{$.wordsz}';
    }
}

role AbsLabel[Str \val_] does Token {
    has Str $.type = '@';
    has Str $.val = val_;
    has Int $.wordsz = 2;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        '@' ~ "{$.val}:{$.wordsz}"
    }
}

role ChildLabel[Str \val_] does Token {
    has Str $.type = '&';
    has Str $.val = val_;
    has Int $.wordsz = 1;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        "$.type$.val:$.wordsz"
    }

}

role AbsPadding[Str \val_] does Token {
    has Str $.type = '|';
    has Str $.val = val_;
    has Int $.wordsz = 0;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        "$.type$.val"
    }
}

role RelPadding[Str \val_] does Token {
    has Str $.type = '$';
    has Str $.val = val_;
    has Int $.wordsz = Int.new(:16(val_));
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        $.type ~ $.val;
    }
}

role LitRelChildAddr [Str \val_] does Token {
    has Str $.type = ',&';
    has Str $.val = val_;
    has Int $.wordsz = 1;
    method tuple {
        [$.val,$.wordsz]
    }
    method Str {
        $.type ~ $.val ~ $.wordsz;
    }
}

# This is for absolute addresses that are referenced relatively.
role LitRelAddr [Str \val_] does Token {
    has Str $.type = ',';
    has Str $.val = val_;
    has Int $.wordsz = 1;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        $.type ~ $.val ~ $.wordsz;
    }
}

role LitAbsAddr [Str \val_, Int \wordsz_] does Token {
    has Str $.type = ';';
    has Str $.val = val_;
    has Int $.wordsz = wordsz_;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        ";$.val:$.wordsz"
    }
}

role LitAbsChildAddr [Str \val_, Int \wordsz_] does Token {
    has Str $.type = ';&';
    has Str $.val = val_;
    has Int $.wordsz = wordsz_;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        ";&$.val:$.wordsz"
    }
}

role LitZeroPageAddr [Str \val_] does Token {
    has Str $.type = '.';
    has Str $.val = val_;
    has Int $.wordsz = 1;
    method tuple {
        [$.val,$.wordsz]
    }
    method Str {
        ".$.val:$.wordsz"
    }
}

role RawAbsAddr [Str \val_] does Token {
    has Str $.type = ':';
    has Str $.val = val_;
    has Int $.wordsz = 2;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        $.type ~ $.val ~ $.wordsz;
    }
}

role Opcode [Str \name_, Int \wordsz_, Bool \keep_, Bool \rs_ ] does Token {
    has Str $.name = name_;
    has Int $.wordsz = wordsz_;
    has Bool $.keep = keep_;
    has Bool $.rs = rs_;
    method tuple( --> List) {
        [$.name,$.wordsz,$.keep,$.rs]
    }
    method Str {
        $.name
        ~ ($.wordsz==1 ?? '' !! '2')
        ~ ($.keep ?? 'k' !! '')
        ~ ($.rs ?? 'r' !! '')
    }
}

role Operation [ Int \nargs_, Int \nres_, Opcode \opc_] does Token {
    has Str $.type = 'op';
    has Int $.nargs = nargs_;
    has Int $.nres = nres_;
    has Opcode $.opcode = opc_;
    method tuple( --> List) {
        [$.nargs,$.nres,$.opcode.tuple]
    }
    method Str {
        $.opcode.Str
    }
}

# ======== ALl tokens below are for code generation ========

# Copy of LitAbsAddr for functions
role FunctionAddr [Str \val_, Int \wordsz_] does Token {
    has Str $.type = ';';
    has Str $.val = val_;
    has Int $.wordsz = wordsz_;
    has Int $.idx=0;
    method tuple {
        [$.val,$.wordsz,$.idx]
    }
    method Str {
        ";$.val:$.wordsz" ~'()'
    }
}

role NoRetVal does Token {
    method Str { 'NoRetVal' }
}
role NoToken does Token {
    method Str { 'NoToken' }
}
role Problem does Token {}

# Function call, the purpose is that we can replace ;f_ JMP2|JSR2 with f_()
role Call [ Token \fname_, Token \callop_, Bool \isJSR_] does Token {
    has Str $.type = 'call';
    has Token $.fname = fname_; # was LitAbsAddr but for dynamic calls it can be a register
    has Token $.callop = callop_; # maybe I don't need this
    has Bool $.isJSR = isJSR_;
    method Str {
        $.fname ~'()'
    }
}

# Conditional function call, the purpose is that we can replace ;f_ JCN2 with if (cond) { f_() }
role CondCall [ Token \cond_, Token \fname_, Token \callop_, ] does Token {
    has Str $.type = 'condcall';
    has Token $.fname = fname_; # was LitAbsAddr but for dynamic calls it can be a register
    has Token $.callop = callop_; # maybe I don't need this
    has Token $.cond = cond_;
    method Str {
        'if (' ~$.cond ~') { ' ~ $.fname ~ '() }';
    }
}

# This is for the case of a boolean op (a cmp op typically) followed by a JMP BRK
role CondReturn [ Token \cond_ ] does Token {
    has Str $.type = 'condreturn';
    has Token $.cond = cond_;
    method Str {
        'if (' ~$.cond ~') { return; }';
    }
}

# This is for the case of a boolean op (a cmp op typically) followed by a JMP and then an arbitrary operation
role CondJump [ Token \cond_, Token \op_ ] does Token {
    has Str $.type = 'condjump';
    has Token $.cond = cond_;
    has Token $.op = op_;
    method Str {
        'if (' ~ $.cond ~') { ' ~ $.op.Str ~ '; }';
    }
}


# This is for the case of a hex lit (or similar) followed by a JMP
role ConstJump [ Token \const_ ] does Token {
    has Str $.type = 'constjump';
    has Token $.const = const_;
    method Str {
        '/* a constant relative jump of size ' ~ $.const.Str ~ ' */';
    }
}

role EvalOpcode does Token {
    has Str $.type = 'eval';
    method Str {
        '#00 STR $1';
    }
}

role BytePair[Token \fst_,Token \snd_] does Token {
    has Str $.type = 'pair';
    has Token $.fst = fst_;
    has Token $.snd = snd_;
    method tuple( --> List) {
        [$.fst,$.snd]
    }
}

# ======== Other types ========
role BlockType {}
role FunctionBlock {}
role UnknownBlock does BlockType {}
role NonFunction does BlockType {}
role Data does BlockType {
    method Str {'Data'}
}
role DeviceMem does BlockType {
    method Str {'DeviceMem'}
}
role ZeroPage does BlockType {
    method Str {'ZeroPage'}
}
# Why is this not called Return?
role PlainFunction does BlockType does FunctionBlock {
    method Str {'Return'}
}
role Exit does BlockType does FunctionBlock {
    method Str {'Exit'}
}
role TailCall does BlockType does FunctionBlock {
    method Str {'TailCall'}
}
role EmptyBlock does BlockType  {
    method Str {'EmptyBlock'}
}
# I think these should now all be obsolete
role CondRecursion does BlockType does FunctionBlock {}
role UncondRecursion does BlockType does FunctionBlock { has Str $.escape-target; }
role CondRecursionTailCall does BlockType does FunctionBlock {}
# It might be easier to have a new one. If it is not a leaf function, inline recursively

role NodeType {}
role UnknownNode does NodeType {}
role LeafFunction does NodeType does FunctionBlock {
    method Str {'Leaf'}
}
role NonLeafFunction does NodeType does FunctionBlock {
        method Str {'NonLeaf'}
} # We could be more specific but do we need to?

role TokenisedProgram {
    has Hash $.blocks = {}; # TokenisedCodeBlock
    has Array $.blocks-sequence = []; # Str
    has Bool $.device-mem is rw = True;
    has Bool $.zero-page is rw = True;
    has Bool $.main is rw = False;
    has Bool $.in-block is rw = False;
    has Bool $.has-jump is rw = False;
    has Bool $.has-brk is rw = False;
    has Bool $.has-ops is rw = False;
    has Str $.block-label is rw = '0000';
    has Int $.token-addr is rw = 0;

    method print() {
        my TokenisedProgram \tokenised_program = self;
        for |tokenised_program.blocks-sequence -> \block {
            say "{tokenised_program.blocks{block}.block-type} {block}()";
                for |tokenised_program.blocks{block}.tokens -> \token_ {
                    say token_.Str;
                }
            say '';
        }
        exit
    }
}

role TokenisedCodeBlock {
    has Array $.tokens = []; # Token
    has Str $.block-label;
    has Hash $.labels = {}; # Str
    has BlockType $.block-type is rw = UnknownBlock.new;
    has Bool $.is-function is rw = False;
}

role ParsedProgram {
    has Map $.blocks = Map.new; # ParsedCodeBlock
    has List $.blocks-sequence = List.new; # Labels
    # Callers storers for every function, the list of functions that calls it, paired with list of function labels that are call args
    has Map $.callers = Map.new; # Map of Maps: store the args for every call

    method print() {
        my ParsedProgram \parsed_program = self;
        for |parsed_program.blocks-sequence -> \block {
            # say "{parsed_program.blocks{block}.function.just} {parsed_program.blocks{block}.block-type} ";
            say "{block}(";
                    if parsed_program.blocks{block}.function-args ~~ Just {
                say join( ', ', map ->\t { t.Str },|parsed_program.blocks{block}.function-args.just);
            }
            if parsed_program.blocks{block}.function-retvals ~~ Just {
                say join( ', ', map ->\t { t.Str },|parsed_program.blocks{block}.function-retvals.just);
            }
            say ')';
            # say 'labels: ',parsed_program.blocks{block}.labels;
            if parsed_program.blocks{block}.registerised-code.elems>0 {
                for |parsed_program.blocks{block}.registerised-code -> \token_ {
                    say token_.Str;
                }
            } else {
                for |parsed_program.blocks{block}.code -> \token_ {
                    say token_.Str;
                }
            }
            say '';
        }
        exit
    }

}

role ParsedCodeBlock {
    has List $.code = List.new;
    has Map $.labels = Map.new;

    has NodeType $.node-type = UnknownNode.new;
    has BlockType $.block-type = UnknownBlock.new ;

    has Maybe $.inlineable = Nothing.new; #False;
    has Maybe $.stack-based = Nothing.new; #True;

    has Maybe $.function = Nothing.new;
    has Maybe $.call-target = Nothing.new;
    has Maybe $.recursion = Nothing.new;
    has Maybe $.terminal = Nothing.new;

    has Maybe $.function-args = Nothing.new;
    has Maybe $.function-retvals = Nothing.new;

}

role ArgumentInferenceState[ParsedProgram \p, Str \l, Int \idx_, Array \st, Int \sct, Array \args_, Int \act] {
    has ParsedProgram $.program = p;
    has Str $.block-label = l;
    has Array $.stack = st;
    has Array $.args = args_;
    has Int $.idx = idx_;
    has Int $.stack-count = sct;
    has Int $.arg-count = act;
    has Bool $.done = False;
    method tuple {
        [$.program,$.block-label,$.idx,$.stack,$.stack-count,$.args,$.arg-count];
    }
}

role RegisteriserState[ParsedProgram \p,Array \ws,Array \rs,Map \lss,Int \rc,Int \grc,Str \cl,List \rbb] {
    has ParsedProgram $.program = p;
    has Array $.argsStack = ws;
    has Array $.returnStack = rs;
    has Map $.loopStacks = lss;
    has Int $.regCounter = rc;
    has Int $.globRegCounter= grc;
    has Str $.currentLabel = cl;
    has List $.regBasedBlock = rbb;

    method tuple {
        [$.program, $.argsStack, $.returnStack, $.loopStacks, $.regCounter, $.globRegCounter, $.currentLabel, $.regBasedBlock];
    }
}