use v6;
# The easiest is simply to generate this similar to the 
# uxn-ops-c-generator.raku but make sure the numbering is correct
my @lines = 
'void EVAL() {',
	'    unsigned char instr = GET1();',
	'    switch(instr) {'
	;
# # Return Mode */
# 		r = instr & 0x40 ? 1 : 0; # byte 7
# # Keep Mode */
# 		k = instr & 0x80 ? 1 : 0 # byte 8
# # Short Mode */
# 		bs = instr & 0x20 ? 1 : 0; # byte 6
		
# 		opc = instr & 0x1f; # bits 5 .. 1

 
 my @opcodes = <
 LIT 
 INC 
 POP 
 NIP 
 SWP 
 ROT 
 DUP 
 OVR 
 EQU 
 NEQ 
 GTH 
 LTH 
 JMP 
 JCN 
 JSR 
 STH 
 LDZ 
 STZ 
 LDR 
 STR 
 LDA 
 STA 
 DEI 
 DEO 
 ADD 
 SUB 
 MUL 
 DIV 
 AND 
 ORA 
 EOR 
 SFT 
>;

my $instr=0;		
for @opcodes -> \opcode {
    for 0, 1 -> \wordsz {
        for 0, 1 -> \rs {
            for 0, 1 -> \keep {
                my \opcode_val = $instr + (wordsz +< 5) + (rs +< 6) + (keep +< 7);
               push @lines, 
               _generate_op(
                opcode,
                (wordsz+1),
                (keep ?? 'k'!! ''),
                (rs ?? 'r' !! ''),
                opcode_val
                );
				
            }
        }
    }
    $instr++;
}		
push @lines, '    }';
push @lines, '}';

map {.say}, @lines ;

	
sub _generate_op(\opcode,\wordsz,\k,\r,\instr) {
	"        case {instr}: {opcode}{wordsz}{k}{r}(); break;"
}