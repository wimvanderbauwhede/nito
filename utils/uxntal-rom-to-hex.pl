#!/usr/bin/perl
use v5.30;
use warnings;
use strict;

use Data::Dumper;
my $BLOCK_SIZE=8;
@ARGV || die "$0 <.rom file>\n";
my $rom_file = $ARGV[0];

open my $ROM, '<:raw', $rom_file or die;
my $rom_sz = (stat $rom_file)[7];

my $rom_str='';
my $ct=0;
my $offset=0;
my $chunk;
while(my $n_bytes_read = read($ROM, $chunk,$rom_sz,$offset)){
    $offset+=$n_bytes_read;
    $rom_str.=$chunk;

	$ct++;
}
close $ROM;
my $sz= length($rom_str);
my @hexes =  unpack("H2" x $sz, $rom_str );

my @mnemonic = qw(
BRK
INC
POP 
NIP
SWP 
ROT
DUP 
OVR 
EQU
NEQ
GTH
LTH 
JMP
JCN
JSR
STH 
LDZ
STZ 
LDR
STR 
LDA
STA 
DEI
DEO
ADD 
SUB
MUL
DIV
AND
ORA
EOR
SFT
);



my $lit=0;my $lit2=0;
    for my $hex (@hexes) {
        # say $hex;
    if (!$lit and hex($hex) == 0x80)  {
        print "#"; $lit=1;next;
    } elsif (!$lit2 and hex($hex) == 0xa0) {
        print "#"; $lit2=2;next;
    } 
    if ($lit==1) {
        say $hex; $lit=0;
    } elsif ($lit2>0) {
        print $hex; $lit2--; 
        if ($lit2==0) {print "\n" }
    } else { 
        my @opcode =  @{opcode($hex)};
        my ($op,$sz,$k,$r) = @opcode;
        
        say $mnemonic[$op].$sz.$k.$r;
    }
}

sub opcode { my ($hexcode_str) = @_;
    my $hexcode=hex($hexcode_str);
    return 
    [    
    ($hexcode & 0x1F),
    ($hexcode >> 5) & 0x1 ? '2' : '', #2
    ($hexcode >> 7) & 0x1 ? 'k':'',
    ($hexcode >> 6) & 0x1 ? 'r' : '' #r
    ];
}

=pod
The main challenge is how to restore labels for functions and zpa vars.
I think this is in general impossible, because arbitrary calculations are possible on addresses.
But most of the time, we can assume:
- a short followed by a jump is a function (@;);
- a byte followed by an LDZ or STZ is a zpa (@.);
- a byte followed by a jump is a relative label (,&);
=cut