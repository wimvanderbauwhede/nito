#!/usr/bin/env raku
use v6;
use lib ( 'lib', '../lib' );
use UxntalTypes;
use UxntalDefs;

my $UXNEMU = True;

my %binops = 
    
    EQU => '==',
    NEQ => '!=',
    GTH => '>',
    LTH => '<',

    ADD => '+',
    SUB => '-',
    MUL => '*',
    DIV => '/',
    AND => '&',
    ORA => '|',
    EOR => '^'
;

my @opcodes = %non_stack_operations.keys.sort;

my $mem =  $UXNEMU
    ?? 'u->ram'
    !! 'mem';

my $zeropage =  $UXNEMU
    ?? 'u->ram'
    !! 'zeropage';

my $wsp =  $UXNEMU
    ?? 'u->wst.ptr'
    !! 'wsp';


if False {
# header decls
for @opcodes -> \opcode {
    for 1,2 -> \wordsz {
        for True,False -> \keep {
            for True,False -> \rs {
                _generate_op_decl(opcode,wordsz,keep,rs);
            }
        }
    }
}
}

say '
#include "uxn-runtime.h"
/* include "uxn-stack-ops.h" */
#include <stdio.h>
#include <stdlib.h>
';

for @opcodes -> \opcode {
    for 1,2 -> \wordsz {
        for True,False -> \keep {
            for True,False -> \rs {
                _generate_op(opcode,wordsz,keep,rs);
            }
        }
    }
}

sub _generate_op_decl(\opcode_,\wordsz,\keep,\rs) {
    my @c_strs;        
    my Str \r = rs ?? 'r' !! '';
    my Str \k = keep ?? 'k' !! '';
    #__attribute__((always_inline)) inline 
    say 'void ' ~ opcode_ ~  wordsz ~ (keep ?? 'k' !! '') ~ (rs ?? 'r' !! '') ~ '(void);';
}
sub _generate_op(\opcode_,\wordsz,\keep,\rs) {
    my @c_strs;        
    my Str \r = rs ?? 'r' !! '';
    my Str \k = keep ?? 'k' !! '';
    my Str \ctype = wordsz==1??'char' !!'short';
    #__attribute__((always_inline)) inline 
    push @c_strs, 'void ' ~ opcode_ ~  wordsz ~ (keep ?? 'k' !! '') ~ (rs ?? 'r' !! '') ~ '(void) {';
    push @c_strs, '#ifdef DBG';
    push @c_strs, 'printf("'~ opcode_ ~   wordsz ~ (keep ?? 'k' !! '') ~ (rs ?? 'r' !! '') ~ '\n");';
    push @c_strs, '#endif';         
    given opcode_ {
        when 'BRK' { 'exit(0);'; }
        when 'STR' {
            "/* {opcode_}{wordsz}{k}{r} is not supported, should have become STA */"
        }
        when 'LDR' { 
            "/* {opcode_}{wordsz}{k}{r} is not supported, should have become LDA */"
        }
        when 'JMP' { 
            if  wordsz == 2 and not keep and not rs {           
                push @c_strs, 'f_ptr fp = GETFP();';
                push @c_strs, 'fp();';  
            } else {
                "/* {opcode_}{wordsz}{k}{r} is not supported: only JMP2 */"
            }
        }
        when 'JSR' { 
            if  wordsz == 2 and not keep and not rs {           
                push @c_strs, 'f_ptr fp = GETFP();';
                push @c_strs, 'fp();';
            } else {
                 "/* {opcode_}{wordsz}{k}{r} is not supported: only JSR2 */"
            }
        }
        when 'JCN' { 
            if  wordsz == 2 and not keep and not rs {           
                push @c_strs, 'f_ptr fp = GETFP();';
                push @c_strs, 'unsigned char cmp = GET1();';
                push @c_strs, 'if (cmp) {fp(); return; }';
            } else {
                 "/* {opcode_}{wordsz}{k}{r} is not supported: only JCN2 */"
            }
        }
        when 'LDA' {   
            # a LDA             
            push @c_strs, 'unsigned short a = GET2'~r~'();';
            if keep {
                push @c_strs, 'PUSH2'~r~'(a);'
            }
            if  wordsz == 1 {                     
                push @c_strs, 'unsigned char v = ' ~ $mem ~ '[a];';                
                push @c_strs, 'PUSH1'~r~'(v);';
            } else {
                if not $UXNEMU {
                    push @c_strs, 'unsigned short v = *((short*)(&' ~ $mem ~ '[a]));';
                } else {
                    push @c_strs, 'unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];';
                }
                push @c_strs, 'PUSH2'~r~'(v);';
            }
        }
        when 'STA' {
            # v a STA
            push @c_strs,'unsigned short a = GET2'~r~'();';
            if  wordsz == 1 {
                push @c_strs,'unsigned char v = GET1'~r~'();';
                if keep {
                    push @c_strs,'PUSH1'~r~'(v);';
                    push @c_strs,'PUSH2'~r~'(a);';           
                }
                push @c_strs,'' ~ $mem ~ '[a] = v;';
            } else {
                push @c_strs,'unsigned short v = GET2'~r~'();';
                if keep {                    
                    push @c_strs,'PUSH2'~r~'(v);';
                    push @c_strs,'PUSH2'~r~'(a);';
                }       
                if not $UXNEMU {             
                    push @c_strs, '*((short*)(&' ~ $mem ~ '[a])) = v;';
                } else {
                    push @c_strs, 'u->ram[a] = v >> 8; u->ram[a + 1] = v;';
                }
            }            
            
        }
        when 'LDZ' {           
            # a LDZ     
            push @c_strs, 'unsigned char a = GET1'~r~'();';
            if keep {
                push @c_strs, 'PUSH1'~r~'(a);';
            }
            if  wordsz == 1 {                     
                
                push @c_strs, 'unsigned char v = ' ~ $zeropage ~ '[(unsigned short)a];';
                
                push @c_strs, 'PUSH1'~r~'(v);';
            } else {
                if not $UXNEMU {
                push @c_strs, 'unsigned short v = *((unsigned short*)(&' ~ $zeropage ~ '[(unsigned short)a]));';
                } else {
                    push @c_strs, 'unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];';
                }
                push @c_strs, 'PUSH2'~r~'(v);';
            }
        }
        when 'STZ' {
            # v a STZ
            push @c_strs, 'unsigned char a = GET1'~r~'();';
            if  wordsz == 1 {
                push @c_strs, 'unsigned char v = GET1'~r~'();';
                if keep {
                    push @c_strs, 'PUSH1'~r~'(v);';
                    push @c_strs, 'PUSH1'~r~'(a);';                    
                }
                push @c_strs, '' ~ $zeropage ~ '[(unsigned short)a] = v;';
            } else {
                push @c_strs, 'unsigned short v = GET2'~r~'();';
                if keep {                    
                    push @c_strs, 'PUSH2'~r~'(v);';
                    push @c_strs, 'PUSH1'~r~'(a);';
                }
                if not $UXNEMU {
                push @c_strs,'*((unsigned short*)(&' ~ $zeropage ~ '[(unsigned short)a])) = v;';
                } else {
                push @c_strs, 'u->ram[a] = v >> 8; u->ram[a + 1] = v;';    
                }
            }            
            
        }            
        when 'DEO' {
            # c p DEO
            
            if $UXNEMU {
                push @c_strs, 'unsigned char p = GET1'~r~'();';
                push @c_strs, 'Device* dev = &(u->dev[p >> 4]);';
                if wordsz==2 { 
                    push @c_strs, 'unsigned short c = GET2'~r~'();';
                    if keep {                    
                        push @c_strs,'PUSH2'~r~'(c);';
                        push @c_strs,'PUSH1'~r~'(p);';
                    }         
                    push @c_strs, 'dev->dat[p & 0xf] = (unsigned char)(c>>8); ';
                    push @c_strs, 'dev->deo(dev, p & 0x0f);';
                    push @c_strs, 'dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); ';
                    push @c_strs, 'dev->deo(dev, (p+1) & 0x0f);';
                } else { 
                    push @c_strs, 'unsigned char c = GET1'~r~'();';
                    if keep {
                        push @c_strs,'PUSH1'~r~'(c);';
                        push @c_strs,'PUSH1'~r~'(p);';                    
                    }
                    push @c_strs, 'dev->dat[p & 0xf] = c; ';
                    push @c_strs, 'dev->deo(dev, p & 0x0f);';
                } 
                push @c_strs, '#ifdef DBG';
                push @c_strs, 'printf("'~opcode_~ ': %d (%c) to port %d\n",c,c,p);';
                push @c_strs, '#endif';                                  
            } else {
                push @c_strs, (
                    keep 
                    ?? 'unsigned char p = GET1'~r~'();'
                    !! 'POP1'~r~'();'
                );
                if  wordsz == 1 {                    
                    push @c_strs,'unsigned char c = GET1'~r~'();';
                    if keep {
                        push @c_strs,'PUSH1'~r~'(c);';
                        push @c_strs,'PUSH1'~r~'(p);';                    
                    }
                    push @c_strs,'printf("%c",c);';
                } else {
                    push @c_strs, 'unsigned short c = GET2'~r~'();';
                
                    if keep {                    
                        push @c_strs,'PUSH2'~r~'(c);';
                        push @c_strs,'PUSH1'~r~'(p);';
                    }                    
                    push @c_strs,'printf("%d",c);';
                }
            }            
        }
        when 'DEI' {
            
            # p DEI
            if $UXNEMU {
                push @c_strs, 'unsigned char p = GET1'~r~'();';

                if keep {
                    push @c_strs,'PUSH1'~r~'(p);';                    
                }                

                push @c_strs, 'Device* dev = &(u->dev[p >> 4]); ';
                push @c_strs, 'unsigned ' ~
                (wordsz==1 ?? 'char' !! 'short')
                ~ ' c = dev->dei(dev, p & 0x0f); ';
                if wordsz==2 { 
                    push @c_strs, 'c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); ';
                }
                push @c_strs, 'PUSH'~wordsz~r~'(c);';
                push @c_strs, '#ifdef DBG';
                push @c_strs, 'printf("'~opcode_~ ': %d (%c) from port %d\n",c,c,p);';
                push @c_strs, '#endif';                                  

            } else {
                push @c_strs,
                (
                    keep
                ?? 'unsigned char p = GET1'~r~'();'
                !! 'POP1'~r~'();'
                );
                if keep {
                    push @c_strs,'PUSH1'~r~'(p);';                    
                }                
                push @c_strs, '/* WARNING: ONLY WORKS FOR char */';              
                push @c_strs, 'PUSH1'~r~'(getc(stdin));';
            }
        }  
        when 'INC' {
            # v INC
            if  wordsz == 1 {                    
                push @c_strs,'unsigned char v = GET1'~r~'();';
                if keep {
                    push @c_strs,'PUSH1'~r~'(v);';
                }
                push @c_strs, 'PUSH1'~r~'(v+1);';
            } else {
                push @c_strs,'unsigned short v = GET2'~r~'();';
                if keep {
                    push @c_strs,'PUSH2'~r~'(v);';
                }
                push @c_strs, 'PUSH2'~r~'(v+1);';
            }                       
        }
        when 'SFT' {
            # b c SFT
            if  wordsz == 1 {                    
                push @c_strs, 'unsigned char c = GET1'~r~'();';
                push @c_strs, 'unsigned char b = GET1'~r~'();';
                if keep {
                    push @c_strs,'PUSH1'~r~'(b);';
                    push @c_strs,'PUSH1'~r~'(c);';
                }                    
                push @c_strs, 'unsigned char c8l = (c & 0xF);';
                push @c_strs, 'unsigned char c8h =  ((c >> 4) & 0xF);';
                push @c_strs, 'PUSH1'~r~'((b>>c8l)<<c8h);';                    
            } else {
                push @c_strs, 'unsigned char c = GET1'~r~'();';
                push @c_strs, 'unsigned short b = GET2'~r~'();';
                if keep {
                    push @c_strs,'PUSH2'~r~'(b);';
                    push @c_strs,'PUSH1'~r~'(c);';
                }
                push @c_strs, 'unsigned char c8l = (c & 0xF);';
                push @c_strs, 'unsigned char c8h =  ((c >> 4) & 0xF);';
                push @c_strs, 'PUSH2'~r~'((b>>c8l)<<c8h);';                    
            }                 
        }            
        when %binops{opcode_}:exists { 
            # a b c BINOP => b `BINOP` c
            if  wordsz == 1 {                    
                push @c_strs, 'unsigned char c = GET1'~r~'();';
                push @c_strs, 'unsigned char b = GET1'~r~'();';
                if keep {
                    push @c_strs,'PUSH1'~r~'(b);';
                    push @c_strs,'PUSH1'~r~'(c);';
                }                    
            } else {
                push @c_strs, 'unsigned short c = GET2'~r~'();';
                push @c_strs, 'unsigned short b = GET2'~r~'();';
                if keep {
                    push @c_strs,'PUSH2'~r~'(b);';
                    push @c_strs,'PUSH2'~r~'(c);';
                }
            }
            if %cmp_operations{opcode_}:exists {
                # push @c_strs, 'unsigned char cmp = ('~ctype~')b' ~ %binops{opcode_} ~ '('~ctype~')c;';
                push @c_strs, 'unsigned char cmp = b' ~ %binops{opcode_} ~ 'c;';
                push @c_strs, 'PUSH1'~r~'( cmp );';
            } else {
                push @c_strs, 'PUSH' ~ wordsz ~ r ~ '( b' ~ %binops{opcode_} ~ 'c );';
            }                
                # push @c_strs, '#ifdef DBG';
                # push @c_strs, 'printf("'~opcode_~ ' wsp:%d %d '~%binops{opcode_}~ ' %d\n",' ~ $wsp ~ ',b,c);';
                # push @c_strs, '#endif';            
        }
        default {    
            die opcode_.raku;
        }        
    };
    push @c_strs,"}\n";
    say join("\n",@c_strs);
} # END of __emit_reg_operation

