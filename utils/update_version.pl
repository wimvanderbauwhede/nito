#!/usr/bin/env perl
use v5.10;
use strict;
use warnings;
my $NEW_VERSION =$ARGV[0];

my @source_files = glob("lib/*.rakumod");

    say("perl -pi -w -e 's/VERSION = \"\\d\\.\\d\\.\\d+\"/VERSION = \"".$NEW_VERSION."\"/;' nito.raku");
    system("perl -pi -w -e 's/VERSION = \"\\d\\.\\d\\.\\d+\"/VERSION = \"".$NEW_VERSION."\"/;' nito.raku");

for my $source_file (@source_files) {
    say("perl -pi -w -e 's/:ver\<\\d\\.\\d\\.\\d+/:ver< ".$NEW_VERSION."/;' $source_file");
    system("perl -pi -w -e 's/:ver\<\\d\\.\\d\\.\\d+/:ver<".$NEW_VERSION."/;' $source_file");
}
