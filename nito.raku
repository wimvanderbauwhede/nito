#!/usr/bin/env raku
use v6;
use lib ( 'lib', '../lib' );
use UxntalTypes;
use UxntalDefs;

use UxntalTokeniser;
use UxntalParser;

use UxntalAnalyser;
use UxntalInliner;

use UxntalEmitter;
use UxntalStackBasedCEmitter;

constant VERSION  = v1.1.0;
constant AUTHOR = 'codeberg:wimvanderbauwhede';

sub MAIN(
          Str $src_file = 'NONE',
          Str $gen_file = 'NONE',
          Str :D($_DBG) = 'None',
          Bool :T($_TAL) = False,
          Bool :C($_C) = False, # But if -T is not set, we assume -C
          Bool :P($DBG_PRINT) = False,
          Bool :R($_REG) = False,
          Int :O($_OPT) = 0 # 1=inline ops; 2+=stack to reg; 3+=inline subs
# The -O mechanism is not optimal, it might be better to have strings for the opts, and have some convenience grouping
# So e.g. -O=inline-ops,stack-to-reg,inline-subs,call-to-goto
# But for the moment, I  say that
# -O=1=inline-ops
# -O=2=inline-ops,stack-to-reg
# -O=3=inline-ops,stack-to-reg,call-to-goto
# -O=4=inline-ops,stack-to-reg,inline-subs
# -O=5=inline-ops,stack-to-reg,inline-subs,call-to-goto
) {
    my $DBG = False;
    my $DBG_LEXER_PRINT = False;
    my $DBG_ANALYSIS_PRINT = False;
    given $_DBG {
        if 'None' {
            $DBG = False;
            $UxntalTokeniser::DBG = False;
            $UxntalParser::DBG = False;
            $UxntalAnalyser::DBG = False;
            $UxntalStackBasedCEmitter::DBG = False;
        }
        if / M / {$DBG = True;}
        if / L / {$UxntalTokeniser::DBG = True; $DBG_LEXER_PRINT = True; }
        if / P / {$UxntalParser::DBG = True}
        if / A / {$UxntalAnalyser::DBG = True; $DBG_ANALYSIS_PRINT = True; }
        if / C / {
            $UxntalStackbasedCEmitter::DBG = True;
            }
    }

    if $_C and $_TAL {
        die 'Choose either -C (C) or -T (Tal)' ~ "\n";
    }
    if $_OPT>0 and $_TAL {
        say 'Opt (-O) is only used for the C emitter';
    }

    say '( ' if $DBG_ANALYSIS_PRINT and $_TAL;
    my (Str \tal_file,  Str \tal_prog_str) = read_tal_file($src_file);

    # Tokenise the program
    say 'Tokenise';
    my Seq \token_seq = tokenise_program(tal_prog_str);
    say 'Parse';
    # Parse program into Token types
    my List \parsed_program_tokens = parse_program_tokens(token_seq);

    # Split program into code units
    my TokenisedProgram \tokenised_program = split_program_into_blocks(parsed_program_tokens);
    if $DBG_LEXER_PRINT {
        tokenised_program.print;
    }

    my ParsedProgram \parsed_program_0 = tokenised_to_parsed_program(tokenised_program);
    # Analysis of subroutines, tailcalls, loops and constants
    say 'Analyse';
    my ParsedProgram \parsed_program_ = analyse_program(parsed_program_0, $_OPT);

    my ParsedProgram \parsed_program = $_OPT>3 ?? inline_subroutine_calls(parsed_program_) !! parsed_program_;

    if $_TAL {
        say 'Emit Uxntal code';
        my List \tal_program_token_strings = emit_tal_program(parsed_program,tal_file);
        say ' )' if $DBG_ANALYSIS_PRINT ;
        if $gen_file ne 'NONE' {
            my $fh = open $gen_file, :w;
            $fh.say( join( ' ', tal_program_token_strings) );
            $fh.close;
        } else {
            say join( ' ', tal_program_token_strings);
        }
    } else { # emit stack-based C
        say 'Emit C code';
        my \c_program_strings = emit_stack_based_C_program(parsed_program,tal_file,$_OPT);
        if $gen_file ne 'NONE' {
            my $fh = open $gen_file, :w;
            $fh.say( join( "\n", c_program_strings) );
            $fh.close;
        } else {
            say join( "\n", c_program_strings);
        }
    }

} # END of main()

# ================================================================================================
# Subroutines
# ================================================================================================
sub read_tal_file ( Str $src_file --> List) {

    $src_file eq 'NONE' and die "Usage: $*PROGRAM-NAME <.tal file>\n";
    my Str \tal_file = $src_file;

    my \input_file = IO::Path.new( tal_file ) ;
    my Str \tal_prog_str = input_file.IO.slurp;

    return (tal_file,tal_prog_str);
} # END of read_tal_file

