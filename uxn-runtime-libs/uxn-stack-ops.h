#ifndef _UXN_STACK_OPS_H_
#define _UXN_STACK_OPS_H_

typedef void (*f_ptr)(void) ;

void PUSHFP( void (*f_ptr)(void)  );
f_ptr GETFP( ) ;
unsigned short FPADDR( void (*f_ptr)(void) );

void PUSH1(unsigned char) ;
void PUSH2(unsigned short) ;
unsigned char GET1();
unsigned short GET2();

unsigned char PEEK1();
unsigned short PEEK2();
char SPEEK1();
short SPEEK2();

void POP1() ;
void POP2() ;
void POP1k() ; 
void POP2k() ; 
void NIP1() ;
void NIP2() ;
void NIP1k() ;
void NIP2k() ;
void ROT1() ;
void ROT2() ;
void ROT1k() ;
void ROT2k() ;
void OVR1() ;
void OVR2() ;
void OVR1k() ;
void OVR2k() ;
void DUP1( ) ;
void DUP2( ) ;
void DUP1k( ) ;
void DUP2k( ) ;
void SWP1();
void SWP2();
void SWP1k();
void SWP2k();

void PUSH1r(unsigned char) ;
void PUSH2r(unsigned short) ;
unsigned char GET1r();
unsigned short GET2r();

void POP1r() ;
void POP2r() ;
void NIP1r() ;
void NIP2r() ;
void ROT1r() ;
void ROT2r() ;
void OVR1r() ;
void OVR2r() ;
void DUP1r( ) ;
void DUP2r( ) ;
void SWP1r();
void SWP2r();
void POP1kr() ; 
void POP2kr() ; 
void NIP1kr() ;
void NIP2kr() ;
void ROT1kr() ;
void ROT2kr() ;
void OVR1kr() ;
void OVR2kr() ;
void DUP1kr( ) ;
void DUP2kr( ) ;
void SWP1kr();
void SWP2kr();

void STH1() ;        
void STH2() ;
void STH1k() ;        
void STH2k() ;
void STH1r() ;        
void STH2r() ;
void STH1kr() ;        
void STH2kr() ;

void LIT1();
void LIT1k();
void LIT1r();
void LIT1kr();
void LIT2();
void LIT2k();
void LIT2r();
void LIT2kr();

#endif