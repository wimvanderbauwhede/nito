#include "uxncli.h"	
#include "uxn-runtime.h"

#include <stdio.h>
#include <stdlib.h>

#define ARG_HANDLING_FIXME

/*
Copyright (c) 2021 Devine Lu Linvega

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

int init_ram(Uxn*) ;

void run_program (void) ;

static int
error(char *msg, const char *err)
{
	fprintf(stderr, "Error %s: %s\n", msg, err);
	return 0;
}

void
system_deo_special(Device *d, Uint8 port)
{
	(void)d;
	(void)port;
}

static void
console_deo(Device *d, Uint8 port)
{
	FILE *fd = port == 0x8 ? stdout : port == 0x9 ? stderr
												  : 0;
	if(fd) {
		fputc(d->dat[port], fd);
		fflush(fd);
	}
}

static Uint8
nil_dei(Device *d, Uint8 port)
{
	return d->dat[port];
}

static void
nil_deo(Device *d, Uint8 port)
{
	(void)d;
	(void)port;
}

static int
console_input(Uxn *u, char c)
{
	Device *d = &u->dev[1];
	d->dat[0x2] = c;
	call_interrupt_handler(GETVECTOR(d));
	return 1;
}

#ifdef ARG_HANDLING_FIXME
static void
run_loop(Uxn *u)
{
	Device *d = &u->dev[0];
	while(!d->dat[0xf]) {
		int c = fgetc(stdin);
		if ((c == 'S') || (c=='s')) {	
			printf("\n");
			system_inspect(u);
		} else if ((c == 'q') || (c=='Q')) {
			/*printf("Instruction count: %d\n",u->instr_cnt);*/
			exit(0);
		} else if (c != EOF) {
			console_input(u, (Uint8)c);
		}
	}	
}
#endif 

int
uxn_interrupt(void)
{
	return 1;
}

static int
start(Uxn *u)
{
	if(!uxn_boot(u, (Uint8 *)calloc(0x10000, sizeof(Uint8))))
		return error("Boot", "Failed");
	/* system   */ uxn_port(u, 0x0, system_dei, system_deo);
	/* console  */ uxn_port(u, 0x1, nil_dei, console_deo);
	/* empty    */ uxn_port(u, 0x2, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x3, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x4, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x5, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x6, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x7, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x8, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0x9, nil_dei, nil_deo);
	/* file0    */ uxn_port(u, 0xa, file_dei, file_deo);
	/* file1    */ uxn_port(u, 0xb, file_dei, file_deo);
	/* datetime */ uxn_port(u, 0xc, datetime_dei, nil_deo);
	/* empty    */ uxn_port(u, 0xd, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0xe, nil_dei, nil_deo);
	/* empty    */ uxn_port(u, 0xf, nil_dei, nil_deo);
	init_ram(u);

	return 1;
}

int
main(int argc, char **argv)
{
	/*Uxn u;*/
	int i;
	u=&uxn;
	if(argc < 1)
		return error("Usage", "uxncli args");
	if(!start(&uxn))
		return error("Start", "Failed");
	run_program();
#ifdef ARG_HANDLING_FIXME
	for(i = 1; i < argc; i++) {
		char *p = argv[i];
		while(*p) {
			console_input(&uxn, *p++);
		}
		console_input(&uxn, '\n');
	}
	run_loop(&uxn);
#endif
	return 0;
}
