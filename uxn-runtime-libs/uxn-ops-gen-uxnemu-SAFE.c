
#include "uxn-runtime.h"
/* include "uxn-stack-ops.h" */
#include <stdio.h>
#include <stdlib.h>

void ADD1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void ADD2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b+c );
#ifdef DBG
printf("ADD wsp:%d %d + %d\n",u->wst->ptrb,c);
#endif
}

void AND1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void AND2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b&c );
#ifdef DBG
printf("AND wsp:%d %d & %d\n",u->wst->ptrb,c);
#endif
}

void BRK1kr(void) {
}

void BRK1k(void) {
}

void BRK1r(void) {
}

void BRK1(void) {
}

void BRK2kr(void) {
}

void BRK2k(void) {
}

void BRK2r(void) {
}

void BRK2(void) {
}

void DEI1kr(void) {
unsigned char p = GET1r();
PUSH1r(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1r(c);
}

void DEI1k(void) {
unsigned char p = GET1();
PUSH1(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1(c);
}

void DEI1r(void) {
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1r(c);
}

void DEI1(void) {
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = 0; 
c = dev->dei(dev, p & 0x0f); 
PUSH1(c);
}

void DEI2kr(void) {
unsigned char p = GET1r();
PUSH1r(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2r(c);
}

void DEI2k(void) {
unsigned char p = GET1();
PUSH1(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2(c);
}

void DEI2r(void) {
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2r(c);
}

void DEI2(void) {
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = 0;
c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2(c);
}

void DEO1kr(void) {
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1r();
PUSH1r(c);
PUSH1r(p);
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
}

void DEO1k(void) {
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1();
PUSH1(c);
PUSH1(p);
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
}

void DEO1r(void) {
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1r();
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
}

void DEO1(void) {
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1();
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f); 
}

void DEO2kr(void) {
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned short c = GET2r();
PUSH2r(c);
PUSH1r(p);
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
}

void DEO2k(void) {
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned short c = GET2();
PUSH2(c);
PUSH1(p);
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
}

void DEO2r(void) {
unsigned char p = GET1r();
Device* dev = &u->dev[p >> 4];
unsigned short c = GET2r();
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
}

void DEO2(void) {
unsigned char p = GET1();
printf("DEO2(%d,%d)\n",p,p>>4);
Device* dev = &u->dev[p >> 4];
unsigned short c = GET2();
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
}

void DIV1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void DIV2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b/c );
#ifdef DBG
printf("DIV wsp:%d %d / %d\n",u->wst->ptrb,c);
#endif
}

void EOR1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EOR2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b^c );
#ifdef DBG
printf("EOR wsp:%d %d ^ %d\n",u->wst->ptrb,c);
#endif
}

void EQU1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b==c;
PUSH1r( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b==c;
PUSH1( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b==c;
PUSH1r( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b==c;
PUSH1( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b==c;
PUSH1r( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b==c;
PUSH1( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b==c;
PUSH1r( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void EQU2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b==c;
PUSH1( cmp );
#ifdef DBG
printf("EQU wsp:%d %d == %d\n",u->wst->ptrb,c);
#endif
}

void GTH1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b>c;
PUSH1r( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b>c;
PUSH1( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b>c;
PUSH1r( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b>c;
PUSH1( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b>c;
PUSH1r( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b>c;
PUSH1( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b>c;
PUSH1r( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void GTH2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b>c;
PUSH1( cmp );
#ifdef DBG
printf("GTH wsp:%d %d > %d\n",u->wst->ptrb,c);
#endif
}

void INC1kr(void) {
unsigned char v = GET1r();
PUSH1r(v);
PUSH1r(v+1);
}

void INC1k(void) {
unsigned char v = GET1();
PUSH1(v);
PUSH1(v+1);
}

void INC1r(void) {
unsigned char v = GET1r();
PUSH1r(v+1);
}

void INC1(void) {
unsigned char v = GET1();
PUSH1(v+1);
}

void INC2kr(void) {
unsigned short v = GET2r();
PUSH2r(v);
PUSH2r(v+1);
}

void INC2k(void) {
unsigned short v = GET2();
PUSH2(v);
PUSH2(v+1);
}

void INC2r(void) {
unsigned short v = GET2r();
PUSH2r(v+1);
}

void INC2(void) {
unsigned short v = GET2();
PUSH2(v+1);
}

void JCN1kr(void) {
}

void JCN1k(void) {
}

void JCN1r(void) {
}

void JCN1(void) {
}

void JCN2kr(void) {
}

void JCN2k(void) {
}

void JCN2r(void) {
}

void JCN2(void) {
f_ptr fp = GETFP();
unsigned char cmp = GET1();
if (cmp) {fp(); return; }
}

void JMP1kr(void) {
}

void JMP1k(void) {
}

void JMP1r(void) {
}

void JMP1(void) {
}

void JMP2kr(void) {
}

void JMP2k(void) {
}

void JMP2r(void) {
}

void JMP2(void) {
f_ptr fp = GETFP();
fp();
}

void JSR1kr(void) {
}

void JSR1k(void) {
}

void JSR1r(void) {
}

void JSR1(void) {
}

void JSR2kr(void) {
}

void JSR2k(void) {
}

void JSR2r(void) {
}

void JSR2(void) {
f_ptr fp = GETFP();
fp();
}

void LDA1kr(void) {
unsigned short a = GET2r();
PUSH2r(a);
unsigned char v = u->ram[a];
PUSH1r(v);
}

void LDA1k(void) {
unsigned short a = GET2();
PUSH2(a);
unsigned char v = u->ram[a];
PUSH1(v);
}

void LDA1r(void) {
unsigned short a = GET2r();
unsigned char v = u->ram[a];
PUSH1r(v);
}

void LDA1(void) {
unsigned short a = GET2();
unsigned char v = u->ram[a];
PUSH1(v);
}

void LDA2kr(void) {
unsigned short a = GET2r();
PUSH2r(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDA2k(void) {
unsigned short a = GET2();
PUSH2(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LDA2r(void) {
unsigned short a = GET2r();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDA2(void) {
unsigned short a = GET2();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LDR1kr(void) {
}

void LDR1k(void) {
}

void LDR1r(void) {
}

void LDR1(void) {
}

void LDR2kr(void) {
}

void LDR2k(void) {
}

void LDR2r(void) {
}

void LDR2(void) {
}

void LDZ1kr(void) {
unsigned char a = GET1r();
PUSH1r(a);
unsigned char v = u->ram[(unsigned short)a];
PUSH1r(v);
}

void LDZ1k(void) {
unsigned char a = GET1();
PUSH1(a);
unsigned char v = u->ram[(unsigned short)a];
PUSH1(v);
}

void LDZ1r(void) {
unsigned char a = GET1r();
unsigned char v = u->ram[(unsigned short)a];
PUSH1r(v);
}

void LDZ1(void) {
unsigned char a = GET1();
unsigned char v = u->ram[(unsigned short)a];
PUSH1(v);
}

void LDZ2kr(void) {
unsigned char a = GET1r();
PUSH1r(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDZ2k(void) {
unsigned char a = GET1();
PUSH1(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LDZ2r(void) {
unsigned char a = GET1r();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDZ2(void) {
unsigned char a = GET1();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LTH1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b<c;
PUSH1r( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b<c;
PUSH1( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b<c;
PUSH1r( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b<c;
PUSH1( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b<c;
PUSH1r( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b<c;
PUSH1( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b<c;
PUSH1r( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void LTH2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b<c;
PUSH1( cmp );
#ifdef DBG
printf("LTH wsp:%d %d < %d\n",u->wst->ptrb,c);
#endif
}

void MUL1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void MUL2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b*c );
#ifdef DBG
printf("MUL wsp:%d %d * %d\n",u->wst->ptrb,c);
#endif
}

void NEQ1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b!=c;
PUSH1r( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b!=c;
PUSH1( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b!=c;
PUSH1r( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b!=c;
PUSH1( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b!=c;
PUSH1r( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b!=c;
PUSH1( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b!=c;
PUSH1r( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void NEQ2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b!=c;
PUSH1( cmp );
#ifdef DBG
printf("NEQ wsp:%d %d != %d\n",u->wst->ptrb,c);
#endif
}

void ORA1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void ORA2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b|c );
#ifdef DBG
printf("ORA wsp:%d %d | %d\n",u->wst->ptrb,c);
#endif
}

void SFT1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH1r((b>>c8l)<<c8h);
}

void SFT1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH1((b>>c8l)<<c8h);
}

void SFT1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH1r((b>>c8l)<<c8h);
}

void SFT1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH1((b>>c8l)<<c8h);
}

void SFT2kr(void) {
unsigned char c = GET1r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH1r(c);
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH2r((b>>c8l)<<c8h);
}

void SFT2k(void) {
unsigned char c = GET1();
unsigned short b = GET2();
PUSH2(b);
PUSH1(c);
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH2((b>>c8l)<<c8h);
}

void SFT2r(void) {
unsigned char c = GET1r();
unsigned short b = GET2r();
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH2r((b>>c8l)<<c8h);
}

void SFT2(void) {
unsigned char c = GET1();
unsigned short b = GET2();
unsigned char c8l = (c & 0xFF);
unsigned char c8h =  ((c >> 8) & 0xFF);
PUSH2((b>>c8l)<<c8h);
}

void STA1kr(void) {
unsigned short a = GET2r();
unsigned char v = GET1r();
PUSH1r(v);
PUSH2r(a);
u->ram[a] = v;
}

void STA1k(void) {
unsigned short a = GET2();
unsigned char v = GET1();
PUSH1(v);
PUSH2(a);
u->ram[a] = v;
}

void STA1r(void) {
unsigned short a = GET2r();
unsigned char v = GET1r();
u->ram[a] = v;
}

void STA1(void) {
unsigned short a = GET2();
unsigned char v = GET1();
u->ram[a] = v;
}

void STA2kr(void) {
unsigned short a = GET2r();
unsigned short v = GET2r();
PUSH2r(v);
PUSH2r(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STA2k(void) {
unsigned short a = GET2();
unsigned short v = GET2();
PUSH2(v);
PUSH2(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STA2r(void) {
unsigned short a = GET2r();
unsigned short v = GET2r();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STA2(void) {
unsigned short a = GET2();
unsigned short v = GET2();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STR1kr(void) {
}

void STR1k(void) {
}

void STR1r(void) {
}

void STR1(void) {
}

void STR2kr(void) {
}

void STR2k(void) {
}

void STR2r(void) {
}

void STR2(void) {
}

void STZ1kr(void) {
unsigned char a = GET1r();
unsigned char v = GET1r();
PUSH1r(v);
PUSH1r(a);
u->ram[(unsigned short)a] = v;
}

void STZ1k(void) {
unsigned char a = GET1();
unsigned char v = GET1();
PUSH1(v);
PUSH1(a);
u->ram[(unsigned short)a] = v;
}

void STZ1r(void) {
unsigned char a = GET1r();
unsigned char v = GET1r();
u->ram[(unsigned short)a] = v;
}

void STZ1(void) {
unsigned char a = GET1();
unsigned char v = GET1();
u->ram[(unsigned short)a] = v;
}

void STZ2kr(void) {
unsigned char a = GET1r();
unsigned short v = GET2r();
PUSH2r(v);
PUSH1r(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STZ2k(void) {
unsigned char a = GET1();
unsigned short v = GET2();
PUSH2(v);
PUSH1(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STZ2r(void) {
unsigned char a = GET1r();
unsigned short v = GET2r();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STZ2(void) {
unsigned char a = GET1();
unsigned short v = GET2();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void SUB1kr(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB1k(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB1r(void) {
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB1(void) {
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB2kr(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB2k(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB2r(void) {
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

void SUB2(void) {
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b-c );
#ifdef DBG
printf("SUB wsp:%d %d - %d\n",u->wst->ptrb,c);
#endif
}

