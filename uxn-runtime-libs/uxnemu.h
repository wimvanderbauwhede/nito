#ifndef _UXNEMU_H_
#define _UXNEMU_H_



#include "uxn.h"

#pragma GCC diagnostic push
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#include <SDL.h>
#include "devices/system.h"
#include "devices/screen.h"
#include "devices/audio.h"
#include "devices/file.h"
#include "devices/controller.h"
#include "devices/mouse.h"
#include "devices/datetime.h"
#ifdef _WIN32
#include <processthreadsapi.h>
#endif
#pragma GCC diagnostic pop
#pragma clang diagnostic pop

/*
Copyright (c) 2021 Devine Lu Linvega

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define WIDTH 64 * 8
#define HEIGHT 40 * 8
#define PAD 4
#define TIMEOUT_MS 334
#define BENCH 0

static SDL_Window *gWindow;
static SDL_Texture *gTexture;
static SDL_Renderer *gRenderer;
static SDL_AudioDeviceID audio_id;
static SDL_Rect gRect;
static SDL_Thread *stdin_thread;

/* devices */

static Device *devscreen, *devmouse, *devctrl, *devaudio0;
static Uint8 zoom = 1;
static Uint32 stdin_event, audio0_event;
static Uint64 exec_deadline, deadline_interval, ms_interval;

static int
error(char *msg, const char *err);

#pragma mark - Generics

static void
audio_callback(void *u, Uint8 *stream, int len);

void
audio_finished_handler(int instance);

static int
stdin_handler(void *p);

static void
set_window_size(SDL_Window *window, int w, int h);

int
set_size(void);

static void
redraw(void);
static int
init(void);

#pragma mark - Devices

void
system_deo_special(Device *d, Uint8 port);
static void
console_deo(Device *d, Uint8 port);

static Uint8
audio_dei(Device *d, Uint8 port);

static void
audio_deo(Device *d, Uint8 port);

static Uint8
nil_dei(Device *d, Uint8 port);

static void
nil_deo(Device *d, Uint8 port);

/* Boot */

/* static int load(Uxn *u, char *rom); */

static int
start(Uxn *u, char *rom);

static void
set_zoom(Uint8 scale);
static void
capture_screen(void);

static void
restart(Uxn *u);

static Uint8
get_button(SDL_Event *event);

static Uint8
get_button_joystick(SDL_Event *event);

static Uint8
get_vector_joystick(SDL_Event *event);

static Uint8
get_key(SDL_Event *event);

static void
do_shortcut(Uxn *u, SDL_Event *event);

static int
console_input(Uxn *u, char c);

static int handle_events(Uxn *u);

/* static */
int run_loop    (Uxn *u); 

int
uxn_interrupt(void);

int init_uxn_sdl(Uxn*);
#endif