#include "uxn-runtime.h"
#include "uxn-stack-ops.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define STACK_SZ 511


void PUSH1(unsigned char v) {
#ifdef DBG_
    printf("PUSH1 wsp:%d\n",u->wst.ptr);
#endif      
    u->wst.dat[u->wst.ptr++]=v;
#ifdef DEFENSIVE
if (u->wst.ptr>STACK_SZ) {
printf("Working stack overflow\n");
exit(0);
}
#endif    
}

void PUSH2(unsigned short v) {
    unsigned char v1 = (unsigned char)(v & 0xFF);
    unsigned char v2 = (unsigned char)((v >> 8) & 0xFF);
#ifdef DBG__
    printf("PUSH2 wsp:%d\n",u->wst.ptr);
#endif    
    u->wst.dat[u->wst.ptr++] = v2;
#ifdef DBG___    
    printf("wsp:%d:%d\n",u->wst.ptr,u->wst.dat[u->wst.ptr-1]);
#endif    
    u->wst.dat[u->wst.ptr++] = v1;
#ifdef DBG_    
    printf("PUSH2 wsp:%d:%d\n",u->wst.ptr,u->wst.dat[u->wst.ptr-1]);
#endif  
#ifdef DEFENSIVE
if (u->wst.ptr>STACK_SZ) {
printf("Working stack overflow\n");
exit(0);
}
#endif    
}

void POP1() {

    u->wst.ptr-=1;
#ifdef DBG_
    printf("POP1 wsp:%d\n",u->wst.ptr);
#endif      

#ifdef DEFENSIVE
if ((short)(u->wst.ptr)<0) {
printf("Working stack underflow\n");
exit(0);
}
#endif
}

void POP2() {
    u->wst.ptr-=2;
#ifdef DBG_
    printf("POP2 wsp:%d\n",u->wst.ptr);
#endif      

#ifdef DEFENSIVE
if ((short)(u->wst.ptr)<0) {
printf("Working stack underflow\n");
exit(0);
}
#endif    
}

void POP1k() { }
void POP2k() { }

/* like pop but returns the value popped */
unsigned char GET1() {
#ifdef DEFENSIVE
if (u->wst.ptr==0) {
printf("Working stack underflow\n");
exit(0);
}
#endif
    unsigned char v = u->wst.dat[--u->wst.ptr];
#ifdef DBG_
printf("GET1 wsp:%d:%d\n",u->wst.ptr,v);
#endif
    return v;
}

unsigned short GET2() {
#ifdef DEFENSIVE
if (u->wst.ptr==1) {
printf("Working stack underflow\n");
exit(0);
}
#endif
    unsigned char v1 = u->wst.dat[--u->wst.ptr];
    unsigned char v2 = u->wst.dat[--u->wst.ptr];
    unsigned short v = (((unsigned short)v2) << 8) + (unsigned short)v1;
#ifdef DBG_
printf("GET2 wsp:%d:%d\n",u->wst.ptr,v);
#endif
    return v;
}

unsigned char PEEK1() {
    unsigned char v = u->wst.dat[u->wst.ptr-1];
    printf("wsp %d:%d\n",u->wst.ptr,v);
    return v;
}
unsigned short PEEK2() {
    unsigned char v1 = u->wst.dat[u->wst.ptr-1];
    unsigned char v2 = u->wst.dat[u->wst.ptr-2];
    unsigned short v = (((unsigned short)v2) << 8) + (unsigned short)v1;
    printf("wsp %d:%d\n",u->wst.ptr,v);
    return v;
}

char SPEEK1() {
    unsigned char v = u->wst.dat[u->wst.ptr-1];
    return (char)v;
}
short SPEEK2() {
    unsigned char v1 = u->wst.dat[u->wst.ptr-1];
    unsigned char v2 = u->wst.dat[u->wst.ptr-2];
    unsigned short v = (((unsigned short)v2) << 8) + (unsigned short)v1;
    return (short)v;
}

/* return stack versions */

void PUSH1r(unsigned char v) {
    u->rst.dat[u->rst.ptr++]=v;
#ifdef DBG_
    printf("PUSH1r rsp:%d\n",u->rst.ptr);
#endif      

#ifdef DEFENSIVE
if (u->rst.ptr>STACK_SZ) {
printf("Return stack overflow\n");
exit(0);
}
#endif    
}

void PUSH2r(unsigned short v) {
    unsigned char v1 = (unsigned char)(v & 0xFF);
    unsigned char v2 = (unsigned char)((v >> 8) & 0xFF);
    u->rst.dat[u->rst.ptr++] = v2;
    u->rst.dat[u->rst.ptr++] = v1;
#ifdef DBG_
    printf("PUSH2r rsp:%d\n",u->rst.ptr);
#endif      
#ifdef DEFENSIVE
if (u->rst.ptr>STACK_SZ) {
printf("Return stack overflow\n");
exit(0);
}
#endif      
}

void POP1r() {
    u->rst.ptr-=1;
#ifdef DBG_
    printf("POP1r rsp:%d\n",u->rst.ptr);
#endif      

#ifdef DEFENSIVE
if ((short)(u->rst.ptr)<0) {
printf("Return stack underflow\n");
exit(0);
}
#endif      
}

void POP2r() {
    u->rst.ptr-=2;
#ifdef DBG_
    printf("POP2r rsp:%d\n",u->rst.ptr);
#endif      
#ifdef DEFENSIVE
if ((short)(u->rst.ptr)<0) {
printf("Return stack underflow\n");
exit(0);
}
#endif      
}

void POP1kr() { }
void POP2kr() { }

unsigned char GET1r() {
    unsigned char v = u->rst.dat[--u->rst.ptr];
#ifdef DBG_
    printf("GET1r rsp:%d\n",u->rst.ptr);
#endif      

#ifdef DEFENSIVE
if ((short)(u->rst.ptr)<0) {
printf("Return stack underflow\n");
exit(0);
}
#endif      
    return v;
}

unsigned short GET2r() {
    unsigned char v1 = u->rst.dat[--u->rst.ptr];
    unsigned char v2 = u->rst.dat[--u->rst.ptr];
    unsigned short v = (((unsigned short)v2) << 8) + (unsigned short)v1;
#ifdef DBG_
    printf("GET2r rsp:%d\n",u->rst.ptr);
#endif      

#ifdef DEFENSIVE
if ((short)(u->rst.ptr)<0) {
printf("Return stack underflow\n");
exit(0);
}
#endif      
    return v;
}

/* Operations below are phrased in terms of the above */

void NIP1() {

        /* a b => b */
    if (u->wst.ptr >= 1 ) {
        unsigned char b = GET1();
        POP1();
        PUSH1( b );
#ifdef DBG_    
    printf("NIP1 wsp:%d:%d\n",u->wst.ptr,u->wst.dat[u->wst.ptr-1]);
#endif    

    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP");
        exit(0);
    }
}

void NIP2() {
        /* a b => b */
#ifdef DBG_    
    printf("NIP2 wsp:%d\n",u->wst.ptr);
#endif    
        
    if (u->wst.ptr >= 3 ) {
        unsigned short b = GET2();
        POP2();
        PUSH2( b );
#ifdef DBG_    
    printf("NIP2 wsp:%d:%d\n",u->wst.ptr,u->wst.dat[u->wst.ptr-1]);
#endif    

    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP2: u->wst.ptr=%d\n",u->wst.ptr);
        exit(0);
    }
}


void ROT1() {
#ifdef DBG_
    printf("ROT1\n");
#endif      

        /* a b c => b c a */
    if ( u->wst.ptr >= 2) {
        unsigned char c = GET1() ;
        unsigned char b = GET1() ;
        unsigned char a = GET1() ;
        PUSH1( b);
        PUSH1( c);
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void ROT2() {
#ifdef DBG_
    printf("ROT2\n");
#endif      
        /* a b c => b c a */
    if ( u->wst.ptr >= 5) {
        unsigned short c = GET2() ;
        unsigned short b = GET2() ;
        unsigned short a = GET2() ;
        PUSH2( b);
        PUSH2( c);
        PUSH2( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void ROT1k() {
#ifdef DBG_
    printf("ROT1k\n");
#endif      

    if ( u->wst.ptr >= 2) {
        unsigned char c = u->wst.dat[u->wst.ptr-1] ;
        unsigned char b = u->wst.dat[u->wst.ptr-2] ;
        unsigned char a = u->wst.dat[u->wst.ptr-3] ;
        PUSH1( b);
        PUSH1( c);
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }

}

void ROT2k() {
#ifdef DBG_
    printf("ROT2k\n");
#endif      

        /* a b c => b c a */
    if ( u->wst.ptr >= 5) {
        unsigned short c = GET2() ;
        unsigned short b = GET2() ;
        unsigned short a = GET2() ;
        PUSH2( a);
        PUSH2( b);
        PUSH2( c);
        PUSH2( b);
        PUSH2( c);
        PUSH2( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}


void OVR1() {
#ifdef DBG_
    printf("OVR1\n");
#endif      

    /* a b => a b a */
    if (u->wst.ptr >= 1) {
        unsigned char a = u->wst.dat[u->wst.ptr-2];
        PUSH1( a);        
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for OVR1");
        exit(0);
    }
}

void OVR2() {
#ifdef DBG_
    printf("OVR2\n");
#endif      

    /* a b => a b a */
    
    if (u->wst.ptr >= 3) {
        unsigned char a2 = u->wst.dat[u->wst.ptr-4];
        unsigned char a1 = u->wst.dat[u->wst.ptr-3];
#ifdef DBG_        
        unsigned char b2 = u->wst.dat[u->wst.ptr-2];
        unsigned char b1 = u->wst.dat[u->wst.ptr-1];
#endif        
        PUSH1( a2);
        PUSH1( a1);
#ifdef DBG_        
        printf("OVR2 wsp:%d:%d,%d,%d,%d\n",u->wst.ptr,a2,a1,b2,b1);
#endif        
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR2");
    exit(0);
    }
}

void OVR1k() {
#ifdef DBG_
    printf("OVR1k\n");
#endif      

    /* a b => a b a */
    if (u->wst.ptr >= 1) {
        unsigned char a = u->wst.dat[u->wst.ptr-2];
        unsigned char b = u->wst.dat[u->wst.ptr-1];
        PUSH1( a);
        PUSH1( b);
        PUSH1( a);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR1k");
    exit(0);
    }
}

void OVR2k() {
#ifdef DBG_
    printf("OVR2k\n");
#endif      

    /* a b => a b a */
    if (u->wst.ptr >= 3) {
        unsigned short b = GET2();
        unsigned short a = GET2();

        PUSH2( a);
        PUSH2( b);
        PUSH2( a);
        PUSH2( b);
        PUSH2( a);
        
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR2k");
    exit(0);
    }
}

void DUP1( ) {
#ifdef DBG_
    printf("DUP1 wsp:%d\n",u->wst.ptr);
#endif      
    /* a => a a */ 
    if (u->wst.ptr >= 1) {
        unsigned char a = u->wst.dat[u->wst.ptr-1];
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP1");
        exit(0);
    }
}

void DUP2( ) {
#ifdef DBG_
    printf("DUP2 wsp:%d\n",u->wst.ptr);
#endif      

    /* a => a a */ 
    /* a2 a1 => a2 a1 a2 a1 */
    if (u->wst.ptr >= 2) {
        unsigned char a1 = u->wst.dat[u->wst.ptr-1];
        unsigned char a2 = u->wst.dat[u->wst.ptr-2];        
        PUSH1( a2);
        PUSH1( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP2: %d\n",u->wst.ptr);
        exit(0);
    }
}

void DUP1k( ) {
#ifdef DBG_
    printf("DUP1k\n");
#endif      

    /* a => a a */ 
    if (u->wst.ptr >= 1) {
        unsigned char a = u->wst.dat[u->wst.ptr-1];
        PUSH1( a);
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP1k");
        exit(0);
    }
}

void DUP2k( ) {
#ifdef DBG_
    printf("DUP2k\n");
#endif      

    /* a => a a */ 
    /* a2 a1 => a2 a1 a2 a1 */
    if (u->wst.ptr >= 2) {
        unsigned char a1 = u->wst.dat[u->wst.ptr-1];
        unsigned char a2 = u->wst.dat[u->wst.ptr-2];        
        PUSH1( a2);
        PUSH1( a1);
        PUSH1( a2);
        PUSH1( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP2k");
        exit(0);
    }
}

void NIP1k() {
    DUP1();
}

void NIP2k() {
    DUP2();
}

void SWP1(){
#ifdef DBG_
    printf("SWP1\n");
#endif      

    /* a b => b a */
    if (u->wst.ptr>=1) {
        unsigned char b = GET1();
        unsigned char a = GET1();
        PUSH1(b);
        PUSH1(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2(){
#ifdef DBG_
    printf("SWP2\n");
#endif      

    /* a b => b a */
    if (u->wst.ptr>=3) {
        unsigned short b = GET2();
        unsigned short a = GET2();
        PUSH2(b);
        PUSH2(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP1k(){
#ifdef DBG_
    printf("SWP1k\n");
#endif      

    /* a b => b a */
    if (u->wst.ptr>=1) {
        unsigned char b = GET1();
        unsigned char a = GET1();
        PUSH1(a);
        PUSH1(b);
        PUSH1(b);
        PUSH1(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2k(){
#ifdef DBG_
    printf("SWP2k\n");
#endif      

    /* a b => b a */
    if (u->wst.ptr>=3) {
        unsigned short b = GET2();
        unsigned short a = GET2();
        PUSH2(a);
        PUSH2(b);
        PUSH2(b);
        PUSH2(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}



/* return stack versions */

void NIP1r() {
#ifdef DBG_
    printf("NIP1r\n");
#endif      

        /* a b => b */
    if (u->rst.ptr >= 1 ) {
        unsigned char b = GET1r();
        POP1r();
        PUSH1r( b );
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP");
        exit(0);
    }
}

void NIP2r() {
#ifdef DBG_
    printf("NIP2r\n");
#endif      
        /* a b => b */
    if (u->rst.ptr >= 3 ) {
        unsigned short b = GET2r();
        POP2r();
        PUSH2r( b );
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP2");
        exit(0);
    }
}

void ROT1r() {
#ifdef DBG_
    printf("ROT1r\n");
#endif      

        /* a b c => b c a */
    if ( u->rst.ptr >= 2) {
        unsigned char c = GET1r() ;
        unsigned char b = GET1r() ;
        unsigned char a = GET1r() ;
        PUSH1r( b);
        PUSH1r( c);
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void ROT2r() {
#ifdef DBG_
    printf("ROT2r\n");
#endif      
        /* a b c => b c a */
    if ( u->rst.ptr >= 5) {
        unsigned short c = GET2r() ;
        unsigned short b = GET2r() ;
        unsigned short a = GET2r() ;
        PUSH2r( b);
        PUSH2r( c);
        PUSH2r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void OVR1r() {
#ifdef DBG_
    printf("OVR1r\n");
#endif      

    /* a b => a b a */
    if (u->rst.ptr >= 1) {
        unsigned char a = u->rst.dat[u->rst.ptr-2];
        PUSH1r( a);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR1r");
    exit(0);
    }
}

void OVR2r() {
#ifdef DBG_
    printf("OVR2r\n");
#endif      
    /* a b => a b a   a1 a2 b1 b2 */
    if (u->rst.ptr > 3) { 
        unsigned short a2 = u->rst.dat[u->rst.ptr-4];
        unsigned short a1 = u->rst.dat[u->rst.ptr-3];
        PUSH1r( a2);
        PUSH1r( a1);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR2r");
    exit(0);
    }
}
void DUP1r( ) {
#ifdef DBG_
    printf("DUP1r\n");
#endif      

    /* a => a a */ 
    if (u->rst.ptr >= 1) {
        unsigned char a = u->rst.dat[u->rst.ptr-1];
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP1r");
        exit(0);
    }
}

void DUP2r( ) {
#ifdef DBG_
    printf("DUP2r\n");
#endif      

    /* a => a a */ 
    /* a2 a1 => a2 a1 a2 a1 */
    if (u->rst.ptr >= 2) {
        unsigned char a1 = u->rst.dat[u->rst.ptr-1];
        unsigned char a2 = u->rst.dat[u->rst.ptr-2];        
        PUSH1r( a2);
        PUSH1r( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP2r");
        exit(0);
    }
}

void SWP1r(){
#ifdef DBG_
    printf("SWP1r\n");
#endif      

    /* a b => b a */
    if (u->rst.ptr>=1) {
        unsigned char b = GET1r();
        unsigned char a = GET1r();
        PUSH1r(b);
        PUSH1r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2r(){
#ifdef DBG_
    printf("SWP2r\n");
#endif      
    /* a b => b a */
    if (u->rst.ptr>=3) {
        unsigned short b = GET2r();
        unsigned short a = GET2r();
        PUSH2r(b);
        PUSH2r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}



void ROT1kr() {
#ifdef DBG_
    printf("ROT1kr\n");
#endif      

    if ( u->rst.ptr >= 2) {
        unsigned char c = u->rst.dat[u->rst.ptr-1] ;
        unsigned char b = u->rst.dat[u->rst.ptr-2] ;
        unsigned char a = u->rst.dat[u->rst.ptr-3] ;
        PUSH1r( b);
        PUSH1r( c);
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }

}

void ROT2kr() {
#ifdef DBG_
    printf("ROT2kr\n");
#endif      
        /* a b c => b c a */
    if ( u->rst.ptr >= 5) {
        unsigned short c = GET2r() ;
        unsigned short b = GET2r() ;
        unsigned short a = GET2r() ;
        PUSH2r( a);
        PUSH2r( b);
        PUSH2r( c);
        PUSH2r( b);
        PUSH2r( c);
        PUSH2r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void OVR1kr() {
#ifdef DBG_
    printf("OVR1kr\n");
#endif      

    /* a b => a b a */
    if (u->rst.ptr >= 1) {
        unsigned char a = u->rst.dat[u->rst.ptr-2];
        unsigned char b = u->rst.dat[u->rst.ptr-1];
        PUSH1r( a);
        PUSH1r( b);
        PUSH1r( a);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR1kr");
    exit(0);
    }
}

void OVR2kr() {
#ifdef DBG_
    printf("OVR2kr\n");
#endif      

    /* a b => a b a */
    if (u->rst.ptr >= 3) {
        unsigned short b = GET2r();
        unsigned short a = GET2r();

        PUSH2r( a);
        PUSH2r( b);
        PUSH2r( a);
        PUSH2r( b);
        PUSH2r( a);
        
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR2kr");
    exit(0);
    }
}

void DUP1kr( ) {
#ifdef DBG_
    printf("DUP1kr\n");
#endif      
    /* a => a a */ 
    if (u->rst.ptr >= 1) {
        unsigned char a = u->rst.dat[u->rst.ptr-1];
        PUSH1r( a);
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP1kr");
        exit(0);
    }
}

void DUP2kr( ) {
#ifdef DBG_
    printf("DUP1kr\n");
#endif      

    /* a => a a */ 
    /* a2 a1 => a2 a1 a2 a1 */
    if (u->rst.ptr >= 2) {
        unsigned char a1 = u->rst.dat[u->rst.ptr-1];
        unsigned char a2 = u->rst.dat[u->rst.ptr-2];        
        PUSH1r( a2);
        PUSH1r( a1);
        PUSH1r( a2);
        PUSH1r( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP2kr");
        exit(0);
    }
}

void NIP1kr() {
    DUP1r();
}

void NIP2kr() {
    DUP2r();
}

void SWP1kr(){
#ifdef DBG_
    printf("SWP1kr\n");
#endif      

    /* a b => b a */
    if (u->rst.ptr>=1) {
        unsigned char b = GET1r();
        unsigned char a = GET1r();
        PUSH1r(a);
        PUSH1r(b);
        PUSH1r(b);
        PUSH1r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2kr(){
#ifdef DBG_
    printf("SWP2kr\n");
#endif      

    /* a b => b a */
    if (u->rst.ptr>=3) {
        unsigned short b = GET2r();
        unsigned short a = GET2r();
        PUSH2r(a);
        PUSH2r(b);
        PUSH2r(b);
        PUSH2r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}


/* stack movement */

void STH1() {      
#ifdef DBG_
    printf("STH1\n");
#endif      

    unsigned char v = GET1();
    PUSH1r(v);
}

void STH2() {
#ifdef DBG_
    printf("STH2\n");
#endif      
    unsigned short v =  GET2();
    PUSH2r(v);
}

void STH1k() {        
#ifdef DBG_
    printf("STH1k\n");
#endif      

    unsigned char v = u->wst.dat[u->wst.ptr-1];
    PUSH1r(v);
}

void STH2k() {
#ifdef DBG_
    printf("STH2k\n");
#endif      

    unsigned char v1 = u->wst.dat[u->wst.ptr-1];
    unsigned char v2 = u->wst.dat[u->wst.ptr-2];
    PUSH1r(v2);
    PUSH1r(v1);
}

void STH1r() {       
#ifdef DBG_
    printf("STH1r\n");
#endif      

    unsigned char v = GET1r();
    PUSH1(v);
}

void STH2r() {
#ifdef DBG_
    printf("STH2r\n");
#endif      

    unsigned short v =  GET2r();
    PUSH2(v);
}

void STH1kr() {  
#ifdef DBG_
    printf("STH1kr\n");
#endif      

    unsigned char v = u->rst.dat[u->rst.ptr-1];
    PUSH1(v);
}

void STH2kr() {
#ifdef DBG_
    printf("STH2kr\n");
#endif      

    unsigned char v1 = u->rst.dat[u->rst.ptr-1];
    unsigned char v2 = u->rst.dat[u->rst.ptr-2];
    PUSH1(v2);
    PUSH1(v1);
}
/* 
The problem here is that we push every vfp to a new f_idx 
What we need to do is first check if vfp exists in functions


*/
void PUSHFP( void (*fp)(void)  ) {
#ifdef DBG_
    printf("PUSHFP\n");
#endif      

    void* vfp = (void*)(*fp);

    int idx=0;
    for (idx=0;idx<f_idx;++idx) {
        if (functions[idx]==vfp) {
            PUSH2(idx);
            return;
        }
    }

    functions[f_idx]=vfp;
    PUSH2(f_idx);
    f_idx++;
    return;
}

f_ptr GETFP(  ) {
#ifdef DBG_
    printf("GETFP\n");
#endif      

    unsigned short f_idx_ = GET2();
#ifdef DBG_
    printf("GETFP: f_idx_ = %d\n",f_idx_);
#endif
    void* vfp = functions[f_idx_];
    f_ptr fp = (f_ptr)vfp;
/*
    uint64_t ifp=0;
    u->wst.ptr-=8;
    for (int i = 0;i<8;i++) {
        ifp+= ((uint64_t)u->wst.dat[u->wst.ptr+i])<<(8*i);
    }    
    printf("%#016lx\n",ifp);
    void* vfp = (void*)ifp;
    f_ptr fp = (f_ptr)vfp;
*/
    return fp;
}

/* To handle RawAbsAddr */
/* v2 ;keypad/ops ADD2 LDA2 JSR2 */



unsigned short FPADDR( void (*fp)(void)  ) {
#ifdef DBG_
    printf("FPADDR\n");
#endif      

    void* vfp = (void*)(*fp);

    unsigned short idx=0;
    for (idx=0;idx<f_idx;++idx) {
        if (functions[idx]==vfp) {
            return idx;
        }
    }
    return 0;
}

void LIT1(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT1k(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT1r(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT1kr(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT2(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT2k(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT2r(){ printf("Putting LIT on the stack is not supported\n"); }
void LIT2kr(){ printf("Putting LIT on the stack is not supported\n"); }
