#include "uxn-runtime.h"
#include "uxn-stack-ops.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void PUSH1(unsigned char v) {
    ws[wsp++]=v;
}

void PUSH2(unsigned short v) {
    unsigned char v1 = (unsigned char)(v & 0xFF);
    unsigned char v2 = (unsigned char)((v >> 8) & 0xFF);
#ifdef DBG
    printf("PUSH2 wsp:%d\n",wsp);
#endif    
    ws[wsp++] = v2;
#ifdef DBG_    
    printf("wsp:%d:%d\n",wsp,ws[wsp-1]);
#endif    
    ws[wsp++] = v1;
#ifdef DBG    
    printf("PUSH2 wsp:%d:%d\n",wsp,ws[wsp-1]);
#endif    
}

void POP1() {
    wsp-=1;
}

void POP2() {
    wsp-=2;
}

void POP1k() { }
void POP2k() { }

/* like pop but returns the value popped */
unsigned char GET1() {
    unsigned char v = ws[--wsp];
#ifdef DBG
printf("GET1 wsp:%d:%d\n",wsp,v);
#endif

    return v;
}

unsigned short GET2() {
    unsigned char v1 = ws[--wsp];
    unsigned char v2 = ws[--wsp];
    unsigned short v = (((unsigned short)v2) << 8) + (unsigned short)v1;
#ifdef DBG
printf("GET2 wsp:%d:%d\n",wsp,v);
#endif

    return v;
}

/* return stack versions */

void PUSH1r(unsigned char v) {
    rs[rsp++]=v;
}

void PUSH2r(unsigned short v) {
    unsigned char v1 = (unsigned char)(v & 0xFF);
    unsigned char v2 = (unsigned char)((v >> 8) & 0xFF);
    rs[rsp++] = v2;
    rs[rsp++] = v1;
}

void POP1r() {
    rsp--;
}

void POP2r() {
    rsp--;
    rsp--;
}

void POP1kr() { }
void POP2kr() { }

unsigned char GET1r() {
    unsigned char v = rs[--rsp];
    return v;
}

unsigned short GET2r() {
    unsigned char v1 = rs[--rsp];
    unsigned char v2 = rs[--rsp];
    unsigned short v = (((unsigned short)v2) << 8) + (unsigned short)v1;
    return v;
}

/* Operations below are phrased in terms of the above */

void NIP1() {
        // a b => b
    if (wsp >= 1 ) {
        unsigned char b = GET1();
        POP1();
        PUSH1( b );
#ifdef DBG    
    printf("NIP1 wsp:%d:%d\n",wsp,ws[wsp-1]);
#endif    

    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP");
        exit(0);
    }
}

void NIP2() {
        // a b => b
#ifdef DBG    
    printf("NIP2 wsp:%d\n",wsp);
#endif    
        
    if (wsp >= 3 ) {
        unsigned short b = GET2();
        POP2();
        PUSH2( b );
#ifdef DBG    
    printf("NIP2 wsp:%d:%d\n",wsp,ws[wsp-1]);
#endif    

    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP2: wsp=%d\n",wsp);
        exit(0);
    }
}


void ROT1() {
        // a b c => b c a
    if ( wsp >= 2) {
        unsigned char c = GET1() ;
        unsigned char b = GET1() ;
        unsigned char a = GET1() ;
        PUSH1( b);
        PUSH1( c);
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void ROT2() {
        // a b c => b c a
    if ( wsp >= 5) {
        unsigned short c = GET2() ;
        unsigned short b = GET2() ;
        unsigned short a = GET2() ;
        PUSH2( b);
        PUSH2( c);
        PUSH2( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void ROT1k() {
    if ( wsp >= 2) {
        unsigned char c = ws[wsp] ;
        unsigned char b = ws[wsp-1] ;
        unsigned char a = ws[wsp-2] ;
        PUSH1( b);
        PUSH1( c);
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }

}

void ROT2k() {
        // a b c => b c a
    if ( wsp >= 5) {
        unsigned short c = GET2() ;
        unsigned short b = GET2() ;
        unsigned short a = GET2() ;
        PUSH2( a);
        PUSH2( b);
        PUSH2( c);
        PUSH2( b);
        PUSH2( c);
        PUSH2( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}


void OVR1() {
    // a b => a b a
    if (wsp >= 1) {
        unsigned char a = ws[wsp-2];
        PUSH1( a);        
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
        exit(0);
    }
}

void OVR2() {
    // a b => a b a
    
    if (wsp >= 3) {
        unsigned char a2 = ws[wsp-4];
        unsigned char a1 = ws[wsp-3];
#ifdef DBG        
        unsigned char b2 = ws[wsp-2];
        unsigned char b1 = ws[wsp-1];
#endif        
        PUSH1( a2);
        PUSH1( a1);
#ifdef DBG        
        printf("OVR2 wsp:%d:%d,%d,%d,%d\n",wsp,a2,a1,b2,b1);
#endif        
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}

void OVR1k() {
    // a b => a b a
    if (wsp >= 1) {
        unsigned char a = ws[wsp-1];
        unsigned char b = ws[wsp];
        PUSH1( a);
        PUSH1( b);
        PUSH1( a);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}

void OVR2k() {
    // a b => a b a
    if (wsp >= 3) {
        unsigned short b = GET2();
        unsigned short a = GET2();

        PUSH2( a);
        PUSH2( b);
        PUSH2( a);
        PUSH2( b);
        PUSH2( a);
        
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}

void DUP1( ) {
    // a => a a 
    if (wsp >= 0) {
        unsigned char a = ws[wsp-1];
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void DUP2( ) {
    // a => a a 
    // a2 a1 => a2 a1 a2 a1
    if (wsp >= 1) {
        unsigned char a1 = ws[wsp-1];
        unsigned char a2 = ws[wsp-2];        
        PUSH1( a2);
        PUSH1( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void DUP1k( ) {
    // a => a a 
    if (wsp >= 0) {
        unsigned char a = ws[wsp-1];
        PUSH1( a);
        PUSH1( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void DUP2k( ) {
    // a => a a 
    // a2 a1 => a2 a1 a2 a1
    if (wsp >= 1) {
        unsigned char a1 = ws[wsp-1];
        unsigned char a2 = ws[wsp-2];        
        PUSH1( a2);
        PUSH1( a1);
        PUSH1( a2);
        PUSH1( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void NIP1k() {
    DUP1();
}

void NIP2k() {
    DUP2();
}

void SWP1(){
    // a b => b a
    if (wsp>=1) {
        unsigned char b = GET1();
        unsigned char a = GET1();
        PUSH1(b);
        PUSH1(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2(){
    // a b => b a
    if (wsp>=3) {
        unsigned short b = GET2();
        unsigned short a = GET2();
        PUSH2(b);
        PUSH2(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP1k(){
    // a b => b a
    if (wsp>=1) {
        unsigned char b = GET1();
        unsigned char a = GET1();
        PUSH1(a);
        PUSH1(b);
        PUSH1(b);
        PUSH1(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2k(){
    // a b => b a
    if (wsp>=3) {
        unsigned short b = GET2();
        unsigned short a = GET2();
        PUSH2(a);
        PUSH2(b);
        PUSH2(b);
        PUSH2(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}



/* return stack versions */

void NIP1r() {
        // a b => b
    if (rsp >= 1 ) {
        unsigned char b = GET1r();
        POP1r();
        PUSH1r( b );
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP");
        exit(0);
    }
}

void NIP2r() {
        // a b => b
    if (rsp >= 3 ) {
        unsigned short b = GET2r();
        POP2r();
        PUSH2r( b );
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for NIP2");
        exit(0);
    }
}

void ROT1r() {
        // a b c => b c a
    if ( rsp >= 2) {
        unsigned char c = GET1r() ;
        unsigned char b = GET1r() ;
        unsigned char a = GET1r() ;
        PUSH1r( b);
        PUSH1r( c);
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void ROT2r() {
        // a b c => b c a
    if ( rsp >= 5) {
        unsigned short c = GET2r() ;
        unsigned short b = GET2r() ;
        unsigned short a = GET2r() ;
        PUSH2r( b);
        PUSH2r( c);
        PUSH2r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void OVR1r() {
    // a b => a b a
    if (rsp >= 1) {
        unsigned char a = rs[rsp-1];
        PUSH1r( a);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}

void OVR2r() {
    // a b => a b a
    if (rsp >= 3) {
        unsigned short a2 = rs[rsp-3];
        unsigned short a1 = rs[rsp-2];
        PUSH1r( a2);
        PUSH1r( a1);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}
void DUP1r( ) {
    // a => a a 
    if (rsp >= 0) {
        unsigned char a = rs[rsp-1];
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void DUP2r( ) {
    // a => a a 
    // a2 a1 => a2 a1 a2 a1
    if (rsp >= 1) {
        unsigned char a1 = rs[rsp-1];
        unsigned char a2 = rs[rsp-2];        
        PUSH1r( a2);
        PUSH1r( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void SWP1r(){
    // a b => b a
    if (rsp>=1) {
        unsigned char b = GET1r();
        unsigned char a = GET1r();
        PUSH1r(b);
        PUSH1r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2r(){
    // a b => b a
    if (rsp>=3) {
        unsigned short b = GET2r();
        unsigned short a = GET2r();
        PUSH2r(b);
        PUSH2r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}



void ROT1kr() {
    if ( rsp >= 2) {
        unsigned char c = rs[rsp] ;
        unsigned char b = rs[rsp-1] ;
        unsigned char a = rs[rsp-2] ;
        PUSH1r( b);
        PUSH1r( c);
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }

}

void ROT2kr() {
        // a b c => b c a
    if ( rsp >= 5) {
        unsigned short c = GET2r() ;
        unsigned short b = GET2r() ;
        unsigned short a = GET2r() ;
        PUSH2r( a);
        PUSH2r( b);
        PUSH2r( c);
        PUSH2r( b);
        PUSH2r( c);
        PUSH2r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for ROT");
        exit(0);
    }
}

void OVR1kr() {
    // a b => a b a
    if (rsp >= 1) {
        unsigned char a = rs[rsp-1];
        unsigned char b = rs[rsp];
        PUSH1r( a);
        PUSH1r( b);
        PUSH1r( a);
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}

void OVR2kr() {
    // a b => a b a
    if (rsp >= 3) {
        unsigned short b = GET2r();
        unsigned short a = GET2r();

        PUSH2r( a);
        PUSH2r( b);
        PUSH2r( a);
        PUSH2r( b);
        PUSH2r( a);
        
    } else {
    printf( "NOT ENOUGH ITEMS ON THE STACK for OVR");
    exit(0);
    }
}

void DUP1kr( ) {
    // a => a a 
    if (rsp >= 0) {
        unsigned char a = rs[rsp-1];
        PUSH1r( a);
        PUSH1r( a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void DUP2kr( ) {
    // a => a a 
    // a2 a1 => a2 a1 a2 a1
    if (rsp >= 1) {
        unsigned char a1 = rs[rsp-1];
        unsigned char a2 = rs[rsp-2];        
        PUSH1r( a2);
        PUSH1r( a1);
        PUSH1r( a2);
        PUSH1r( a1);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for DUP");
        exit(0);
    }
}

void NIP1kr() {
    DUP1r();
}

void NIP2kr() {
    DUP2r();
}

void SWP1kr(){
    // a b => b a
    if (rsp>=1) {
        unsigned char b = GET1r();
        unsigned char a = GET1r();
        PUSH1r(a);
        PUSH1r(b);
        PUSH1r(b);
        PUSH1r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}

void SWP2kr(){
    // a b => b a
    if (rsp>=3) {
        unsigned short b = GET2r();
        unsigned short a = GET2r();
        PUSH2r(a);
        PUSH2r(b);
        PUSH2r(b);
        PUSH2r(a);
    } else {
        printf( "NOT ENOUGH ITEMS ON THE STACK for SWP");
        exit(0);
    }
}


/* stack movement */

void STH1() {        
    unsigned char v = GET1();
    PUSH1r(v);
}

void STH2() {
    unsigned short v =  GET2();
    PUSH2r(v);
}

void STH1k() {        
    unsigned char v = ws[wsp];
    PUSH1r(v);
}

void STH2k() {
    unsigned char v1 = ws[wsp];
    unsigned char v2 = ws[wsp-1];
    PUSH1r(v2);
    PUSH1r(v1);
}

void STH1r() {        
    unsigned char v = GET1r();
    PUSH1(v);
}

void STH2r() {
    unsigned short v =  GET2r();
    PUSH2(v);
}

void STH1kr() {        
    unsigned char v = rs[rsp];
    PUSH1(v);
}

void STH2kr() {
    unsigned char v1 = rs[rsp];
    unsigned char v2 = rs[rsp-1];
    PUSH1(v2);
    PUSH1(v1);
}


void PUSHFP( void (*fp)(void)  ) {
    void* vfp = (void*)(*fp);
    functions[f_idx]=vfp;
    PUSH2(f_idx);
    f_idx++;
    return;
}

f_ptr GETFP(  ) {
    unsigned short f_idx = GET2();
    void* vfp = functions[f_idx];
    f_ptr fp = (f_ptr)vfp;
/*
    uint64_t ifp=0;
    wsp-=8;
    for (int i = 0;i<8;i++) {
        ifp+= ((uint64_t)ws[wsp+i])<<(8*i);
    }    
    printf("%#016lx\n",ifp);
    void* vfp = (void*)ifp;
    f_ptr fp = (f_ptr)vfp;
*/
    return fp;
}