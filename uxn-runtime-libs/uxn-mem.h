#ifndef _UXN_MEM_
#define _UXN_MEM_

#ifdef UXNEMU
#include "uxn.h"
Uxn uxn;
Uxn* u;
#else
unsigned char zeropage[256];
unsigned char mem[64*1024-256];
unsigned char ws[255];
unsigned int wsp;
unsigned char rs[255];
unsigned int rsp;
#endif
void* functions[256];
unsigned short f_idx;
#endif 
