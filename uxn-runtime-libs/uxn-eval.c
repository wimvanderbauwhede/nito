#include "uxn-stack-ops.h"
#include "uxn-ops-gen.h"
void EVAL() {
    unsigned char instr = GET1();
    switch(instr) {        
        case 0: LIT1(); break;
        case 128: LIT1k(); break;
        case 64: LIT1r(); break;
        case 192: LIT1kr(); break;
        case 32: LIT2(); break;
        case 160: LIT2k(); break;
        case 96: LIT2r(); break;
        case 224: LIT2kr(); break;
        case 1: INC1(); break;
        case 129: INC1k(); break;
        case 65: INC1r(); break;
        case 193: INC1kr(); break;
        case 33: INC2(); break;
        case 161: INC2k(); break;
        case 97: INC2r(); break;
        case 225: INC2kr(); break;
        case 2: POP1(); break;
        case 130: POP1k(); break;
        case 66: POP1r(); break;
        case 194: POP1kr(); break;
        case 34: POP2(); break;
        case 162: POP2k(); break;
        case 98: POP2r(); break;
        case 226: POP2kr(); break;
        case 3: NIP1(); break;
        case 131: NIP1k(); break;
        case 67: NIP1r(); break;
        case 195: NIP1kr(); break;
        case 35: NIP2(); break;
        case 163: NIP2k(); break;
        case 99: NIP2r(); break;
        case 227: NIP2kr(); break;
        case 4: SWP1(); break;
        case 132: SWP1k(); break;
        case 68: SWP1r(); break;
        case 196: SWP1kr(); break;
        case 36: SWP2(); break;
        case 164: SWP2k(); break;
        case 100: SWP2r(); break;
        case 228: SWP2kr(); break;
        case 5: ROT1(); break;
        case 133: ROT1k(); break;
        case 69: ROT1r(); break;
        case 197: ROT1kr(); break;
        case 37: ROT2(); break;
        case 165: ROT2k(); break;
        case 101: ROT2r(); break;
        case 229: ROT2kr(); break;
        case 6: DUP1(); break;
        case 134: DUP1k(); break;
        case 70: DUP1r(); break;
        case 198: DUP1kr(); break;
        case 38: DUP2(); break;
        case 166: DUP2k(); break;
        case 102: DUP2r(); break;
        case 230: DUP2kr(); break;
        case 7: OVR1(); break;
        case 135: OVR1k(); break;
        case 71: OVR1r(); break;
        case 199: OVR1kr(); break;
        case 39: OVR2(); break;
        case 167: OVR2k(); break;
        case 103: OVR2r(); break;
        case 231: OVR2kr(); break;
        case 8: EQU1(); break;
        case 136: EQU1k(); break;
        case 72: EQU1r(); break;
        case 200: EQU1kr(); break;
        case 40: EQU2(); break;
        case 168: EQU2k(); break;
        case 104: EQU2r(); break;
        case 232: EQU2kr(); break;
        case 9: NEQ1(); break;
        case 137: NEQ1k(); break;
        case 73: NEQ1r(); break;
        case 201: NEQ1kr(); break;
        case 41: NEQ2(); break;
        case 169: NEQ2k(); break;
        case 105: NEQ2r(); break;
        case 233: NEQ2kr(); break;
        case 10: GTH1(); break;
        case 138: GTH1k(); break;
        case 74: GTH1r(); break;
        case 202: GTH1kr(); break;
        case 42: GTH2(); break;
        case 170: GTH2k(); break;
        case 106: GTH2r(); break;
        case 234: GTH2kr(); break;
        case 11: LTH1(); break;
        case 139: LTH1k(); break;
        case 75: LTH1r(); break;
        case 203: LTH1kr(); break;
        case 43: LTH2(); break;
        case 171: LTH2k(); break;
        case 107: LTH2r(); break;
        case 235: LTH2kr(); break;
        case 12: JMP1(); break;
        case 140: JMP1k(); break;
        case 76: JMP1r(); break;
        case 204: JMP1kr(); break;
        case 44: JMP2(); break;
        case 172: JMP2k(); break;
        case 108: JMP2r(); break;
        case 236: JMP2kr(); break;
        case 13: JCN1(); break;
        case 141: JCN1k(); break;
        case 77: JCN1r(); break;
        case 205: JCN1kr(); break;
        case 45: JCN2(); break;
        case 173: JCN2k(); break;
        case 109: JCN2r(); break;
        case 237: JCN2kr(); break;
        case 14: JSR1(); break;
        case 142: JSR1k(); break;
        case 78: JSR1r(); break;
        case 206: JSR1kr(); break;
        case 46: JSR2(); break;
        case 174: JSR2k(); break;
        case 110: JSR2r(); break;
        case 238: JSR2kr(); break;
        case 15: STH1(); break;
        case 143: STH1k(); break;
        case 79: STH1r(); break;
        case 207: STH1kr(); break;
        case 47: STH2(); break;
        case 175: STH2k(); break;
        case 111: STH2r(); break;
        case 239: STH2kr(); break;
        case 16: LDZ1(); break;
        case 144: LDZ1k(); break;
        case 80: LDZ1r(); break;
        case 208: LDZ1kr(); break;
        case 48: LDZ2(); break;
        case 176: LDZ2k(); break;
        case 112: LDZ2r(); break;
        case 240: LDZ2kr(); break;
        case 17: STZ1(); break;
        case 145: STZ1k(); break;
        case 81: STZ1r(); break;
        case 209: STZ1kr(); break;
        case 49: STZ2(); break;
        case 177: STZ2k(); break;
        case 113: STZ2r(); break;
        case 241: STZ2kr(); break;
        case 18: LDR1(); break;
        case 146: LDR1k(); break;
        case 82: LDR1r(); break;
        case 210: LDR1kr(); break;
        case 50: LDR2(); break;
        case 178: LDR2k(); break;
        case 114: LDR2r(); break;
        case 242: LDR2kr(); break;
        case 19: STR1(); break;
        case 147: STR1k(); break;
        case 83: STR1r(); break;
        case 211: STR1kr(); break;
        case 51: STR2(); break;
        case 179: STR2k(); break;
        case 115: STR2r(); break;
        case 243: STR2kr(); break;
        case 20: LDA1(); break;
        case 148: LDA1k(); break;
        case 84: LDA1r(); break;
        case 212: LDA1kr(); break;
        case 52: LDA2(); break;
        case 180: LDA2k(); break;
        case 116: LDA2r(); break;
        case 244: LDA2kr(); break;
        case 21: STA1(); break;
        case 149: STA1k(); break;
        case 85: STA1r(); break;
        case 213: STA1kr(); break;
        case 53: STA2(); break;
        case 181: STA2k(); break;
        case 117: STA2r(); break;
        case 245: STA2kr(); break;
        case 22: DEI1(); break;
        case 150: DEI1k(); break;
        case 86: DEI1r(); break;
        case 214: DEI1kr(); break;
        case 54: DEI2(); break;
        case 182: DEI2k(); break;
        case 118: DEI2r(); break;
        case 246: DEI2kr(); break;
        case 23: DEO1(); break;
        case 151: DEO1k(); break;
        case 87: DEO1r(); break;
        case 215: DEO1kr(); break;
        case 55: DEO2(); break;
        case 183: DEO2k(); break;
        case 119: DEO2r(); break;
        case 247: DEO2kr(); break;
        case 24: ADD1(); break;
        case 152: ADD1k(); break;
        case 88: ADD1r(); break;
        case 216: ADD1kr(); break;
        case 56: ADD2(); break;
        case 184: ADD2k(); break;
        case 120: ADD2r(); break;
        case 248: ADD2kr(); break;
        case 25: SUB1(); break;
        case 153: SUB1k(); break;
        case 89: SUB1r(); break;
        case 217: SUB1kr(); break;
        case 57: SUB2(); break;
        case 185: SUB2k(); break;
        case 121: SUB2r(); break;
        case 249: SUB2kr(); break;
        case 26: MUL1(); break;
        case 154: MUL1k(); break;
        case 90: MUL1r(); break;
        case 218: MUL1kr(); break;
        case 58: MUL2(); break;
        case 186: MUL2k(); break;
        case 122: MUL2r(); break;
        case 250: MUL2kr(); break;
        case 27: DIV1(); break;
        case 155: DIV1k(); break;
        case 91: DIV1r(); break;
        case 219: DIV1kr(); break;
        case 59: DIV2(); break;
        case 187: DIV2k(); break;
        case 123: DIV2r(); break;
        case 251: DIV2kr(); break;
        case 28: AND1(); break;
        case 156: AND1k(); break;
        case 92: AND1r(); break;
        case 220: AND1kr(); break;
        case 60: AND2(); break;
        case 188: AND2k(); break;
        case 124: AND2r(); break;
        case 252: AND2kr(); break;
        case 29: ORA1(); break;
        case 157: ORA1k(); break;
        case 93: ORA1r(); break;
        case 221: ORA1kr(); break;
        case 61: ORA2(); break;
        case 189: ORA2k(); break;
        case 125: ORA2r(); break;
        case 253: ORA2kr(); break;
        case 30: EOR1(); break;
        case 158: EOR1k(); break;
        case 94: EOR1r(); break;
        case 222: EOR1kr(); break;
        case 62: EOR2(); break;
        case 190: EOR2k(); break;
        case 126: EOR2r(); break;
        case 254: EOR2kr(); break;
        case 31: SFT1(); break;
        case 159: SFT1k(); break;
        case 95: SFT1r(); break;
        case 223: SFT1kr(); break;
        case 63: SFT2(); break;
        case 191: SFT2k(); break;
        case 127: SFT2r(); break;
        case 255: SFT2kr(); break;
    }
}
