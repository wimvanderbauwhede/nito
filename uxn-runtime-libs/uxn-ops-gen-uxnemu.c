
#include "uxn-runtime.h"
/* include "uxn-stack-ops.h" */
#include <stdio.h>
#include <stdlib.h>

void ADD1kr(void) {
#ifdef DBG
printf("ADD1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b+c );
}

void ADD1k(void) {
#ifdef DBG
printf("ADD1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b+c );
}

void ADD1r(void) {
#ifdef DBG
printf("ADD1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b+c );
}

void ADD1(void) {
#ifdef DBG
printf("ADD1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b+c );
}

void ADD2kr(void) {
#ifdef DBG
printf("ADD2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b+c );
}

void ADD2k(void) {
#ifdef DBG
printf("ADD2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b+c );
}

void ADD2r(void) {
#ifdef DBG
printf("ADD2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b+c );
}

void ADD2(void) {
#ifdef DBG
printf("ADD2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b+c );
}

void AND1kr(void) {
#ifdef DBG
printf("AND1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b&c );
}

void AND1k(void) {
#ifdef DBG
printf("AND1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b&c );
}

void AND1r(void) {
#ifdef DBG
printf("AND1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b&c );
}

void AND1(void) {
#ifdef DBG
printf("AND1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b&c );
}

void AND2kr(void) {
#ifdef DBG
printf("AND2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b&c );
}

void AND2k(void) {
#ifdef DBG
printf("AND2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b&c );
}

void AND2r(void) {
#ifdef DBG
printf("AND2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b&c );
}

void AND2(void) {
#ifdef DBG
printf("AND2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b&c );
}

void BRK1kr(void) {
#ifdef DBG
printf("BRK1kr\n");
#endif
}

void BRK1k(void) {
#ifdef DBG
printf("BRK1k\n");
#endif
}

void BRK1r(void) {
#ifdef DBG
printf("BRK1r\n");
#endif
}

void BRK1(void) {
#ifdef DBG
printf("BRK1\n");
#endif
}

void BRK2kr(void) {
#ifdef DBG
printf("BRK2kr\n");
#endif
}

void BRK2k(void) {
#ifdef DBG
printf("BRK2k\n");
#endif
}

void BRK2r(void) {
#ifdef DBG
printf("BRK2r\n");
#endif
}

void BRK2(void) {
#ifdef DBG
printf("BRK2\n");
#endif
}

void DEI1kr(void) {
#ifdef DBG
printf("DEI1kr\n");
#endif
unsigned char p = GET1r();
PUSH1r(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1r(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI1k(void) {
#ifdef DBG
printf("DEI1k\n");
#endif
unsigned char p = GET1();
PUSH1(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI1r(void) {
#ifdef DBG
printf("DEI1r\n");
#endif
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1r(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI1(void) {
#ifdef DBG
printf("DEI1\n");
#endif
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]); 
unsigned char c = dev->dei(dev, p & 0x0f); 
PUSH1(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI2kr(void) {
#ifdef DBG
printf("DEI2kr\n");
#endif
unsigned char p = GET1r();
PUSH1r(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2r(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI2k(void) {
#ifdef DBG
printf("DEI2k\n");
#endif
unsigned char p = GET1();
PUSH1(p);
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI2r(void) {
#ifdef DBG
printf("DEI2r\n");
#endif
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2r(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEI2(void) {
#ifdef DBG
printf("DEI2\n");
#endif
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]); 
unsigned short c = dev->dei(dev, p & 0x0f); 
c = (c << 8) + dev->dei(dev, (p + 1) & 0x0f); 
PUSH2(c);
#ifdef DBG
printf("DEI: %d (%c) from port %d\n",c,c,p);
#endif
}

void DEO1kr(void) {
#ifdef DBG
printf("DEO1kr\n");
#endif
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1r();
PUSH1r(c);
PUSH1r(p);
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO1k(void) {
#ifdef DBG
printf("DEO1k\n");
#endif
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1();
PUSH1(c);
PUSH1(p);
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO1r(void) {
#ifdef DBG
printf("DEO1r\n");
#endif
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1r();
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO1(void) {
#ifdef DBG
printf("DEO1\n");
#endif
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned char c = GET1();
dev->dat[p & 0xf] = c; 
dev->deo(dev, p & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO2kr(void) {
#ifdef DBG
printf("DEO2kr\n");
#endif
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned short c = GET2r();
PUSH2r(c);
PUSH1r(p);
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO2k(void) {
#ifdef DBG
printf("DEO2k\n");
#endif
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned short c = GET2();
PUSH2(c);
PUSH1(p);
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO2r(void) {
#ifdef DBG
printf("DEO2r\n");
#endif
unsigned char p = GET1r();
Device* dev = &(u->dev[p >> 4]);
unsigned short c = GET2r();
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DEO2(void) {
#ifdef DBG
printf("DEO2\n");
#endif
unsigned char p = GET1();
Device* dev = &(u->dev[p >> 4]);
unsigned short c = GET2();
dev->dat[p & 0xf] = (unsigned char)(c>>8); 
dev->deo(dev, p & 0x0f);
dev->dat[(p+1) & 0xf] = (unsigned char)(c & 0xFF); 
dev->deo(dev, (p+1) & 0x0f);
#ifdef DBG
printf("DEO: %d (%c) to port %d\n",c,c,p);
#endif
}

void DIV1kr(void) {
#ifdef DBG
printf("DIV1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b/c );
}

void DIV1k(void) {
#ifdef DBG
printf("DIV1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b/c );
}

void DIV1r(void) {
#ifdef DBG
printf("DIV1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b/c );
}

void DIV1(void) {
#ifdef DBG
printf("DIV1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b/c );
}

void DIV2kr(void) {
#ifdef DBG
printf("DIV2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b/c );
}

void DIV2k(void) {
#ifdef DBG
printf("DIV2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b/c );
}

void DIV2r(void) {
#ifdef DBG
printf("DIV2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b/c );
}

void DIV2(void) {
#ifdef DBG
printf("DIV2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b/c );
}

void EOR1kr(void) {
#ifdef DBG
printf("EOR1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b^c );
}

void EOR1k(void) {
#ifdef DBG
printf("EOR1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b^c );
}

void EOR1r(void) {
#ifdef DBG
printf("EOR1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b^c );
}

void EOR1(void) {
#ifdef DBG
printf("EOR1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b^c );
}

void EOR2kr(void) {
#ifdef DBG
printf("EOR2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b^c );
}

void EOR2k(void) {
#ifdef DBG
printf("EOR2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b^c );
}

void EOR2r(void) {
#ifdef DBG
printf("EOR2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b^c );
}

void EOR2(void) {
#ifdef DBG
printf("EOR2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b^c );
}

void EQU1kr(void) {
#ifdef DBG
printf("EQU1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b==c;
PUSH1r( cmp );
}

void EQU1k(void) {
#ifdef DBG
printf("EQU1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b==c;
PUSH1( cmp );
}

void EQU1r(void) {
#ifdef DBG
printf("EQU1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b==c;
PUSH1r( cmp );
}

void EQU1(void) {
#ifdef DBG
printf("EQU1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b==c;
PUSH1( cmp );
}

void EQU2kr(void) {
#ifdef DBG
printf("EQU2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b==c;
PUSH1r( cmp );
}

void EQU2k(void) {
#ifdef DBG
printf("EQU2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b==c;
PUSH1( cmp );
}

void EQU2r(void) {
#ifdef DBG
printf("EQU2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b==c;
PUSH1r( cmp );
}

void EQU2(void) {
#ifdef DBG
printf("EQU2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b==c;
PUSH1( cmp );
}

void GTH1kr(void) {
#ifdef DBG
printf("GTH1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b>c;
PUSH1r( cmp );
}

void GTH1k(void) {
#ifdef DBG
printf("GTH1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b>c;
PUSH1( cmp );
}

void GTH1r(void) {
#ifdef DBG
printf("GTH1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b>c;
PUSH1r( cmp );
}

void GTH1(void) {
#ifdef DBG
printf("GTH1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b>c;
PUSH1( cmp );
}

void GTH2kr(void) {
#ifdef DBG
printf("GTH2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b>c;
PUSH1r( cmp );
}

void GTH2k(void) {
#ifdef DBG
printf("GTH2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b>c;
PUSH1( cmp );
}

void GTH2r(void) {
#ifdef DBG
printf("GTH2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b>c;
PUSH1r( cmp );
}

void GTH2(void) {
#ifdef DBG
printf("GTH2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b>c;
PUSH1( cmp );
}

void INC1kr(void) {
#ifdef DBG
printf("INC1kr\n");
#endif
unsigned char v = GET1r();
PUSH1r(v);
PUSH1r(v+1);
}

void INC1k(void) {
#ifdef DBG
printf("INC1k\n");
#endif
unsigned char v = GET1();
PUSH1(v);
PUSH1(v+1);
}

void INC1r(void) {
#ifdef DBG
printf("INC1r\n");
#endif
unsigned char v = GET1r();
PUSH1r(v+1);
}

void INC1(void) {
#ifdef DBG
printf("INC1\n");
#endif
unsigned char v = GET1();
PUSH1(v+1);
}

void INC2kr(void) {
#ifdef DBG
printf("INC2kr\n");
#endif
unsigned short v = GET2r();
PUSH2r(v);
PUSH2r(v+1);
}

void INC2k(void) {
#ifdef DBG
printf("INC2k\n");
#endif
unsigned short v = GET2();
PUSH2(v);
PUSH2(v+1);
}

void INC2r(void) {
#ifdef DBG
printf("INC2r\n");
#endif
unsigned short v = GET2r();
PUSH2r(v+1);
}

void INC2(void) {
#ifdef DBG
printf("INC2\n");
#endif
unsigned short v = GET2();
PUSH2(v+1);
}

void JCN1kr(void) {
#ifdef DBG
printf("JCN1kr\n");
#endif
}

void JCN1k(void) {
#ifdef DBG
printf("JCN1k\n");
#endif
}

void JCN1r(void) {
#ifdef DBG
printf("JCN1r\n");
#endif
}

void JCN1(void) {
#ifdef DBG
printf("JCN1\n");
#endif
}

void JCN2kr(void) {
#ifdef DBG
printf("JCN2kr\n");
#endif
}

void JCN2k(void) {
#ifdef DBG
printf("JCN2k\n");
#endif
}

void JCN2r(void) {
#ifdef DBG
printf("JCN2r\n");
#endif
}

void JCN2(void) {
#ifdef DBG
printf("JCN2\n");
#endif
f_ptr fp = GETFP();
unsigned char cmp = GET1();
if (cmp) {fp(); return; }
}

void JMP1kr(void) {
#ifdef DBG
printf("JMP1kr\n");
#endif
}

void JMP1k(void) {
#ifdef DBG
printf("JMP1k\n");
#endif
}

void JMP1r(void) {
#ifdef DBG
printf("JMP1r\n");
#endif
}

void JMP1(void) {
#ifdef DBG
printf("JMP1\n");
#endif
}

void JMP2kr(void) {
#ifdef DBG
printf("JMP2kr\n");
#endif
}

void JMP2k(void) {
#ifdef DBG
printf("JMP2k\n");
#endif
}

void JMP2r(void) {
#ifdef DBG
printf("JMP2r\n");
#endif
}

void JMP2(void) {
#ifdef DBG
printf("JMP2\n");
#endif
f_ptr fp = GETFP();
fp();
}

void JSR1kr(void) {
#ifdef DBG
printf("JSR1kr\n");
#endif
}

void JSR1k(void) {
#ifdef DBG
printf("JSR1k\n");
#endif
}

void JSR1r(void) {
#ifdef DBG
printf("JSR1r\n");
#endif
}

void JSR1(void) {
#ifdef DBG
printf("JSR1\n");
#endif
}

void JSR2kr(void) {
#ifdef DBG
printf("JSR2kr\n");
#endif
}

void JSR2k(void) {
#ifdef DBG
printf("JSR2k\n");
#endif
}

void JSR2r(void) {
#ifdef DBG
printf("JSR2r\n");
#endif
}

void JSR2(void) {
#ifdef DBG
printf("JSR2\n");
#endif
f_ptr fp = GETFP();
fp();
}

void LDA1kr(void) {
#ifdef DBG
printf("LDA1kr\n");
#endif
unsigned short a = GET2r();
PUSH2r(a);
unsigned char v = u->ram[a];
PUSH1r(v);
}

void LDA1k(void) {
#ifdef DBG
printf("LDA1k\n");
#endif
unsigned short a = GET2();
PUSH2(a);
unsigned char v = u->ram[a];
PUSH1(v);
}

void LDA1r(void) {
#ifdef DBG
printf("LDA1r\n");
#endif
unsigned short a = GET2r();
unsigned char v = u->ram[a];
PUSH1r(v);
}

void LDA1(void) {
#ifdef DBG
printf("LDA1\n");
#endif
unsigned short a = GET2();
unsigned char v = u->ram[a];
PUSH1(v);
}

void LDA2kr(void) {
#ifdef DBG
printf("LDA2kr\n");
#endif
unsigned short a = GET2r();
PUSH2r(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDA2k(void) {
#ifdef DBG
printf("LDA2k\n");
#endif
unsigned short a = GET2();
PUSH2(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LDA2r(void) {
#ifdef DBG
printf("LDA2r\n");
#endif
unsigned short a = GET2r();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDA2(void) {
#ifdef DBG
printf("LDA2\n");
#endif
unsigned short a = GET2();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LDR1kr(void) {
#ifdef DBG
printf("LDR1kr\n");
#endif
}

void LDR1k(void) {
#ifdef DBG
printf("LDR1k\n");
#endif
}

void LDR1r(void) {
#ifdef DBG
printf("LDR1r\n");
#endif
}

void LDR1(void) {
#ifdef DBG
printf("LDR1\n");
#endif
}

void LDR2kr(void) {
#ifdef DBG
printf("LDR2kr\n");
#endif
}

void LDR2k(void) {
#ifdef DBG
printf("LDR2k\n");
#endif
}

void LDR2r(void) {
#ifdef DBG
printf("LDR2r\n");
#endif
}

void LDR2(void) {
#ifdef DBG
printf("LDR2\n");
#endif
}

void LDZ1kr(void) {
#ifdef DBG
printf("LDZ1kr\n");
#endif
unsigned char a = GET1r();
PUSH1r(a);
unsigned char v = u->ram[(unsigned short)a];
PUSH1r(v);
}

void LDZ1k(void) {
#ifdef DBG
printf("LDZ1k\n");
#endif
unsigned char a = GET1();
PUSH1(a);
unsigned char v = u->ram[(unsigned short)a];
PUSH1(v);
}

void LDZ1r(void) {
#ifdef DBG
printf("LDZ1r\n");
#endif
unsigned char a = GET1r();
unsigned char v = u->ram[(unsigned short)a];
PUSH1r(v);
}

void LDZ1(void) {
#ifdef DBG
printf("LDZ1\n");
#endif
unsigned char a = GET1();
unsigned char v = u->ram[(unsigned short)a];
PUSH1(v);
}

void LDZ2kr(void) {
#ifdef DBG
printf("LDZ2kr\n");
#endif
unsigned char a = GET1r();
PUSH1r(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDZ2k(void) {
#ifdef DBG
printf("LDZ2k\n");
#endif
unsigned char a = GET1();
PUSH1(a);
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LDZ2r(void) {
#ifdef DBG
printf("LDZ2r\n");
#endif
unsigned char a = GET1r();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2r(v);
}

void LDZ2(void) {
#ifdef DBG
printf("LDZ2\n");
#endif
unsigned char a = GET1();
unsigned short v = (u->ram[a] << 8) + u->ram[a + 1];
PUSH2(v);
}

void LTH1kr(void) {
#ifdef DBG
printf("LTH1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b<c;
PUSH1r( cmp );
}

void LTH1k(void) {
#ifdef DBG
printf("LTH1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b<c;
PUSH1( cmp );
}

void LTH1r(void) {
#ifdef DBG
printf("LTH1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b<c;
PUSH1r( cmp );
}

void LTH1(void) {
#ifdef DBG
printf("LTH1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b<c;
PUSH1( cmp );
}

void LTH2kr(void) {
#ifdef DBG
printf("LTH2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b<c;
PUSH1r( cmp );
}

void LTH2k(void) {
#ifdef DBG
printf("LTH2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b<c;
PUSH1( cmp );
}

void LTH2r(void) {
#ifdef DBG
printf("LTH2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b<c;
PUSH1r( cmp );
}

void LTH2(void) {
#ifdef DBG
printf("LTH2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b<c;
PUSH1( cmp );
}

void MUL1kr(void) {
#ifdef DBG
printf("MUL1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b*c );
}

void MUL1k(void) {
#ifdef DBG
printf("MUL1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b*c );
}

void MUL1r(void) {
#ifdef DBG
printf("MUL1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b*c );
}

void MUL1(void) {
#ifdef DBG
printf("MUL1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b*c );
}

void MUL2kr(void) {
#ifdef DBG
printf("MUL2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b*c );
}

void MUL2k(void) {
#ifdef DBG
printf("MUL2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b*c );
}

void MUL2r(void) {
#ifdef DBG
printf("MUL2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b*c );
}

void MUL2(void) {
#ifdef DBG
printf("MUL2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b*c );
}

void NEQ1kr(void) {
#ifdef DBG
printf("NEQ1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char cmp = b!=c;
PUSH1r( cmp );
}

void NEQ1k(void) {
#ifdef DBG
printf("NEQ1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char cmp = b!=c;
PUSH1( cmp );
}

void NEQ1r(void) {
#ifdef DBG
printf("NEQ1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char cmp = b!=c;
PUSH1r( cmp );
}

void NEQ1(void) {
#ifdef DBG
printf("NEQ1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char cmp = b!=c;
PUSH1( cmp );
}

void NEQ2kr(void) {
#ifdef DBG
printf("NEQ2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
unsigned char cmp = b!=c;
PUSH1r( cmp );
}

void NEQ2k(void) {
#ifdef DBG
printf("NEQ2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
unsigned char cmp = b!=c;
PUSH1( cmp );
}

void NEQ2r(void) {
#ifdef DBG
printf("NEQ2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
unsigned char cmp = b!=c;
PUSH1r( cmp );
}

void NEQ2(void) {
#ifdef DBG
printf("NEQ2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
unsigned char cmp = b!=c;
PUSH1( cmp );
}

void ORA1kr(void) {
#ifdef DBG
printf("ORA1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b|c );
}

void ORA1k(void) {
#ifdef DBG
printf("ORA1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b|c );
}

void ORA1r(void) {
#ifdef DBG
printf("ORA1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b|c );
}

void ORA1(void) {
#ifdef DBG
printf("ORA1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b|c );
}

void ORA2kr(void) {
#ifdef DBG
printf("ORA2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b|c );
}

void ORA2k(void) {
#ifdef DBG
printf("ORA2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b|c );
}

void ORA2r(void) {
#ifdef DBG
printf("ORA2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b|c );
}

void ORA2(void) {
#ifdef DBG
printf("ORA2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b|c );
}

void SFT1kr(void) {
#ifdef DBG
printf("SFT1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH1r((b>>c8l)<<c8h);
}

void SFT1k(void) {
#ifdef DBG
printf("SFT1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH1((b>>c8l)<<c8h);
}

void SFT1r(void) {
#ifdef DBG
printf("SFT1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH1r((b>>c8l)<<c8h);
}

void SFT1(void) {
#ifdef DBG
printf("SFT1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH1((b>>c8l)<<c8h);
}

void SFT2kr(void) {
#ifdef DBG
printf("SFT2kr\n");
#endif
unsigned char c = GET1r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH1r(c);
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH2r((b>>c8l)<<c8h);
}

void SFT2k(void) {
#ifdef DBG
printf("SFT2k\n");
#endif
unsigned char c = GET1();
unsigned short b = GET2();
PUSH2(b);
PUSH1(c);
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH2((b>>c8l)<<c8h);
}

void SFT2r(void) {
#ifdef DBG
printf("SFT2r\n");
#endif
unsigned char c = GET1r();
unsigned short b = GET2r();
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH2r((b>>c8l)<<c8h);
}

void SFT2(void) {
#ifdef DBG
printf("SFT2\n");
#endif
unsigned char c = GET1();
unsigned short b = GET2();
unsigned char c8l = (c & 0xF);
unsigned char c8h =  ((c >> 4) & 0xF);
PUSH2((b>>c8l)<<c8h);
}

void STA1kr(void) {
#ifdef DBG
printf("STA1kr\n");
#endif
unsigned short a = GET2r();
unsigned char v = GET1r();
PUSH1r(v);
PUSH2r(a);
u->ram[a] = v;
}

void STA1k(void) {
#ifdef DBG
printf("STA1k\n");
#endif
unsigned short a = GET2();
unsigned char v = GET1();
PUSH1(v);
PUSH2(a);
u->ram[a] = v;
}

void STA1r(void) {
#ifdef DBG
printf("STA1r\n");
#endif
unsigned short a = GET2r();
unsigned char v = GET1r();
u->ram[a] = v;
}

void STA1(void) {
#ifdef DBG
printf("STA1\n");
#endif
unsigned short a = GET2();
unsigned char v = GET1();
u->ram[a] = v;
}

void STA2kr(void) {
#ifdef DBG
printf("STA2kr\n");
#endif
unsigned short a = GET2r();
unsigned short v = GET2r();
PUSH2r(v);
PUSH2r(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STA2k(void) {
#ifdef DBG
printf("STA2k\n");
#endif
unsigned short a = GET2();
unsigned short v = GET2();
PUSH2(v);
PUSH2(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STA2r(void) {
#ifdef DBG
printf("STA2r\n");
#endif
unsigned short a = GET2r();
unsigned short v = GET2r();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STA2(void) {
#ifdef DBG
printf("STA2\n");
#endif
unsigned short a = GET2();
unsigned short v = GET2();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STR1kr(void) {
#ifdef DBG
printf("STR1kr\n");
#endif
}

void STR1k(void) {
#ifdef DBG
printf("STR1k\n");
#endif
}

void STR1r(void) {
#ifdef DBG
printf("STR1r\n");
#endif
}

void STR1(void) {
#ifdef DBG
printf("STR1\n");
#endif
}

void STR2kr(void) {
#ifdef DBG
printf("STR2kr\n");
#endif
}

void STR2k(void) {
#ifdef DBG
printf("STR2k\n");
#endif
}

void STR2r(void) {
#ifdef DBG
printf("STR2r\n");
#endif
}

void STR2(void) {
#ifdef DBG
printf("STR2\n");
#endif
}

void STZ1kr(void) {
#ifdef DBG
printf("STZ1kr\n");
#endif
unsigned char a = GET1r();
unsigned char v = GET1r();
PUSH1r(v);
PUSH1r(a);
u->ram[(unsigned short)a] = v;
}

void STZ1k(void) {
#ifdef DBG
printf("STZ1k\n");
#endif
unsigned char a = GET1();
unsigned char v = GET1();
PUSH1(v);
PUSH1(a);
u->ram[(unsigned short)a] = v;
}

void STZ1r(void) {
#ifdef DBG
printf("STZ1r\n");
#endif
unsigned char a = GET1r();
unsigned char v = GET1r();
u->ram[(unsigned short)a] = v;
}

void STZ1(void) {
#ifdef DBG
printf("STZ1\n");
#endif
unsigned char a = GET1();
unsigned char v = GET1();
u->ram[(unsigned short)a] = v;
}

void STZ2kr(void) {
#ifdef DBG
printf("STZ2kr\n");
#endif
unsigned char a = GET1r();
unsigned short v = GET2r();
PUSH2r(v);
PUSH1r(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STZ2k(void) {
#ifdef DBG
printf("STZ2k\n");
#endif
unsigned char a = GET1();
unsigned short v = GET2();
PUSH2(v);
PUSH1(a);
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STZ2r(void) {
#ifdef DBG
printf("STZ2r\n");
#endif
unsigned char a = GET1r();
unsigned short v = GET2r();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void STZ2(void) {
#ifdef DBG
printf("STZ2\n");
#endif
unsigned char a = GET1();
unsigned short v = GET2();
u->ram[a] = v >> 8; u->ram[a + 1] = v;
}

void SUB1kr(void) {
#ifdef DBG
printf("SUB1kr\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r(b);
PUSH1r(c);
PUSH1r( b-c );
}

void SUB1k(void) {
#ifdef DBG
printf("SUB1k\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1(b);
PUSH1(c);
PUSH1( b-c );
}

void SUB1r(void) {
#ifdef DBG
printf("SUB1r\n");
#endif
unsigned char c = GET1r();
unsigned char b = GET1r();
PUSH1r( b-c );
}

void SUB1(void) {
#ifdef DBG
printf("SUB1\n");
#endif
unsigned char c = GET1();
unsigned char b = GET1();
PUSH1( b-c );
}

void SUB2kr(void) {
#ifdef DBG
printf("SUB2kr\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r(b);
PUSH2r(c);
PUSH2r( b-c );
}

void SUB2k(void) {
#ifdef DBG
printf("SUB2k\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2(b);
PUSH2(c);
PUSH2( b-c );
}

void SUB2r(void) {
#ifdef DBG
printf("SUB2r\n");
#endif
unsigned short c = GET2r();
unsigned short b = GET2r();
PUSH2r( b-c );
}

void SUB2(void) {
#ifdef DBG
printf("SUB2\n");
#endif
unsigned short c = GET2();
unsigned short b = GET2();
PUSH2( b-c );
}

