# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
The versioning convention is as follows:

* Versions only apply to the master branch
* All versions have a corresponding tag
* The version number reflects the release date:
    - 1.1.0: 2022-10-18
    - Major version increments each year
    - Minor version increments each quarter
    - Patch version increments inside a quarter, ad-hoc

## [Unreleased]
### Planned
    - Stack-to-register transformation for subroutine calls

